package com.atlassian.oauth.serviceprovider.testdata;

import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.HashMap;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.util.Check;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.user.UserManager;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session.newSession;

public class HardcodedConsumerAndTokenSetup
{
    private static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxZDzGUGk6rElyPm0iOua0lWg84nOlhQN1gmTFTIu5WFyQFHZF6OA4HX7xATttQZ6N21yKMakuNdRvEudyN/coUqe89r3Ae+rkEIn4tCxGpJWX205xVF3Cgsn8ICj6dLUFQPiWXouoZ7HG0sPKhCLXXOvUXmekivtyx4bxVFD9Zy4SQ7IHTx0V0pZYGc6r1gF0LqRmGVQDaQSbivigH4mlVwoAO9Tfccf+V00hYuSvntU+B1ZygMw2rAFLezJmnftTxPuehqWu9xS5NVsPsWgBL7LOi3oY8lhzOYjbMKDWM6zUtpOmWJA52cVJW6zwxCxE28/592IARxlJcq14tjwYwIDAQAB";

    public HardcodedConsumerAndTokenSetup(ServiceProviderConsumerStore consumerStore, ServiceProviderTokenStore store, UserManager userManager)
    {
        Check.notNull(consumerStore, "consumerStore");
        Check.notNull(store, "store");

        PublicKey publicKey;
        try
        {
            publicKey = RSAKeys.fromPemEncodingToPublicKey(PUBLIC_KEY);
        }
        catch (GeneralSecurityException e)
        {
            throw new RuntimeException(e);
        }
        
        Consumer hardcodedConsumer = Consumer.key("hardcoded-consumer")
            .name("Hardcoded Consumer")
            .publicKey(publicKey)
            .description("Hardcoded Consumer")
            .callback(URI.create("http://localhost:8080/consumer/oauthcallback"))
            .build();
        consumerStore.put(hardcodedConsumer);
        
        Consumer hardcodedConsumerWithoutCallback = Consumer.key("hardcoded-nocallback")
            .name("Hardcoded Consumer - Without Callback")
            .publicKey(publicKey)
            .description("Hardcoded Consumer - Without Callback")
            .build();
        consumerStore.put(hardcodedConsumerWithoutCallback);

        URI callback = URI.create("http://consumer/callback");
        
        ServiceProviderToken nonAuthorizedRequestTokenForAuthorizing = ServiceProviderToken.newRequestToken("bb6dd1391ce33b5bd3ecad1175139a39")
            .tokenSecret("29c3005cc5fbe5d431f27b29d6191ea3")
            .consumer(hardcodedConsumer)
            .callback(callback)
            .version(Version.V_1_0_A)
            .build();
        store.put(nonAuthorizedRequestTokenForAuthorizing);
        
        ServiceProviderToken nonAuthorizedRequestTokenForDenying = ServiceProviderToken.newRequestToken("RiZie2UaooXee5siJi6gee0tmeeBe0cu")
            .tokenSecret("ew0kaiK1Eetekee2Ahjah2hoAif5eu9P")
            .consumer(hardcodedConsumer)
            .callback(callback)
            .version(Version.V_1_0_A)
            .build();
        store.put(nonAuthorizedRequestTokenForDenying);

        ServiceProviderToken spareNonAuthorizedRequestToken = ServiceProviderToken.newRequestToken("cc7ee2402df44c6ce4fdbe2286240b40")
            .tokenSecret("30d4116dd6acf6e542a38c30e7202fb4")
            .consumer(hardcodedConsumer)
            .callback(callback)
            .version(Version.V_1_0_A)
            .build();
        store.put(spareNonAuthorizedRequestToken);
        
        ServiceProviderToken nonAuthorizedRequestTokenWithoutCallbackForAuthorizing = ServiceProviderToken.newRequestToken("iezied5IEeh0IoquuGh9riexUenei4Ai")
            .tokenSecret("xei1kohXEepheed3Hemie7AhpoiG2cum")
            .consumer(hardcodedConsumerWithoutCallback)
            .version(Version.V_1_0_A)
            .build();
        store.put(nonAuthorizedRequestTokenWithoutCallbackForAuthorizing);

        ServiceProviderToken nonAuthorizedRequestTokenWithoutCallbackForDenying = ServiceProviderToken.newRequestToken("Ga9zoo0Ger0oa0IuNaeShoh4eiShae6a")
            .tokenSecret("Zijae1XuoT5AYooneingi4NoXiw0uvee")
            .consumer(hardcodedConsumerWithoutCallback)
            .version(Version.V_1_0_A)
            .build();
        store.put(nonAuthorizedRequestTokenWithoutCallbackForDenying);

        ServiceProviderToken authorizedRequestTokenForSwapping = ServiceProviderToken.newRequestToken("5c09d8d4e50065eb49a05200035bd780")
            .tokenSecret("870abbc4847d9b5790cff56a2e9b8279")
            .consumer(hardcodedConsumer)
            .authorizedBy(userManager.resolve("fred"))
            .verifier("cu3Aechunoo1CeluKiK1Tielphooci7I")
            .callback(callback)
            .version(Version.V_1_0_A)
            .build();
        store.put(authorizedRequestTokenForSwapping);

        ServiceProviderToken spareAuthorizedRequestToken = ServiceProviderToken.newRequestToken("6b10e9e5f61176fc50b16311146ce891")
            .tokenSecret("981bccd5958e0c6801daa67b3f0c9380")
            .consumer(hardcodedConsumer)
            .authorizedBy(userManager.resolve("fred"))
            .verifier("deShay0zai2YeenishooTa7iaB9suph1")
            .callback(callback)
            .version(Version.V_1_0_A)
            .build();
        store.put(spareAuthorizedRequestToken);

        ServiceProviderToken accessToken = ServiceProviderToken.newAccessToken("71b5607f60c0aae6161ce251dd55e8ed")
            .tokenSecret("160881ffbe3c4ff0f6a1ba9078e92e83")
            .consumer(hardcodedConsumer)
            .authorizedBy(userManager.resolve("fred"))
            .properties(new HashMap<String, String>() {{ put("alternate.consumer.name", "TooCool Gadget"); }})
            .build();
        store.put(accessToken);

        // OAuth v1 test tokens
        ServiceProviderToken nonAuthorizedVersion1RequestTokenForAuthorizing = ServiceProviderToken.newRequestToken("Quie9Tooico3ahShpagh0Voodoh1Phah")
            .tokenSecret("29c3005cc5fbe5d431f27b29d6191ea3")
            .consumer(hardcodedConsumer)
            .callback(callback)
            .version(Version.V_1_0)
            .build();
        store.put(nonAuthorizedVersion1RequestTokenForAuthorizing);

        ServiceProviderToken nonAuthorizedVersion1RequestTokenForDenying = ServiceProviderToken.newRequestToken("eep2eeTaich5aiQuroos6IegIer2eife")
            .tokenSecret("ew0kaiK1Eetekee2Ahjah2hoAif5eu9P")
            .consumer(hardcodedConsumer)
            .callback(callback)
            .version(Version.V_1_0)
            .build();
        store.put(nonAuthorizedVersion1RequestTokenForDenying);

        ServiceProviderToken spareNonAuthorizedVersion1RequestToken = ServiceProviderToken.newRequestToken("eeS5ijooay1oiShoeiR4euVoEv2ohj5t")
            .tokenSecret("30d4116dd6acf6e542a38c30e7202fb4")
            .consumer(hardcodedConsumer)
            .callback(callback)
            .version(Version.V_1_0)
            .build();
        store.put(spareNonAuthorizedVersion1RequestToken);
        
        ServiceProviderToken nonAuthorizedVersion1RequestTokenWithoutCallbackForAuthorizing = ServiceProviderToken.newRequestToken("iqu3Ti8siet7uoShcoonaeT6zu5woh3H")
            .tokenSecret("xei1kohXEepheed3Hemie7AhpoiG2cum")
            .consumer(hardcodedConsumerWithoutCallback)
            .version(Version.V_1_0)
            .build();
        store.put(nonAuthorizedVersion1RequestTokenWithoutCallbackForAuthorizing);

        ServiceProviderToken nonAuthorizedVersion1RequestTokenWithoutCallbackForDenying = ServiceProviderToken.newRequestToken("ieXiiN8jIec8ohbioopaqu4Athei2eiT")
            .tokenSecret("Zijae1XuoT5AYooneingi4NoXiw0uvee")
            .consumer(hardcodedConsumerWithoutCallback)
            .version(Version.V_1_0)
            .build();
        store.put(nonAuthorizedVersion1RequestTokenWithoutCallbackForDenying);

        ServiceProviderToken authorizedVersion1RequestTokenForSwapping = ServiceProviderToken.newRequestToken("laim9XailocaZoh3Aeph2aekweesi5Ie")
            .tokenSecret("870abbc4847d9b5790cff56a2e9b8279")
            .consumer(hardcodedConsumer)
            .authorizedBy(userManager.resolve("fred"))
            .callback(callback)
            .version(Version.V_1_0)
            .build();
        store.put(authorizedVersion1RequestTokenForSwapping);
        
        ServiceProviderToken renewableAccessToken = ServiceProviderToken.newAccessToken("oeRah6xePai6cou3Phiequ2atoo0OhJu")
            .tokenSecret("Ahcee6eeeiBuyeh4Tueth7naVor2aeh4")
            .consumer(hardcodedConsumer)
            .authorizedBy(userManager.resolve("fred"))
            .version(Version.V_1_0_A)
            .session(newSession("Bohro1ziaefaJ1FiAifaKai8Phah5ahH").timeToLive(Long.MAX_VALUE).build())
            .build();
        store.put(renewableAccessToken);

        ServiceProviderToken nonRenewableAccessToken = ServiceProviderToken.newAccessToken("loP1shofiDoo5eeGiequ8oZurei8Ahch")
            .tokenSecret("Bae7acheAeph3iikiephahF9Gookoh3h")
            .consumer(hardcodedConsumer)
            .authorizedBy(userManager.resolve("fred"))
            .version(Version.V_1_0_A)
            .session(newSession("Ohs5ux1kzohJu4Eeaiv0no3Ujoowae8F").creationTime(0).lastRenewalTime(100).build())
            .build();
        store.put(nonRenewableAccessToken);
    }
}
