package com.atlassian.oauth.consumer.sal;

import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Properties;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.consumer.core.HostConsumerAndSecretProvider;
import com.atlassian.oauth.consumer.sal.PluginSettingsConsumerTokenStore.ConsumerTokenProperties;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session.newSession;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.USER_WITH_LONG_USERNAME;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_LONG_KEY;

import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER_WITH_LONG_KEY;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.Matchers.mapWithKeys;
import static com.atlassian.oauth.testing.Matchers.withStringLength;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.atlassian.oauth.testing.TestData.repeat;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.codec.digest.DigestUtils.shaHex;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PluginSettingsConsumerTokenStoreTest
{
    private static final String TOKEN = "abcdefghijklmnopqrstuvwxyz";
    private static final String TOKEN2 = "zbcdefghijklmnopqrstuvwxya";
    private static final String TOKEN_SECRET = "zyxwvutsrqponmlkjihgfedcba";
    private static final Map<String, String> TOKEN_PROPERTIES = ImmutableMap.of(
        "oauth_session_handle", "somehandle",
        "oauth_expires_in", "3600"
    );
    private static ConsumerToken REQUEST_TOKEN = ConsumerToken.newRequestToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
    private static Properties REQUEST_TOKEN_AS_PROPERTIES = new ConsumerTokenProperties(REQUEST_TOKEN).asProperties();
    private static ConsumerToken ACCESS_TOKEN = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
    private static Properties ACCESS_TOKEN_AS_PROPERTIES = new ConsumerTokenProperties(ACCESS_TOKEN).asProperties();
    private static ConsumerToken ACCESS_TOKEN_WITH_LONG_CONSUMER_KEY = ConsumerToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER_WITH_LONG_KEY).properties(TOKEN_PROPERTIES).build();

    @Mock ConsumerServiceStore consumerServiceStore;
    @Mock HostConsumerAndSecretProvider hostCasProvider;
    
    Map<String, Object> settings;

    ConsumerTokenStore store;
    
    @Before
    public void setUp() throws NoSuchAlgorithmException
    {
        settings = newHashMap();
        
        PluginSettingsFactory pluginSettingsFactory = mock(PluginSettingsFactory.class);
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(new MapBackedPluginSettings(settings));

        ConsumerAndSecret cas = new ConsumerAndSecret("service", RSA_CONSUMER, KEYS.getPrivate());
        ConsumerAndSecret hostCas = new ConsumerAndSecret("service", HMAC_CONSUMER, "sekret");
        when(hostCasProvider.get()).thenReturn(hostCas);
        when(consumerServiceStore.getByKey(RSA_CONSUMER.getKey())).thenReturn(cas);

        store = new PluginSettingsConsumerTokenStore(pluginSettingsFactory, consumerServiceStore, hostCasProvider);
    }
    
    @Test
    public void assertThatGetReturnsProperlyConstructedRequestToken()
    {
        settings.put(settingsKey("tokenkey"), REQUEST_TOKEN_AS_PROPERTIES);
        
        ConsumerToken token = store.get(new ConsumerTokenStore.Key("tokenkey"));
        
        assertThat(token, is(equalTo(REQUEST_TOKEN)));
    }

    @Test
    public void assertThatGettingTokenWhichUsesHostConsumerInformationUsesHostCasProvider()
    {
        ConsumerToken requestToken = ConsumerToken.newRequestToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(HMAC_CONSUMER).properties(TOKEN_PROPERTIES).build();
        Properties tokenProperties = new ConsumerTokenProperties(requestToken).asProperties();
        settings.put(settingsKey("tokenkey"), tokenProperties);
        
        ConsumerToken token = store.get(new ConsumerTokenStore.Key("tokenkey"));
        
        assertThat(token, is(equalTo(requestToken)));
    }

    @Test
    public void assertThatGetReturnsProperlyConstructedAccessToken()
    {
        settings.put(settingsKey("tokenkey"), ACCESS_TOKEN_AS_PROPERTIES);
        
        ConsumerToken token = store.get(new ConsumerTokenStore.Key("tokenkey"));
        
        assertThat(token, is(equalTo(ACCESS_TOKEN)));
    }
    
    @Test
    public void assertThatPutSavesRequestTokenInPluginSettings()
    {
        store.put(new ConsumerTokenStore.Key("tokenkey"), REQUEST_TOKEN);
        
        assertThat(
            (Properties) settings.get(settingsKey("tokenkey")),
            is(equalTo(REQUEST_TOKEN_AS_PROPERTIES))
        );
        assertThat(
            (String) settings.get(ConsumerTokenStore.class.getName() + ".consumerKeys." + RSA_CONSUMER.getKey()), 
            is(equalTo("tokenkey"))
        );
    }

    @Test
    public void assertThatGetConsumerTokensKeysForConsumerReturnsTokenAndKey() throws Exception
    {
        final ConsumerToken token1Consumer1 = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
        store.put(new ConsumerTokenStore.Key("abc"), token1Consumer1);
        final Map<ConsumerTokenStore.Key, ConsumerToken> consumerTokens = store.getConsumerTokens(RSA_CONSUMER.getKey());
        assertThat(consumerTokens.size(), is(equalTo(1)));
        assertThat(consumerTokens.get(new ConsumerTokenStore.Key("abc")),is(equalTo(token1Consumer1)));
    }


    @Test
    public void assertThatGetConsumerTokensKeysForConsumerReturnsCorrectToken() throws Exception
    {
        final Consumer consumer2 = Consumer.key("consumer2")
                .name("Consumer using RSA")
                .description("description")
                .signatureMethod(Consumer.SignatureMethod.RSA_SHA1)
                .publicKey(KEYS.getPublic())
                .callback(URI.create("http://consumer/callback"))
                .build();

        final ConsumerToken token1Consumer1 = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
        final ConsumerToken token1Consumer2 = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(consumer2).properties(TOKEN_PROPERTIES).build();
        store.put(new ConsumerTokenStore.Key("abc"), token1Consumer1);
        store.put(new ConsumerTokenStore.Key("ghi"), token1Consumer2);

        ConsumerAndSecret cas = new ConsumerAndSecret("service", consumer2, KEYS.getPrivate());
        when(consumerServiceStore.getByKey(consumer2.getKey())).thenReturn(cas);

        final Map<ConsumerTokenStore.Key, ConsumerToken> consumerTokens = store.getConsumerTokens(consumer2.getKey());
        assertThat(consumerTokens.get(new ConsumerTokenStore.Key("ghi")),is(equalTo(token1Consumer2)));
        assertThat(consumerTokens.size(), is(equalTo(1)));
    }

    @Test
    public void assertThatAfterRemovingAllTokensGetConsumerTokensKeysForConsumerReturnsNoTokens() throws Exception
    {
        final String key = "consumer-hmac";
        final Consumer consumer = Consumer.key(key)
                .name("Consumer")
                .description("description")
                .signatureMethod(Consumer.SignatureMethod.RSA_SHA1)
                .publicKey(KEYS.getPublic())
                .callback(URI.create("http://consumer/callback"))
                .build();
        final ConsumerToken token1Consumer1 = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(consumer).properties(TOKEN_PROPERTIES).build();
        final ConsumerToken token1Consumer2 = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(consumer).properties(TOKEN_PROPERTIES).build();
        store.put(new ConsumerTokenStore.Key("abc"), token1Consumer1);
        store.put(new ConsumerTokenStore.Key("ghi"), token1Consumer2);
        final Map<ConsumerTokenStore.Key, ConsumerToken> tokenMap = store.getConsumerTokens(key);
        Preconditions.checkState(tokenMap.size() == 2);
        store.remove(new ConsumerTokenStore.Key("abc"));
        store.remove(new ConsumerTokenStore.Key("ghi"));
        final Map<ConsumerTokenStore.Key, ConsumerToken> consumerTokens = store.getConsumerTokens(key);
        assertThat(consumerTokens.size(), is(equalTo(0)));
    }
    
    @Test
    public void assertThatGetConsumerTokensSkipsAnyMissingTokens() throws Exception
    {
        // OAUTH-220: in case the PluginSettings have gotten into an inconsistent state where a token was removed
        // without removing its key from the consumer's token list, it should just ignore the nonexistent key
        final ConsumerToken token1 = ConsumerToken.newAccessToken(TOKEN).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
        final ConsumerToken token2 = ConsumerToken.newAccessToken(TOKEN2).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
        store.put(new ConsumerTokenStore.Key("abc"), token1);
        store.put(new ConsumerTokenStore.Key("ghi"), token2);
        settings.remove(settingsKey("abc"));
        
        final Map<ConsumerTokenStore.Key, ConsumerToken> consumerTokens = store.getConsumerTokens(RSA_CONSUMER.getKey());
        assertThat(consumerTokens.keySet(), contains(new ConsumerTokenStore.Key("ghi")));
    }

    @Test
    public void assertThatPutSavesAccessTokenInPluginSettings()
    {
        store.put(new ConsumerTokenStore.Key("tokenkey"), ACCESS_TOKEN);
        
        assertThat(
            (Properties) settings.get(settingsKey("tokenkey")),
            is(equalTo(ACCESS_TOKEN_AS_PROPERTIES))
        );
        assertThat(
            (String) settings.get(ConsumerTokenStore.class.getName() + ".consumerKeys."  + RSA_CONSUMER.getKey()),
            is(equalTo("tokenkey"))
        );
    }
    
    @Test
    public void assertThatRemoveSetsPluginSettingsValuesToNull()
    {
        settings.put(settingsKey("tokenkey"), ACCESS_TOKEN_AS_PROPERTIES);
        settings.put(ConsumerTokenStore.class.getName() + ".consumerKeys.Test1234", "tokenkey");
        
        store.remove(new ConsumerTokenStore.Key("tokenkey"));
        
        assertNull("token properties", settings.get(settingsKey("tokenkey")));
        assertNull("consumerKeys", settings.get(ConsumerTokenStore.class.getName() + ".consumerKeys" + RSA_CONSUMER.getKey()));
    }
    
    @Test
    public void assertThatRemoveTokensForConsumerRemovesAllTokensAssociatedWithTheConsumer()
    {
        settings.put(settingsKey("tokenkey"), ACCESS_TOKEN_AS_PROPERTIES);
        settings.put(settingsKey("othertokenkey"), ACCESS_TOKEN_AS_PROPERTIES);
        settings.put(ConsumerTokenStore.class.getName() + ".consumerKeys." + RSA_CONSUMER.getKey(), "tokenkey/othertokenkey");

        store.removeTokensForConsumer(RSA_CONSUMER.getKey());
        
        assertNull(settings.get(settingsKey("tokenkey")));
        assertNull(settings.get(settingsKey("othertokenkey")));
        assertNull(settings.get(ConsumerTokenStore.class.getName() + ".consumerKeys." + RSA_CONSUMER.getKey()));
    }
    
    @Test
    public void assertThatRemovingNonExistentTokenDoesNotThrowException()
    {
        store.remove(new ConsumerTokenStore.Key("non-existent"));
    }
    
    /**
     * Some SAL implementations (like Confluence's) require keys to be less than 100 characters.  So, we need to check
     * for that limit.
     */
    @Test
    public void assertThatKeysAreLessThanOneHundredCharacters()
    {
        store.put(new ConsumerTokenStore.Key(repeat("tokenkey", 1000)), REQUEST_TOKEN);

        assertThat(settings, mapWithKeys(withStringLength(lessThanOrEqualTo(100))));
    }
    
    @Test
    public void assertThatConsumerPropertyKeysAreLessThanOneHundredCharacters()
    {
        store.put(new ConsumerTokenStore.Key("test"), ACCESS_TOKEN_WITH_LONG_CONSUMER_KEY);

        assertThat(settings, mapWithKeys(withStringLength(lessThanOrEqualTo(100))));
    }
    
    private String settingsKey(String key)
    {
        return ConsumerTokenStore.class.getName() + ".keys." + shaHex(key);
    }
}
