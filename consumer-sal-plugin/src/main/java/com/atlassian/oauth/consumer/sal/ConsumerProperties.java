package com.atlassian.oauth.consumer.sal;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.consumer.ConsumerCreationException;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.shared.sal.AbstractSettingsProperties;
import com.atlassian.oauth.util.RSAKeys;

final class ConsumerProperties extends AbstractSettingsProperties
{
    static final String KEY = "key";
    static final String NAME = "name";
    static final String PUBLIC_KEY = "publicKey";
    static final String PRIVATE_KEY = "privateKey";
    static final String DESCRIPTION = "description";
    static final String CALLBACK = "callback";
    static final String SIGNATURE_METHOD = "signatureMethod";
    static final String SHARED_SECRET = "sharedSecret";
    
    public ConsumerProperties(Properties properties)
    {
        super(properties);
    }

    public ConsumerProperties(ConsumerAndSecret cas)
    {
        putConsumerKey(cas.getConsumer().getKey());
        putSignatureMethod(cas.getConsumer().getSignatureMethod());
        if (cas.getConsumer().getSignatureMethod() == SignatureMethod.HMAC_SHA1)
        {
            putSharedSecrect(cas.getSharedSecret());
        }
        else
        {
            putPublicKey(cas.getConsumer().getPublicKey());
            putPrivateKey(cas.getPrivateKey());
        }
        putConsumerName(cas.getConsumer().getName());
        if (cas.getConsumer().getCallback() != null)
        {
            putCallback(cas.getConsumer().getCallback());
        }
        putDescription(cas.getConsumer().getDescription());
    }

    public String getConsumerKey()
    {
        return get(KEY);
    }

    public void putConsumerKey(String key)
    {
        put(KEY, key);
    }
    
    public SignatureMethod getSignatureMethod()
    {
        return SignatureMethod.valueOf(get(SIGNATURE_METHOD));
    }

    public void putSignatureMethod(SignatureMethod signatureMethod)
    {
        put(SIGNATURE_METHOD, signatureMethod.name());
    }

    public String getSharedSecret()
    {
        return get(SHARED_SECRET);
    }

    public void putSharedSecrect(String sharedSecret)
    {
        put(SHARED_SECRET, sharedSecret);
    }

    public PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        return RSAKeys.fromPemEncodingToPublicKey(get(PUBLIC_KEY));
    }

    public void putPublicKey(PublicKey publicKey)
    {
        put(PUBLIC_KEY, RSAKeys.toPemEncoding(publicKey));
    }

    public PrivateKey getPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        return RSAKeys.fromPemEncodingToPrivateKey(get(PRIVATE_KEY));
    }

    public void putPrivateKey(PrivateKey privateKey)
    {
        put(PRIVATE_KEY, RSAKeys.toPemEncoding(privateKey));
    }

    public String getConsumerName()
    {
        return get(NAME);
    }

    public void putConsumerName(String name)
    {
        put(NAME, name);
    }

    public URI getCallback() throws URISyntaxException
    {
        String callback = get(CALLBACK);
        if (callback == null)
        {
            return null;
        }
        return new URI(callback);
    }

    public void putCallback(URI callback)
    {
        if (callback == null)
        {
            return;
        }
        put(CALLBACK, callback.toString());
    }

    public String getDescription()
    {
        return get(DESCRIPTION);
    }

    public void putDescription(String description)
    {
        if (description == null)
        {
            return;
        }
        put(DESCRIPTION, description);
    }
    
    public ConsumerAndSecret asConsumerAndSecret(String serviceName)
    {
        try
        {
            Consumer consumer = newConsumer();
            if (consumer.getSignatureMethod() == SignatureMethod.HMAC_SHA1)
            {
                return new ConsumerAndSecret(serviceName, consumer, getSharedSecret());
            }
            else
            {
                return new ConsumerAndSecret(serviceName, consumer, getPrivateKey());
            }
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new ConsumerCreationException("No encryption provider with the RSA algorithm installed", e);
        }
        catch (InvalidKeySpecException e)
        {
            throw new ConsumerCreationException("Invalid public key found in store", e);
        }
        catch (URISyntaxException e)
        {
            throw new ConsumerCreationException("Callback URI in store is not a valid URI", e);
        }        
    }

    private Consumer newConsumer() throws URISyntaxException, NoSuchAlgorithmException, InvalidKeySpecException
    {
        SignatureMethod signatureMethod = getSignatureMethod();
        Consumer.InstanceBuilder builder = Consumer.key(getConsumerKey())
            .name(getConsumerName())
            .description(getDescription())
            .callback(getCallback())
            .signatureMethod(signatureMethod);
        
        if (signatureMethod == SignatureMethod.RSA_SHA1)
        {
            builder = builder.publicKey(getPublicKey());
        }
        Consumer consumer = builder.build();
        return consumer;
    }
}