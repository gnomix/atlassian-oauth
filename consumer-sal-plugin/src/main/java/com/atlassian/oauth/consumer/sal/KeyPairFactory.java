package com.atlassian.oauth.consumer.sal;

import java.security.GeneralSecurityException;
import java.security.KeyPair;

/**
 * Factory for creating consumer public/private key pairs.
 */
public interface KeyPairFactory
{
    /**
     * Returns a newly generated public/private key pair.
     * 
     * @return a newly generated public/private key pair
     * @throws GeneralSecurityException  thrown if there is a problem generating the key pair 
     */
    KeyPair newKeyPair() throws GeneralSecurityException;
}
