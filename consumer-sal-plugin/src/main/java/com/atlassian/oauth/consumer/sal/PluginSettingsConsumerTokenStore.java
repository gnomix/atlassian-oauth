package com.atlassian.oauth.consumer.sal;

import static com.atlassian.oauth.shared.sal.Functions.toDecodedKeys;
import static com.atlassian.oauth.shared.sal.Functions.toEncodedKeys;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import static org.apache.commons.codec.digest.DigestUtils.shaHex;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Token;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore;
import com.atlassian.oauth.consumer.core.HostConsumerAndSecretProvider;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.shared.sal.HashingLongPropertyKeysPluginSettings;
import com.atlassian.oauth.shared.sal.PrefixingPluginSettings;
import com.atlassian.oauth.shared.sal.TokenProperties;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

/**
 * <p>Implementation of the {@code ConsumerTokenStore} interface that uses a {@link PluginSettingsFactory} to store
 * tokens.  The {@link PluginSettings} entry keys are prefixed with com.atlassian.oauth.consumer.ConsumerTokenStore. 
 * Tokens are stored using the following property names
 * 
 * <ul>
 *     <li>{key}.token</li>
 *     <li>{key}.tokenSecret</li>
 *     <li>{key}.type</li>
 * </ul>
 * 
 * where {key} is the {@link ConsumerTokenStore.Key} used to access the token.  The token type is whether the token is an
 * access token or a request token and is stored with an single character, "a" or "r", respectively.</p>
 * 
 * <p>When building {@link ConsumerToken} objects from the stored values, the {@code ConsumerToken}s {@code consumer}
 * attribute is set using the {@link Consumer} returned by the {@link ConsumerService#getConsumer()} method of the
 * injected {@link ConsumerService} service and <b>does not</b> try to store <i>any</i> consumer information in the
 * {@link #put(com.atlassian.oauth.consumer.ConsumerTokenStore.Key, Token)} method.  This makes it unsuitable if you are
 * trying to store {@code ConsumerToken} objects that are for consumers other than that provided by the 
 * {@link ConsumerService}.</p> 
 */
public class PluginSettingsConsumerTokenStore implements ConsumerTokenStore
{
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ConsumerServiceStore consumerServiceStore;
    private final HostConsumerAndSecretProvider hostCasProvider;
    
    public PluginSettingsConsumerTokenStore(PluginSettingsFactory factory,
        ConsumerServiceStore consumerServiceStore,
        HostConsumerAndSecretProvider hostCasProvider)
    {
        this.pluginSettingsFactory = Check.notNull(factory, "factory");
        this.consumerServiceStore = Check.notNull(consumerServiceStore, "consumerServiceStore");
        this.hostCasProvider = Check.notNull(hostCasProvider, "hostCasProvider");
    }
    
    public ConsumerToken get(Key key)
    {
        Check.notNull(key, "key");
        TokenProperties props = settings().get(key);
        if (props == null)
        {
            return null;
        }
        ConsumerAndSecret cas = hostCasProvider.get();
        if (cas == null || !cas.getConsumer().getKey().equals(props.getConsumerKey()))
        {
            cas = consumerServiceStore.getByKey(props.getConsumerKey());
        }
        Check.notNull("consumerAndSecret", cas);
        final Consumer consumer = cas.getConsumer();
        if (props.isAccessToken())
        {
            return ConsumerToken.newAccessToken(props.getToken())
                .tokenSecret(props.getTokenSecret())
                .consumer(consumer)
                .properties(props.getProperties())
                .build();
        }
        else
        {
            return ConsumerToken.newRequestToken(props.getToken())
                .tokenSecret(props.getTokenSecret())
                .consumer(consumer)
                .properties(props.getProperties())
                .build();
        }
    }

    public Map<Key, ConsumerToken> getConsumerTokens(final String consumerKey)
    {
        Check.notNull(consumerKey, "consumerKey");
        final ImmutableMap.Builder<Key, ConsumerToken> consumerTokens = ImmutableMap.builder();
        for (String tokenStr : settings().getTokensForConsumer(consumerKey))
        {
            final Key key = new Key(tokenStr);
            final ConsumerToken token = get(key);
            if (token != null)
            {
                consumerTokens.put(key, token);
            }
        }
        return consumerTokens.build();
    }

    public ConsumerToken put(Key key, ConsumerToken token)
    {
        Check.notNull(key, "key");
        Check.notNull(token, "token");
        Settings settings = settings();
        settings.put(key, new ConsumerTokenProperties(token));
        settings.addTokenForConsumer(token.getConsumer().getKey(), key);
        return token;
    }

    public void remove(Key key)
    {
        Check.notNull(key, "key");
        Settings settings = settings();
        TokenProperties properties = settings.get(key);
        if (properties == null)
        {
            return;
        }
        settings.remove(key);
        settings.removeTokenForConsumer(properties.getConsumerKey(), key);
    }
    
    public void removeTokensForConsumer(String consumerKey)
    {
        Check.notNull(consumerKey, "consumerKey");
        Settings settings = settings();
        Set<String> tokens = settings.getTokensForConsumer(consumerKey);
        for (String token : tokens)
        {
            settings.remove(new Key(token));
        }
        settings.removeTokensForConsumer(consumerKey);
    }

    private Settings settings()
    {
        return new Settings(pluginSettingsFactory.createGlobalSettings());
    }

    static final class Settings
    {
        private final PluginSettings settings;

        Settings(PluginSettings settings)
        {
            this.settings = new PrefixingPluginSettings(new HashingLongPropertyKeysPluginSettings(settings), ConsumerTokenStore.class.getName());
        }
        
        TokenProperties get(Key key)
        {
            Properties props = (Properties) settings.get(tokenSettingKey(key));
            if (props == null)
            {
                return null;
            }
            return new ConsumerTokenProperties(props);
        }

        void put(Key key, TokenProperties tokenProperties)
        {
            settings.put(tokenSettingKey(key), tokenProperties.asProperties());
        }

        void remove(Key key)
        {
            settings.remove(tokenSettingKey(key));
        }

        private String tokenSettingKey(Key key)
        {
            return "keys." + shaHex(key.toString());
        }

        public Set<String> getTokensForConsumer(String consumerKey)
        {
            String tokenKeys = (String) settings.get(consumerSettingKey(consumerKey));
            if (tokenKeys == null)
            {
                return ImmutableSet.of();
            }
            return ImmutableSet.copyOf(transform(asList(tokenKeys.split("/")), toDecodedKeys()));
        }
        
        private void putTokensForConsumer(String consumerKey, Iterable<String> tokens)
        {
            if (tokens.iterator().hasNext())
            {
                settings.put(consumerSettingKey(consumerKey), Joiner.on("/").join(transform(tokens, toEncodedKeys())));
            }
            else
            {
                settings.put(consumerSettingKey(consumerKey), null);
            }
        }

        public void removeTokenForConsumer(String consumerKey, Key tokenKey)
        {
            final Set<String> tokens = Sets.filter(getTokensForConsumer(consumerKey), not(equalTo(tokenKey.toString())));
            putTokensForConsumer(consumerKey, tokens);
        }

        public void addTokenForConsumer(String consumerKey, Key tokenKey)
        {
            putTokensForConsumer(consumerKey, Sets.union(ImmutableSet.of(tokenKey.toString()), getTokensForConsumer(consumerKey)));
        }

        public void removeTokensForConsumer(String consumerKey)
        {
            settings.remove(consumerSettingKey(consumerKey));
        }

        private String consumerSettingKey(String consumerKey)
        {
            return "consumerKeys." + consumerKey;
        }
    }
    
    static final class ConsumerTokenProperties extends TokenProperties
    {
        public ConsumerTokenProperties(Properties properties)
        {
            super(properties);
        }

        public ConsumerTokenProperties(ConsumerToken token)
        {
            super(token);
        }
    }
}
