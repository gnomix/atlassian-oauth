package com.atlassian.oauth.consumer.sal;

import java.security.GeneralSecurityException;
import java.security.KeyPair;

import com.atlassian.oauth.util.RSAKeys;

/**
 * Simple implementation that uses the installed crypto provider to generate RSA key pairs.
 */
public class KeyPairFactoryImpl implements KeyPairFactory
{
    public KeyPair newKeyPair() throws GeneralSecurityException
    {
        return RSAKeys.generateKeyPair();
    }
}
