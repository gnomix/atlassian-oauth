package com.atlassian.oauth.consumer.sal;

import static com.atlassian.oauth.shared.sal.Functions.toDecodedKeys;
import static com.atlassian.oauth.shared.sal.Functions.toEncodedKeys;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.isBlank;

import java.util.Properties;
import java.util.Set;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore;
import com.atlassian.oauth.shared.sal.HashingLongPropertyKeysPluginSettings;
import com.atlassian.oauth.shared.sal.PrefixingPluginSettings;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

public class PluginSettingsConsumerServiceStore implements ConsumerServiceStore
{
    private final PluginSettingsFactory pluginSettingsFactory;

    public PluginSettingsConsumerServiceStore(PluginSettingsFactory factory)
    {
        this.pluginSettingsFactory = Check.notNull(factory, "factory");
    }
    
    public ConsumerAndSecret get(String service)
    {
        Check.notNull(service, "service");
        ConsumerProperties consumerProperties = settings().getConsumerProperties(service);
        if (consumerProperties == null)
        {
            return null;
        }
        return consumerProperties.asConsumerAndSecret(service);
    }
    
    public ConsumerAndSecret getByKey(String consumerKey)
    {
        Check.notNull(consumerKey, "consumerKey");
        String service = settings().getServiceNameForConsumerKey(consumerKey);
        if (service == null)
        {
            return null;
        }
        return get(service);
    }

    public Iterable<Consumer> getAllServiceProviders()
    {
        return Iterables.transform(settings().getServiceNames(), new Function<String, Consumer>()
        {
            public Consumer apply(String service)
            {
                return get(service).getConsumer();
            }
        });
    }
    
    public void put(String service, ConsumerAndSecret cas)
    {
        Check.notNull(service, "service");
        Check.notNull(cas, "cas");

        settings().putConsumerProperties(service, new ConsumerProperties(cas));
    }
    
    public void removeByKey(String consumerKey)
    {
        Check.notNull(consumerKey, "consumerKey");
        String service = settings().getServiceNameForConsumerKey(consumerKey);
        if (service == null)
        {
            return;
        }
        settings().removeConsumerProperties(service, consumerKey);
    }

    private Settings settings()
    {
        return new Settings(pluginSettingsFactory.createGlobalSettings());
    }
    
    static final class Settings
    {
        private final PluginSettings settings;

        public Settings(PluginSettings settings)
        {
            this.settings = new PrefixingPluginSettings(new HashingLongPropertyKeysPluginSettings(settings), ConsumerService.class.getName());
        }
        
        public ConsumerProperties getConsumerProperties(String service)
        {
            Properties props = (Properties) settings.get(service);
            if (props == null)
            {
                return null;
            }
            return new ConsumerProperties(props);
        }

        private void putConsumerProperties(String service, ConsumerProperties props)
        {
            settings.put(service, props.asProperties());
            addService(service);
            putServiceNameForConsumerKey(props.getConsumerKey(), service);
        }

        private void removeConsumerProperties(String service, String consumerKey)
        {
            settings.remove(service);
            removeService(service);
            removeServiceNameForConsumerKey(consumerKey);
        }

        public String getServiceNameForConsumerKey(String consumerKey)
        {
            return (String) settings.get(Keys.CONSUMER_SERVICE + "." + consumerKey);
        }

        private void putServiceNameForConsumerKey(String consumerKey, String service)
        {
            settings.put(Keys.CONSUMER_SERVICE + "." + consumerKey, service);
        }

        private void removeServiceNameForConsumerKey(String consumerKey)
        {
            settings.remove(Keys.CONSUMER_SERVICE + "." + consumerKey);
        }

        public Set<String> getServiceNames()
        {
            String encodedKeys = (String) settings.get(Keys.SERVICE_NAMES);
            if (isBlank(encodedKeys))
            {
                return ImmutableSet.of();
            }
            return ImmutableSet.copyOf(transform(asList(encodedKeys.split("/")), toDecodedKeys()));
        }

        private void putServiceNames(Iterable<String> keys)
        {
            settings.put(Keys.SERVICE_NAMES, Joiner.on("/").join(transform(keys, toEncodedKeys())));            
        }
        
        private void addService(String service)
        {
            putServiceNames(Sets.union(ImmutableSet.of(service), getServiceNames()));
        }
        
        private void removeService(String service)
        {
            putServiceNames(Sets.filter(getServiceNames(), not(equalTo(service))));
        }

        static final class Keys
        {
            static final String SERVICE_NAMES = "serviceNames";
            static final String CONSUMER_SERVICE = "consumerService";
        }
    }    
}
