package com.atlassian.oauth;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.net.URI;

import org.junit.Test;

import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.Request.Parameter;
import com.google.common.collect.ImmutableList;

public class RequestTest
{
    @Test
    public void assertThatMultipleParametersOfTheSameNameArePersisted()
    {
        Request.Parameter param1 = new Parameter("name", "value1");
        Request.Parameter param2 = new Parameter("name", "value2");
        Request request = newRequest(param1, param2);
        
        assertThat(request.getParameters(), contains(param1, param2));
    }

    @Test
    public void assertThatGetParameterReturnsTheFirstParameterValue()
    {
        Request request = newRequest(new Parameter("name", "value1"), new Parameter("name", "value2"));
        
        assertThat(request.getParameter("name"), is(equalTo("value1")));
    }
    
    private Request newRequest(Parameter... parameters)
    {
        return new Request(HttpMethod.GET, URI.create("http://example.com"), ImmutableList.of(parameters));
    }
}
