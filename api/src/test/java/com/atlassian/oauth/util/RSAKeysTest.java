package com.atlassian.oauth.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;

import org.junit.Test;

public class RSAKeysTest
{
    @Test
    public void assertThatConvertingFromOpenSslStripsHeadersFootersAndNewlines()
    {
        assertThat(RSAKeys.convertFromOpenSsl(PUBLIC_KEY), is(equalTo(ENCODED_PUBLIC_KEY)));
    }

    @Test
    public void assertThatGenerateCreatesAnRSAKeyPair() throws NoSuchAlgorithmException
    {
        KeyPair keyPair = RSAKeys.generateKeyPair();
        assertThat(keyPair.getPrivate().getAlgorithm(), is(equalTo("RSA")));
        assertThat(keyPair.getPublic().getAlgorithm(), is(equalTo("RSA")));
    }
    
    @Test
    public void assertThatFromPemEncodingToPublicKeyWorksWithEncodedKeys() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        assertThat(RSAKeys.fromPemEncodingToPublicKey(ENCODED_PUBLIC_KEY), is(instanceOf(RSAPublicKey.class)));
    }
    
    @Test
    public void assertThatFromPemEncodingToPublicKeyWorksWithOpenSslFormattedEncodedStrings() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        assertThat(RSAKeys.fromPemEncodingToPublicKey(PUBLIC_KEY), is(instanceOf(RSAPublicKey.class)));
    }

    @Test
    public void assertThatFromPemEncodingToPrivateKeyWorksWithEncodedKey() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        assertThat(RSAKeys.fromPemEncodingToPrivateKey(ENCODED_PRIVATE_KEY), is(instanceOf(RSAPrivateKey.class)));
    }

    @Test
    public void assertThatFromPemEncodingToPrivateKeyWorksWithOpenSslFormattedEncodedStrings() throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        assertThat(RSAKeys.fromPemEncodingToPrivateKey(PRIVATE_KEY), is(instanceOf(RSAPrivateKey.class)));
    }
    
    @Test
    public void assertThatFromEncodedCertificate() throws CertificateException
    {
        assertThat(RSAKeys.fromEncodedCertificateToPublicKey(CERTIFICATE), is(instanceOf(RSAPublicKey.class)));
    }

    private static final String ENCODED_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFkPMZQaTqsSXI+bSI65rSVaDzic6WFA3WCZMVMi7lYXJAUdkXo4DgdfvEBO21Bno3bXIoxqS411G8S53I39yhSp7z2vcB76uQQifi0LEaklZfbTnFUXcKCyfwgKPp0tQVA+JZei6hnscbSw8qEItdc69ReZ6SK+3LHhvFUUP1nLhJDsgdPHRXSllgZzqvWAXQupGYZVANpBJuK+KAfiaVXCgA71N9xx/5XTSFi5K+e1T4HVnKAzDasAUt7Mmad+1PE+56Gpa73FLk1Ww+xaAEvss6LehjyWHM5iNswoNYzrNS2k6ZYkDnZxUlbrPDELETbz/n3YgBHGUlyrXi2PBjAgMBAAECggEAAtMctqq6meRofuQbEa4Uq5cv0uuQeZLV086VPMNX6k2nXYYODYl36T2mmNndMC5khvBYpn6Ykk/5yjBmlB2nQOMZPLFPwMZVdJ2Nhm+naJLZC0o7fje49PrN2mFsdoZeI+LHVLIrgoILpLdBAz/zTiW+RvLvMnXQU4wdp4eO6i8J/Jwh0AY8rWsAGkk1mdZDwklPZZiwR3z+DDsDwPxFs8z6cE5rWJd2c/fhAQrHwOXyrQPsGyLHTOqS3BkjtEZrKRUlfdgV76VlThwrE5pAWuO0GPyfK/XCklwcNS1a5XxCOq3uUogWRhCsqUX6pYfAVS6xzX56MGDndQVlp7U5uQKBgQDyTDwhsNTWlmr++FyYrc6liSF9NEMBNDubrfLJH1kaOp590bE8fu3BG0UlkVcueUr05e33Kx1DMSFW72lR4dht1jruWsbFp6LlT3SUtyW2kcSet3fC8gySs2r6NncsZ2XFPoxTkalKpQ1atGoBe3XIKeT8RDZtgoLztQy7/7yANQKBgQDQvSHEKS5SttoFFf4YkUh2QmNX5m7XaDlTLB/3xjnlz8NWOweK1aVysb4t2Tct/SR4ZZ/qZDBlaaj4X9h9nlxxIMoXEyX6Ilc4tyCWBXxn6HFMSa/Rrq662Vzz228cPvW2XGOQWdj7IqwKO9cXgJkI5W84YtMtYrTPLDSjhfpxNwKBgGVCoPq/iSOpN0wZhbE1KiCaP8mwlrQhHSxBtS6CkF1a1DPm97g9n6VNfUdnB1Vf0YipsxrSBOe416MaaRyUUzwMBRLqExo1pelJnIIuTG+RWeeu6zkoqUKCAxpQuttu1uRo8IJYZLTSZ9NZhNfbveyKPa2D4G9B1PJ+3rSO+ztlAoGAZNRHQEMILkpHLBfAgsuC7iUJacdUmVauAiAZXQ1yoDDo0Xl4HjcvUSTMkccQIXXbLREh2w4EVqhgR4G8yIk7bCYDmHvWZ2o5KZtD8VO7EVI1kD0z4Zx4qKcggGbp2AINnMYqDetopX7NDbB0KNUklyiEvf72tUCtyDk5QBgSrqcCgYEAnlg3ByRd/qTFz/darZi9ehT68Cq0CS7/B9YvfnF7YKTAv6J2Hd/i9jGKcc27x6IMi0vf7zrqCyTMq56omiLdu941oWfsOnwffWRBInvrUWTj6yGHOYUtg2z4xESUoFYDeWwe/vX6TugL3oXSX3Sy3KWGlJhn/OmsN2fgajHRip0=";
    private static final String ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoa5uprLJ8KWdcX4cOINz92mIBiRJCG8SbeDVQzvLk64TO12oaU0P9nCWvVCp5BnYNFUEzg0y1LrnZqyeUJRF+a6C0U7ZCucETzJoYc18kBnXejZkAyZhMt82twQdDV6jEDczi5tfG04a8Tc7YBmtbbvyDQdKhujnzM/TLHbOTqb0hOsDY1EJphtZ055jOlhDawqRoeEwrNF+B09e+CNkKG+OMSGbydmN0yfkpDav6hBt/N2cKhmllhbUmh4708C86NlFhvBiQ3Z+TKPKGIVhASwFjSFD2dEubQ/QLXK12W75istw+DLAFjjlaTAI182yhfJXpAgbyBRRDbiDqPholwIDAQAB";
    
    private static final String CERTIFICATE = 
        "-----BEGIN CERTIFICATE-----\n" +
        "MIIDHDCCAoWgAwIBAgIJAMbTCksqLiWeMA0GCSqGSIb3DQEBBQUAMGgxCzAJBgNV\n" +
        "BAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIG\n" +
        "A1UEChMLR29vZ2xlIEluYy4xDjAMBgNVBAsTBU9ya3V0MQ4wDAYDVQQDEwVscnlh\n" +
        "bjAeFw0wODAxMDgxOTE1MjdaFw0wOTAxMDcxOTE1MjdaMGgxCzAJBgNVBAYTAlVT\n" +
        "MQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChML\n" +
        "R29vZ2xlIEluYy4xDjAMBgNVBAsTBU9ya3V0MQ4wDAYDVQQDEwVscnlhbjCBnzAN\n" +
        "BgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAseBXZ4NDhm24nX3sJRiZJhvy9eDZX12G\n" +
        "j4HWAMmhAcnm2iBgYpAigwhVHtOs+ZIUIdzQHvHeNd0ydc1Jg8e+C+Mlzo38OvaG\n" +
        "D3qwvzJ0LNn7L80c0XVrvEALdD9zrO+0XSZpTK9PJrl2W59lZlJFUk3pV+jFR8NY\n" +
        "eB/fto7AVtECAwEAAaOBzTCByjAdBgNVHQ4EFgQUv7TZGZaI+FifzjpTVjtPHSvb\n" +
        "XqUwgZoGA1UdIwSBkjCBj4AUv7TZGZaI+FifzjpTVjtPHSvbXqWhbKRqMGgxCzAJ\n" +
        "BgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEU\n" +
        "MBIGA1UEChMLR29vZ2xlIEluYy4xDjAMBgNVBAsTBU9ya3V0MQ4wDAYDVQQDEwVs\n" +
        "cnlhboIJAMbTCksqLiWeMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEA\n" +
        "CETnhlEnCJVDXoEtSSwUBLP/147sqiu9a4TNqchTHJObwTwDPUMaU6XIs2OTMmFu\n" +
        "GeIYpkHXzTa9Q6IKlc7Bt2xkSeY3siRWCxvZekMxPvv7YTcnaVlZzHrVfAzqNsTG\n" +
        "P3J//C0j+8JWg6G+zuo5k7pNRKDY76GxxHPYamdLfwk=\n" +
        "-----END CERTIFICATE-----";
    
    private static final String PRIVATE_KEY = 
        "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQ\n" +
        "DFkPMZQaTqsSXI+bSI65rSVaDzic6WFA3WCZMVMi7lYXJAUdkX\n" +
        "o4DgdfvEBO21Bno3bXIoxqS411G8S53I39yhSp7z2vcB76uQQi\n" +
        "fi0LEaklZfbTnFUXcKCyfwgKPp0tQVA+JZei6hnscbSw8qEItd\n" +
        "c69ReZ6SK+3LHhvFUUP1nLhJDsgdPHRXSllgZzqvWAXQupGYZV\n" +
        "ANpBJuK+KAfiaVXCgA71N9xx/5XTSFi5K+e1T4HVnKAzDasAUt\n" +
        "7Mmad+1PE+56Gpa73FLk1Ww+xaAEvss6LehjyWHM5iNswoNYzr\n" +
        "NS2k6ZYkDnZxUlbrPDELETbz/n3YgBHGUlyrXi2PBjAgMBAAEC\n" +
        "ggEAAtMctqq6meRofuQbEa4Uq5cv0uuQeZLV086VPMNX6k2nXY\n" +
        "YODYl36T2mmNndMC5khvBYpn6Ykk/5yjBmlB2nQOMZPLFPwMZV\n" +
        "dJ2Nhm+naJLZC0o7fje49PrN2mFsdoZeI+LHVLIrgoILpLdBAz\n" +
        "/zTiW+RvLvMnXQU4wdp4eO6i8J/Jwh0AY8rWsAGkk1mdZDwklP\n" +
        "ZZiwR3z+DDsDwPxFs8z6cE5rWJd2c/fhAQrHwOXyrQPsGyLHTO\n" +
        "qS3BkjtEZrKRUlfdgV76VlThwrE5pAWuO0GPyfK/XCklwcNS1a\n" +
        "5XxCOq3uUogWRhCsqUX6pYfAVS6xzX56MGDndQVlp7U5uQKBgQ\n" +
        "DyTDwhsNTWlmr++FyYrc6liSF9NEMBNDubrfLJH1kaOp590bE8\n" +
        "fu3BG0UlkVcueUr05e33Kx1DMSFW72lR4dht1jruWsbFp6LlT3\n" +
        "SUtyW2kcSet3fC8gySs2r6NncsZ2XFPoxTkalKpQ1atGoBe3XI\n" +
        "KeT8RDZtgoLztQy7/7yANQKBgQDQvSHEKS5SttoFFf4YkUh2Qm\n" +
        "NX5m7XaDlTLB/3xjnlz8NWOweK1aVysb4t2Tct/SR4ZZ/qZDBl\n" +
        "aaj4X9h9nlxxIMoXEyX6Ilc4tyCWBXxn6HFMSa/Rrq662Vzz22\n" +
        "8cPvW2XGOQWdj7IqwKO9cXgJkI5W84YtMtYrTPLDSjhfpxNwKB\n" +
        "gGVCoPq/iSOpN0wZhbE1KiCaP8mwlrQhHSxBtS6CkF1a1DPm97\n" +
        "g9n6VNfUdnB1Vf0YipsxrSBOe416MaaRyUUzwMBRLqExo1pelJ\n" +
        "nIIuTG+RWeeu6zkoqUKCAxpQuttu1uRo8IJYZLTSZ9NZhNfbve\n" +
        "yKPa2D4G9B1PJ+3rSO+ztlAoGAZNRHQEMILkpHLBfAgsuC7iUJ\n" +
        "acdUmVauAiAZXQ1yoDDo0Xl4HjcvUSTMkccQIXXbLREh2w4EVq\n" +
        "hgR4G8yIk7bCYDmHvWZ2o5KZtD8VO7EVI1kD0z4Zx4qKcggGbp\n" +
        "2AINnMYqDetopX7NDbB0KNUklyiEvf72tUCtyDk5QBgSrqcCgY\n" +
        "EAnlg3ByRd/qTFz/darZi9ehT68Cq0CS7/B9YvfnF7YKTAv6J2\n" +
        "Hd/i9jGKcc27x6IMi0vf7zrqCyTMq56omiLdu941oWfsOnwffW\n" +
        "RBInvrUWTj6yGHOYUtg2z4xESUoFYDeWwe/vX6TugL3oXSX3Sy\n" +
        "3KWGlJhn/OmsN2fgajHRip0=\n";

    private static final String PUBLIC_KEY = 
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoa5upr\n" +
        "LJ8KWdcX4cOINz92mIBiRJCG8SbeDVQzvLk64TO12oaU0P9nCW\n" +
        "vVCp5BnYNFUEzg0y1LrnZqyeUJRF+a6C0U7ZCucETzJoYc18kB\n" +
        "nXejZkAyZhMt82twQdDV6jEDczi5tfG04a8Tc7YBmtbbvyDQdK\n" +
        "hujnzM/TLHbOTqb0hOsDY1EJphtZ055jOlhDawqRoeEwrNF+B0\n" +
        "9e+CNkKG+OMSGbydmN0yfkpDav6hBt/N2cKhmllhbUmh4708C8\n" +
        "6NlFhvBiQ3Z+TKPKGIVhASwFjSFD2dEubQ/QLXK12W75istw+D\n" +
        "LAFjjlaTAI182yhfJXpAgbyBRRDbiDqPholwIDAQAB\n";
}
