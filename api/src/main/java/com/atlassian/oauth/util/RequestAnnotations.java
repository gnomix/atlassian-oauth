package com.atlassian.oauth.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Managed annotations on the request to mark certain information to downstream parties
 *
 * @since 1.2
 */
public class RequestAnnotations
{
    private static final String OAUTH_REQUEST_FLAG = "com.atlassian.oath.request-flag";

    /**
     * @param req The servlet request to test, cannot be null;
     * @return If the request was authenticated via OAuth
     * @throws NullPointerException If the req variable is null
     */
    public static boolean isOAuthRequest(HttpServletRequest req) throws NullPointerException
    {
        Check.notNull(req);
        return req.getAttribute(OAUTH_REQUEST_FLAG) != null;
    }

    /**
     * Annotates the servlet request as one having been authenticated via OAuth.  Subsequent calls on the same
     * request will have the same result.
     *
     * @param req The servlet request to annotate
     * @throws NullPointerException If the req variable is null
     */
    public static void markAsOAuthRequest(HttpServletRequest req) throws NullPointerException
    {
        Check.notNull(req);
        req.setAttribute(OAUTH_REQUEST_FLAG, "true");
    }
}
