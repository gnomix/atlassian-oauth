package com.atlassian.oauth.util;


/**
 * Utility to perform checks on parameters.
 */
public final class Check
{
    /**
     * Check that {@code reference} is not {@code null}.  If it is, throw a {@code NullPointerException}.
     * 
     * @param reference reference to check is {@code null} or not
     * @return {@code reference} so it may be used
     * @throws NullPointerException if {@code reference} is {@code null}
     */
    public static <T> T notNull(T reference)
    {
        if (reference == null)
        {
            throw new NullPointerException();
        }
        return reference;
    }

    /**
     * Check that {@code reference} is not {@code null}.  If it is, throw a {@code NullPointerException}.
     * 
     * @param reference reference to check is {@code null} or not
     * @param errorMessage message passed to the {@code NullPointerException} constructor to give more context when debugging 
     * @return {@code reference} so it may be used
     * @throws NullPointerException if {@code reference} is {@code null}
     */
    public static <T> T notNull(T reference, Object errorMessage)
    {
        if (reference == null)
        {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
        return reference;
    }

    /**
     * Check that {@code iterable} is not {@code null} and that none of the elements it contains are {@code null}.  If
     * it is {@code null} or contains any {@code null} elements, throw a {@code NullPointerException}.
     * 
     * @param iterable iterable to check is {@code null} or contains {@code null} elements
     * @param errorMessage message passed to the {@code NullPointerException} constructor to give more context when debugging 
     * @return {@code iterable} so it may be used
     * @throws NullPointerException if {@code iterable} is {@code null} or contains {@code null} elements
     */
    public static <T> Iterable<T> contentsNotNull(Iterable<T> iterable, Object errorMessage)
    {
        Check.notNull(iterable, errorMessage);
        for (T element : iterable)
        {
            Check.notNull(element, errorMessage);
        }
        return iterable;
    }

    /**
     * Checks if the string is {@code null}, empty, or contains only whitespace.
     * 
     * @param str {@code String} to check
     * @param errorMessage
     * @return {@code str} so it may be used
     * @throws NullPointerException if {@code str} is {@code null}
     * @throws IllegalArgumentException if {@code str} is empty or contains only whitespace
     */
    public static String notBlank(String str, Object errorMessage)
    {
        notNull(str, errorMessage);
        if (str.length() == 0 || str.trim().length() == 0)
        {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
        return str;
    }
}
