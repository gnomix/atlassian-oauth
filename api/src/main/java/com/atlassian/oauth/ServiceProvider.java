package com.atlassian.oauth;

import java.net.URI;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Representation of an {@link http://oauth.net/core/1.0#anchor5 OAuth Service Provider}.  
 */
public final class ServiceProvider
{
    private final URI requestTokenUri;
    private final URI authorizeUri;
    private final URI accessTokenUri;

    public ServiceProvider(URI requestTokenUri, URI authorizeUri, URI accessTokenUri)
    {
        this.requestTokenUri = requestTokenUri;
        this.authorizeUri = authorizeUri;
        this.accessTokenUri = accessTokenUri;
    }

    /**
     * Returns the URL used to obtain an unauthorized Request Token, described in 
     * {@link http://oauth.net/core/1.0#auth_step1 OAuth Section 6.1 (Obtaining an Unauthorized Request Token)}. 
     *  
     * @return the URL used to obtain an unauthorized Request Token
     */
    public URI getRequestTokenUri()
    {
        return requestTokenUri;
    }

    /**
     * Returns the URL used to obtain User authorization for Consumer access, described in 
     * {@link http://oauth.net/core/1.0#auth_step2 OAuth Section 6.2 (Obtaining User Authorization)}. 
     * 
     * @return the URL used to obtain User authorization for Consumer access
     */
    public URI getAuthorizeUri()
    {
        return authorizeUri;
    }

    /**
     * Returns the URL used to exchange the User-authorized Request Token for an Access Token, described in 
     * {@link http://oauth.net/core/1.0#auth_step3 OAuth Section 6.3 (Obtaining an Access Token)}. 
     * @return
     */
    public URI getAccessTokenUri()
    {
        return accessTokenUri;
    }
    
    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
            .append("requestTokenUri", requestTokenUri)
            .append("authorizeUri", authorizeUri)
            .append("accessTokenUri", accessTokenUri)
            .toString();
    }
}
