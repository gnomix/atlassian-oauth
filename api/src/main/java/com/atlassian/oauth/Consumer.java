package com.atlassian.oauth;

import java.net.URI;
import java.security.PublicKey;

import org.apache.commons.lang.builder.ToStringBuilder;

import net.jcip.annotations.Immutable;

import com.atlassian.oauth.util.Check;

/**
 * <p>Immutable representation of an OAuth consumer.  At a miminum a consumer is required to have a key, a name, and a
 * public key.  A consumer can also be configured to have a description and a default callback URL that will be used
 * if a callback URL is not provided after a user authorizes a request token.</p>
 * 
 * <p>Instances of {@code Consumer} objects can be constructed using the builder.  For example
 * <pre>
 *   Consumer c = Consumer.key("consumer-key").name("Consumer").publicKey(publicRSAKey).build();
 * </pre></p>
 */
@Immutable
public final class Consumer
{
    private final String key;
    private final String name;
    private final String description;
    private final SignatureMethod signatureMethod;
    private final PublicKey publicKey;
    private final URI callback;

    private Consumer(InstanceBuilder builder)
    {
        key = builder.key;
        name = builder.name;
        signatureMethod = builder.signatureMethod;
        publicKey = builder.publicKey;
        description = builder.description;
        callback = builder.callback;
    }

    /**
     * Static factory method that starts the process of building a {@code Consumer} instance.  Returns an
     * {@code InstanceBuilder} so the other attribute can be set.
     * 
     * @param key unique key used to identify the consumer in requests unauthorized OAuth request tokens   
     * @return the builder for constructing the rest of the {@code Consumer} instance
     */
    public static InstanceBuilder key(String key)
    {
        return new InstanceBuilder(Check.notNull(key, "key"));
    }

    /**
     * Returns the unique key used to identify the consumer in requests unauthorized OAuth request tokens.
     * 
     * @return the unique key used to identify the consumer in requests unauthorized OAuth request tokens
     */
    public String getKey()
    {
        return key;
    }

    /**
     * Returns the name of the consumer as it will be displayed to the user.
     *   
     * @return the name of the consumer as it will be displayed to the user
     */
    public String getName()
    {
        return name;
    }

    /**
     * Returns the method the consumer uses to sign requests.
     * 
     * @return the method the consumer uses to sign requests
     */
    public SignatureMethod getSignatureMethod()
    {
        return signatureMethod;
    }

    /**
     * Returns the RSA public key for the consumer, {@code null} if the signature method is not RSA-SHA1.
     * If the signature method is RSA-SHA1, this key is used in verifying the signature in requests from the consumer.
     * 
     * @return the RSA public key for the consumer, {@code null} if the signature method is not RSA-SHA1
     */
    public PublicKey getPublicKey()
    {
        return publicKey;
    }

    /**
     * Returns the optional description of the consumer as it would be displayed to the user, {@code null} if the
     * description was not set.
     * 
     * @return the optional description of the consumer as it would be displayed to the user, {@code null} if the
     *         description was not set.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Returns the default callback {@code URI} used after a request token has been authorized if no callback
     * {@code URI} was provided in the authorization request.
     *  
     * @return the default callback {@code URI} used after a request token has been authorized
     */
    public URI getCallback()
    {
        return callback;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
            .append("key", key)
            .append("name", name)
            .append("description", description)
            .append("callback", callback)
            .append("signatureMethod", signatureMethod)
            .append("publicKey", publicKey)
            .toString();
    }
    
    /**
     * The ways in which consumers can sign requests, as per
     * {@link http://oauth.net/core/1.0/#signing_process OAuth spec, Section 9}. 
     */
    public enum SignatureMethod
    {
        HMAC_SHA1, RSA_SHA1
    }

    /**
     * Builder allowing the optional attributes of the {@code Consumer} object under construction to be set and
     * construction of the final {@code Consumer} instance.
     */
    public static final class InstanceBuilder
    {
        private final String key;
        
        private String name;
        private SignatureMethod signatureMethod;
        private PublicKey publicKey;
        private String description;
        private URI callback;

        public InstanceBuilder(String key)
        {
            this.key = key;
        }

        /**
         * Sets the {@code name} attribute of the {@code Consumer} object under construction and returns {@code this}
         *  builder to allow other attributes to be set
         * 
         * @param name value to be used as the {@code name} attribute of the {@code Consumer} being constructed
         * @return {@code this} builder
         */
        public InstanceBuilder name(String name)
        {
            this.name = Check.notNull(name);
            return this;
        }

        /**
         * Sets the {@code signatureMethod} attribute of the {@code Consumer} object under construction and returns
         * {@code this} builder to allow other attributes to be set
         * 
         * @param signatureMethod {@code SignatureMethod} to be used when signing requests as this consumer
         * @return {@code this} builder
         */
        public InstanceBuilder signatureMethod(SignatureMethod signatureMethod)
        {
            this.signatureMethod = Check.notNull(signatureMethod);
            return this;
        }

        /**
         * Sets the {@code publicKey} attribute of the {@code Consumer} object under construction and returns 
         * {@code this} builder to allow other attributes to be set
         * 
         * @param publicKey RSA {@code PublicKey} to be used as the {@code publicKey} attribute of the {@code Consumer}
         *                  being constructed
         * @return {@code this} builder
         */
        public InstanceBuilder publicKey(PublicKey publicKey)
        {
            this.signatureMethod = SignatureMethod.RSA_SHA1;
            this.publicKey = Check.notNull(publicKey);
            return this;
        }

        /**
         * Sets the description of the consumer as it would be displayed to the user and returns 
         * {@code this} builder to allow other attributes to be set
         * 
         * @param description the description of the consumer as it would be displayed to the user 
         * @return {@code this} builder
         */
        public InstanceBuilder description(String description)
        {
            this.description = description; 
            return this;
        }

        /**
         * Sets the default callback URL used after a request token has been authorized and returns 
         * {@code this} builder to allow other attributes to be set
         * 
         * @param callback the default callback URI used after a request token has been authorized 
         * @return {@code this} builder
         */
        public InstanceBuilder callback(URI callback)
        {
            this.callback = callback;
            return this;
        }

        /**
         * Constructs and returns the final{@code Consumer} instance.
         *  
         * @return the final {@code Consumer} instance
         */
        public Consumer build()
        {
            Check.notNull(name, "name");
            Check.notNull(signatureMethod, "signatureMethod");
            if (signatureMethod == SignatureMethod.RSA_SHA1)
            {
                Check.notNull(publicKey, "publicKey must be set when the signature method is RSA-SHA1");
            }
            return new Consumer(this);
        }
    }
}
