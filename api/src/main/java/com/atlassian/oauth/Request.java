package com.atlassian.oauth;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.jcip.annotations.Immutable;

import com.atlassian.oauth.util.Check;

/**
 * Represents an HTTP request.  An HTTP request consists of the HTTP method - GET or POST in this case, the {@link URI},
 * and any parameters to be sent.
 */
@Immutable
public class Request
{
    public static final String OAUTH_SESSION_HANDLE = "oauth_session_handle";
    public static final String OAUTH_EXPIRES_IN = "oauth_expires_in";
    public static final String OAUTH_AUTHORIZATION_EXPIRES_IN = "oauth_authorization_expires_in";
    
    private final HttpMethod method;
    private final URI uri;
    private final Iterable<Parameter> parameters;
    
    // only created when needed
    private Map<String, Iterable<String>> parameterMap;

    public Request(HttpMethod method, URI uri, Iterable<Parameter> parameters)
    {
        this.method = Check.notNull(method, "method");
        this.uri = Check.notNull(uri, "uri");
        this.parameters = copy(parameters);
    }
    
    private <T> Iterable<T> copy(Iterable<T> elements)
    {
        List<T> copy = new ArrayList<T>();
        for (T e : elements)
        {
            copy.add(e);
        }
        return Collections.unmodifiableList(copy);
    }
    
    /**
     * Returns the HTTP method to use when making the request - either GET or POST.
     * 
     * @return the HTTP method to use when making the request
     */
    public HttpMethod getMethod()
    {
        return method;
    }
    
    /**
     * Returns the {@code URI} to make the request to.
     * 
     * @return the {@code URI} to make the request to.
     */
    public URI getUri()
    {
        return uri;
    }
    
    /**
     * Returns the parameters to be sent as part of the request.
     * 
     * @return the parameters to be sent as part of the request
     */
    public Iterable<Parameter> getParameters()
    {
        return parameters;
    }

    /**
     * Returns the value of the parameter.  If there are multiple parameters for the value, only the first is returned.
     * 
     * @return the value of the parameter
     */
    public String getParameter(String parameterName)
    {
        Iterable<String> values = getParameterMap().get(parameterName);
        if (values == null)
        {
            return null;
        }
        Iterator<String> it = values.iterator();
        if (!it.hasNext())
        {
            return null;
        }
        return it.next();
    }
    
    private Map<String, Iterable<String>> getParameterMap()
    {
        if (parameterMap == null)
        {
            parameterMap = makeUnmodifiableMap(makeParameterMap());
        }
        return parameterMap;
    }

    private Map<String, List<String>> makeParameterMap()
    {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        if (parameters != null)
        {
            for (Parameter p : parameters)
            {
                String name = p.getName();
                List<String> values = map.get(name);
                if (values == null)
                {
                    values = new LinkedList<String>();
                    map.put(name, values);
                }
                values.add(p.getValue());
            }
        }
        return map;
    }
    
    private Map<String, Iterable<String>> makeUnmodifiableMap(Map<String, List<String>> map)
    {
        Map<String, Iterable<String>> immutableMap = new HashMap<String, Iterable<String>>();
        for (Map.Entry<String, List<String>> entry : map.entrySet())
        {
            immutableMap.put(entry.getKey(), Collections.unmodifiableList(entry.getValue()));
        }
        return Collections.unmodifiableMap(immutableMap);
    }

    /**
     * Enumeration of the HTTP methods that may be used when making OAuth requests. 
     */
    public enum HttpMethod
    {
        GET, POST, PUT, DELETE, OPTIONS, TRACE, HEAD
    }

    /**
     * Representation of parameters that may be added to an HTTP request.  A parameter is a simple key/value pair.
     */
    @Immutable
    public static class Parameter
    {
        private final String name;
        private final String value;

        public Parameter(String name, String value)
        {
            this.name = Check.notNull(name, "name");
            this.value = value;
        }

        /**
         * Returns the name of the parameter.
         * 
         * @return the name of the parameter
         */
        public String getName()
        {
            return name;
        }

        /**
         * Returns the value of the parameter.
         * 
         * @return the value of the parameter
         */
        public String getValue()
        {
            return value;
        }

        @Override
        public String toString()
        {
            return name + '=' + value;
        }

        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + name.hashCode();
            result = prime * result + value.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null || getClass() != obj.getClass())
            {
                return false;
            }
            final Parameter that = (Parameter) obj;
            return name.equals(that.name) && value.equals(that.value);
        }
    }
}
