jQuery(function($) {
    var removeConfirmation = $("#remove-confirmation-text").attr("value");
    var removeErrorMessage = $("#remove-error-message").attr("value");
    
    var removeConsumer = function(consumerKey) {
        if (!confirm(removeConfirmation)) {
            return;
        }
        $.ajax({
            type: "POST",
            data: { key: consumerKey },
            success: function() {
                $(".remove[href$=" + consumerKey + "]").parents("tr:first").remove();
                hideTableIfEmpty("#oauth-plugin .content table");
            },
            error: function() {
                alert(removeErrorMessage);
            }
        });
    };
    
    $(".remove").each(function() {
        $(this).click(function(event) {
            removeConsumer(this.href.substring(this.href.lastIndexOf("#") + 1));
            event.preventDefault();
        });
    });
});
