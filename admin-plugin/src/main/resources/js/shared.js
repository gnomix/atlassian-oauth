function hideTableIfEmpty (element) {
    var table = $(element);
    if (table.find("tr").size() <= 1) {
        table.hide();
    }

}