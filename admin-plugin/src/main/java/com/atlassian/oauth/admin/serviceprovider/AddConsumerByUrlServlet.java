package com.atlassian.oauth.admin.serviceprovider;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.StoreException;
import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.IllegalUriException;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.Check;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

public class AddConsumerByUrlServlet extends AbstractAdminServlet
{
    private static final String BASE_URL_PARAMETER = "baseUrl";

    static final String CONSUMER_INFO_PATH = "plugins/servlet/oauth/consumer-info";
    
    private final ServiceProviderConsumerStore store;
    private final TemplateRenderer renderer;
    private final RequestFactory<Request<?, ?>> requestFactory;
    
    public AddConsumerByUrlServlet(ServiceProviderConsumerStore store, 
        TemplateRenderer renderer,
        MessageFactory messageFactory, 
        UserManager userManager,
        RequestFactory<Request<?, ?>> requestFactory,
        LoginUriProvider loginUriProvider,
        WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.store = Check.notNull(store, "store");
        this.renderer = Check.notNull(renderer, "renderer");
        this.requestFactory = Check.notNull(requestFactory, "requestFactory");
    }

    @Override
    protected void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        URI baseUri;
        try
        {
            baseUri = getBaseUri(request);
        }
        catch (IllegalUriException e)
        {
            renderError(request, response, e.getFieldMessage());
            return;
        }
        
        try
        {
            store.put(fetchConsumerInformation(baseUri));
        }
        catch (ResponseException e)
        {
            renderError(request, response, messageFactory.newMessage("com.atlassian.oauth.serviceprovider.consumer.communication.error", e.getMessage()));
            return;
        }
        catch (StoreException e)
        {
            renderError(request, response, messageFactory.newMessage("com.atlassian.oauth.serviceprovider.error.adding.consumer.to.store", e.getMessage()));
            return;
        }
        response.sendRedirect(request.getContextPath() + "/plugins/servlet/oauth/consumers/list");
    }

    private URI getBaseUri(HttpServletRequest request)
    {
        URI baseUri;
        try
        {
            String uriParam = request.getParameter(BASE_URL_PARAMETER);
            if (uriParam == null)
            {
                throw new IllegalUriException(messageFactory.newMessage("com.atlassian.oauth.serviceprovider.consumer.base.uri.is.required"));
            }
            if (!uriParam.endsWith("/"))
            {
                uriParam += "/";
            }
            baseUri = new URI(uriParam);
        }
        catch (URISyntaxException e)
        {
            throw new IllegalUriException(messageFactory.newMessage("com.atlassian.oauth.serviceprovider.invalid.uri", e.getMessage()));
        }
        if (!baseUri.isAbsolute())
        {
            throw new IllegalUriException(messageFactory.newMessage("com.atlassian.oauth.serviceprovider.consumer.base.uri.must.be.absolute"));
        }
        if (!"http".equals(baseUri.getScheme()) && !"https".equals(baseUri.getScheme()))
        {
            throw new IllegalUriException(messageFactory.newMessage("com.atlassian.oauth.serviceprovider.consumer.base.uri.must.be.http.or.https"));
        }
        return baseUri;
    }

    private void renderError(HttpServletRequest request, HttpServletResponse response, final Message error) throws IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        renderer.render(
            "consumers/add.vm",
            ImmutableMap.<String, Object>of(
                // We use a HashMap here because Velocity doesn't seem to like ImmutableMaps, see OAUTH-103
                "fieldErrorMessages", new HashMap<String, Message>() {{ put(BASE_URL_PARAMETER, error); }},
                BASE_URL_PARAMETER, request.getParameter(BASE_URL_PARAMETER)
            ),
            response.getWriter()
        );
        return;
    }

    private Consumer fetchConsumerInformation(URI baseUri) throws ResponseException
    {
        ConsumerInformationResponseHandler handler = new ConsumerInformationResponseHandler();
        Request<?, ?> request = requestFactory.createRequest(MethodType.GET, baseUri.resolve(CONSUMER_INFO_PATH).toASCIIString());
        request.setHeader("Accept", "application/xml");
        request.execute(handler);
        return handler.getConsumer();
    }
    
    private static class ConsumerInformationResponseHandler implements ResponseHandler
    {
        private Consumer consumer;
        
        public void handle(Response response) throws ResponseException
        {
            if (response.getStatusCode() != HttpServletResponse.SC_OK)
            {
                throw new ResponseException("Server responded with an error");
            }
            if (response.getHeader("Content-Type") != null && !response.getHeader("Content-Type").startsWith("application/xml"))
            {
                throw new ResponseException("Server sent an invalid response");
            }
            try
            {
                DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = docBuilder.parse(response.getResponseBodyAsStream());
                
                String consumerKey = doc.getElementsByTagName("key").item(0).getTextContent();
                String name = doc.getElementsByTagName("name").item(0).getTextContent();
                PublicKey publicKey = RSAKeys.fromPemEncodingToPublicKey(doc.getElementsByTagName("publicKey").item(0).getTextContent());
                
                String description = null;
                if (doc.getElementsByTagName("description").getLength() > 0)
                {
                    description = doc.getElementsByTagName("description").item(0).getTextContent();
                }
                URI callback = null;
                if (doc.getElementsByTagName("callback").getLength() > 0)
                {
                    callback = new URI(doc.getElementsByTagName("callback").item(0).getTextContent());
                }
                
                consumer = Consumer.key(consumerKey)
                    .name(name)
                    .publicKey(publicKey)
                    .description(description)
                    .callback(callback)
                    .build();
            }
            catch (ParserConfigurationException e)
            {
                throw new ResponseException("Unable to parse consumer information", e);
            }
            catch (SAXException e)
            {
                throw new ResponseException("Unable to parse consumer information", e);
            }
            catch (IOException e)
            {
                throw new ResponseException("Unable to parse consumer information", e);
            }
            catch (DOMException e)
            {
                throw new ResponseException("Unable to parse consumer information", e);
            }
            catch (URISyntaxException e)
            {
                throw new ResponseException("Unable to parse consumer information, callback is not a valid URL", e);
            }
            catch (NoSuchAlgorithmException e)
            {
                throw new ResponseException("Unable to parse consumer information, no RSA providers are installed", e);
            }
            catch (InvalidKeySpecException e)
            {
                throw new ResponseException("Unable to parse consumer information, the public key is not a validly encoded RSA public key", e);
            }
        }
        
        public Consumer getConsumer()
        {
            return consumer;
        }
    }
}
