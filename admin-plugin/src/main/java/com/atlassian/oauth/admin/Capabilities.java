package com.atlassian.oauth.admin;

import java.util.Map;

import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;

/**
 * Helper to determine if the consumer-plugin and service-provider-plugin are installed.  It does this by listening
 * for the {@link ConsumerService} and the {@link ServiceProviderConsumerStore} to be bound and unbound.
 */
public class Capabilities
{
    private volatile boolean consumerServiceAvailable = false;
    private volatile boolean serviceProviderConsumerStoreAvailable = false;
    
    public boolean isConsumerServiceAvailable()
    {
        return consumerServiceAvailable;
    }
    
    public boolean isServiceProviderConsumerStoreAvailable()
    {
        return serviceProviderConsumerStoreAvailable;
    }
    
    public void onBind(ConsumerService consumerService, Map<String, String> properties)
    {
        consumerServiceAvailable = true;
    }

    public void onUnbind(ConsumerService consumerService, Map<String, String> properties)
    {
        consumerServiceAvailable = false;
    }

    public void onBind(ServiceProviderConsumerStore serviceProviderConsumerStore, Map<String, String> properties)
    {
        serviceProviderConsumerStoreAvailable = true;
    }

    public void onUnbind(ServiceProviderConsumerStore serviceProviderConsumerStore, Map<String, String> properties)
    {
        serviceProviderConsumerStoreAvailable = false;
    }
}
