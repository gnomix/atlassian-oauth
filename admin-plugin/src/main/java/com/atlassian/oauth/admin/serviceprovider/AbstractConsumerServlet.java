package com.atlassian.oauth.admin.serviceprovider;

import static org.apache.commons.lang.StringUtils.isEmpty;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class AbstractConsumerServlet extends AbstractAdminServlet
{
    protected static final String CONSUMER_KEY = "key";
    protected static final String NAME = "name";
    protected static final String DESCRIPTION = "description";
    protected static final String PUBLIC_KEY = "publicKey";
    protected static final String CALLBACK = "callback";

    public AbstractConsumerServlet(UserManager userManager, MessageFactory messageFactory, LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
    }

    protected final URI getCallbackUri(HttpServletRequest request, Map<String, Message> fieldErrorMessages)
    {
        String uriParam = request.getParameter(CALLBACK);
        if (uriParam == null || isEmpty(uriParam))
        {
            return null;
        }

        URI callback;
        try
        {
            if (!uriParam.endsWith("/"))
            {
                uriParam += "/";
            }
            callback = new URI(uriParam);
        }
        catch (URISyntaxException e)
        {
            fieldErrorMessages.put(CALLBACK, messageFactory.newMessage("com.atlassian.oauth.serviceprovider.invalid.uri"));
            return null;
        }
        if (!callback.isAbsolute())
        {
            fieldErrorMessages.put(CALLBACK, messageFactory.newMessage("com.atlassian.oauth.serviceprovider.callback.uri.must.be.absolute"));
            return null;
        }
        if (!"http".equals(callback.getScheme()) && !"https".equals(callback.getScheme()))
        {
            fieldErrorMessages.put(CALLBACK, messageFactory.newMessage("com.atlassian.oauth.serviceprovider.callback.uri.must.be.http.or.https"));
            return null;
        }
        return callback;
    }

    protected final PublicKey getPublicKey(HttpServletRequest request, Map<String, Message> fieldErrorMessages)
    {
        String publicKeyParam = checkRequiredParameter(request, PUBLIC_KEY, fieldErrorMessages, "missing.public.key");
        if (publicKeyParam == null)
        {
            return null;
        }
        PublicKey publicKey = null;
        try
        {
            if (publicKeyParam.startsWith("-----BEGIN CERTIFICATE-----"))
            {
                publicKey = RSAKeys.fromEncodedCertificateToPublicKey(publicKeyParam);
            }
            else
            {
                publicKey = RSAKeys.fromPemEncodingToPublicKey(publicKeyParam);
            }
        }
        catch (GeneralSecurityException e)
        {
            fieldErrorMessages.put(PUBLIC_KEY, messageFactory.newMessage("invalid.public.key", e.getMessage()));
        }
        return publicKey;
    }

    protected final Map<String, Object> buildErrorMap(HttpServletRequest req, String messageKey, String message)
    {
        return builderFromRequest(req).put(messageKey, message).build();
    }

    protected final Map<String, Object> buildErrorMap(HttpServletRequest req, String messagesKey, Map<String, Message> errorMessages)
    {
        return builderFromRequest(req).put(messagesKey, errorMessages).build();
    }

    private final Builder<String, Object> builderFromRequest(HttpServletRequest request)
    {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        putIfNotNull(builder, request, CONSUMER_KEY);
        putIfNotNull(builder, request, PUBLIC_KEY);
        putIfNotNull(builder, request, NAME);
        putIfNotNull(builder, request, DESCRIPTION);
        putIfNotNull(builder, request, CALLBACK);
        return builder;
    }
    
    private void putIfNotNull(ImmutableMap.Builder<String, Object> builder, HttpServletRequest request, String parameterName)
    {
        if (request.getParameter(parameterName) != null)
        {
            builder.put(parameterName, request.getParameter(parameterName));
        }
    }
}
