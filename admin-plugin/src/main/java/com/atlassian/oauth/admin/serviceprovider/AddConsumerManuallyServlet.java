package com.atlassian.oauth.admin.serviceprovider;

import java.io.IOException;
import java.net.URI;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.admin.Paths;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.StoreException;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;

public class AddConsumerManuallyServlet extends AbstractConsumerServlet
{
    private static final String ADD_TEMPLATE = "consumers/add.vm";
    
    private final ServiceProviderConsumerStore store;
    private final TemplateRenderer renderer;

    public AddConsumerManuallyServlet(ServiceProviderConsumerStore store,
            TemplateRenderer renderer, 
            MessageFactory messageFactory, 
            UserManager userManager,
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.store = Check.notNull(store, "store");
        this.renderer = Check.notNull(renderer, "renderer");
    }

    @Override
    protected void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Message> fieldErrorMessages = new HashMap<String, Message>();

        String key = checkRequiredParameter(request, CONSUMER_KEY, fieldErrorMessages, "com.atlassian.oauth.serviceprovider.missing.consumer.key");
        String name = checkRequiredParameter(request, NAME, fieldErrorMessages, "missing.consumer.name");
        String description = request.getParameter(DESCRIPTION);
        PublicKey publicKey = getPublicKey(request, fieldErrorMessages);
        URI callback = getCallbackUri(request, fieldErrorMessages);
        
        if (!fieldErrorMessages.isEmpty())
        {
            response.setContentType("text/html;charset=UTF-8");
            renderer.render(ADD_TEMPLATE, buildErrorMap(request, "fieldErrorMessages", fieldErrorMessages), response.getWriter());
            return;
        }
        
        Consumer consumer = Consumer.key(key).name(name).publicKey(publicKey).description(description).callback(callback).build();
        try
        {
            store.put(consumer);
        }
        catch (StoreException e)
        {
            response.setContentType("text/html;charset=UTF-8");
            renderer.render(ADD_TEMPLATE, buildErrorMap(request, "errorMessages", e.getMessage()), response.getWriter());
            return;
        }
        response.sendRedirect(request.getContextPath() + Paths.CONSUMER_LIST);
    }
}
