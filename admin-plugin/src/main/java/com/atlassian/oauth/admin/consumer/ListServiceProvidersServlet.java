package com.atlassian.oauth.admin.consumer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

public class ListServiceProvidersServlet extends AbstractAdminServlet
{
    private final ConsumerService service;
    private final TemplateRenderer renderer;

    public ListServiceProvidersServlet(ConsumerService service,
            TemplateRenderer renderer,
            UserManager userManager,
            MessageFactory messageFactory,
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.service = Check.notNull(service, "service");
        this.renderer = Check.notNull(renderer, "renderer");
    }

    @Override
    protected void doRestrictedGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        Iterable<Consumer> consumers = service.getAllServiceProviders();
        renderer.render("service-providers/list.vm", ImmutableMap.<String, Object>of("consumers", consumers), response.getWriter());
    }
    
    @Override
    protected void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String consumerKey = request.getParameter("key");
        if (consumerKey == null)
        {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        service.removeConsumerByKey(consumerKey);
    }
}
