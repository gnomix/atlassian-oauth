package com.atlassian.oauth.admin;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A central admin servlet that can be used as the link in web-items.  It doesn't do anything on its own except check
 * what capabilities a system has and redirect to the appropriate page based on that information.
 */
public class AdminServlet extends HttpServlet
{
    private final Capabilities capabilities;
    
    public AdminServlet(Capabilities capabilities)
    {
        this.capabilities = checkNotNull(capabilities, "capabilities");
    }
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String path;
        if (capabilities.isServiceProviderConsumerStoreAvailable())
        {
            path = Paths.CONSUMER_LIST;
        }
        else if (capabilities.isConsumerServiceAvailable())
        {
            path = Paths.VIEW_CONSUMER_INFO;
        }
        else
        {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        response.sendRedirect(request.getContextPath() + path);
    }
}
