package com.atlassian.oauth.admin.consumer;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.admin.Paths;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.shared.servlet.RendererContextBuilder;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;

public class AddServiceProviderServlet extends AbstractAdminServlet
{
    private static final String CONSUMER_KEY_PARAMETER = "consumerKey";
    private static final String NAME_PARAMETER = "name";
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String SHARED_SECRET_PARAMETER = "sharedSecret";
    
    private final TemplateRenderer renderer;
    private final ConsumerService service;

    public AddServiceProviderServlet(ConsumerService service,
            TemplateRenderer renderer,
            UserManager userManager,
            MessageFactory messageFactory,
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.service = Check.notNull(service, "consumerService");
        this.renderer = Check.notNull(renderer, "renderer");
    }
    
    @Override
    public void doRestrictedGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        response.setContentType("text/html;charset=UTF-8");
        renderer.render("service-providers/add.vm", response.getWriter());
    }

    @Override
    public void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Map<String, Message> fieldErrorMessages = Maps.newHashMap();
        String consumerKey = checkRequiredParameter(request, CONSUMER_KEY_PARAMETER, fieldErrorMessages, "com.atlassian.oauth.consumer.serviceprovider.key.is.required");
        String name = checkRequiredParameter(request, NAME_PARAMETER, fieldErrorMessages, "com.atlassian.oauth.consumer.serviceprovider.name.is.required");
        String description = request.getParameter(DESCRIPTION_PARAMETER);
        String sharedSecret = checkRequiredParameter(request, SHARED_SECRET_PARAMETER, fieldErrorMessages, "com.atlassian.oauth.consumer.serviceprovider.shared.secret.is.required");

        if (!fieldErrorMessages.isEmpty())
        {
            response.setContentType("text/html;utf-8");
            RendererContextBuilder builder = new RendererContextBuilder()
                .put("fieldErrorMessages", fieldErrorMessages)
                .put(CONSUMER_KEY_PARAMETER, consumerKey)
                .put(NAME_PARAMETER, name)
                .put(DESCRIPTION_PARAMETER, description)
                .put(SHARED_SECRET_PARAMETER, sharedSecret);
            renderer.render("service-providers/add.vm", builder.build(), response.getWriter());
            return;
        }

        Consumer consumer = Consumer.key(consumerKey)
            .name(name)
            .signatureMethod(SignatureMethod.HMAC_SHA1)
            .description(description)
            .build();
        
        service.add(name, consumer, sharedSecret);
        response.sendRedirect(request.getContextPath() + Paths.SERVICE_PROVIDER_LIST);
    }
}
