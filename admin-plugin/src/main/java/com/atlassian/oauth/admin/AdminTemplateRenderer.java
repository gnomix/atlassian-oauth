package com.atlassian.oauth.admin;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.oauth.util.Check;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRendererFactory;
import com.google.common.collect.ImmutableMap;

/**
 * Wrap a {@link TemplateRenderer} returned by the factory so we can supply a macro library to use.
 */
public class AdminTemplateRenderer implements TemplateRenderer
{
    private final TemplateRenderer renderer;
    
    public AdminTemplateRenderer(VelocityTemplateRendererFactory factory)
    {
        Check.notNull(factory, "factory");
        renderer = factory.getInstance(ImmutableMap.of("velocimacro.library", "/macros.vm"));
    }
    
    public void render(String templateName, Writer writer) throws RenderingException, IOException
    {
        renderer.render(templateName, writer);
    }

    public void render(String templateName, Map<String, Object> context, Writer writer) throws RenderingException,IOException
    {
        renderer.render(templateName, context, writer);
    }

    public String renderFragment(String fragment, Map<String, Object> context) throws RenderingException
    {
        return renderer.renderFragment(fragment, context);
    }

    public boolean resolve(String templateName)
    {
        return renderer.resolve(templateName);
    }
}
