package com.atlassian.oauth.admin.consumer;

import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.lang.StringUtils.isEmpty;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.admin.Paths;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.IllegalUriException;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.shared.servlet.RendererContextBuilder;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;

public final class UpdateConsumerInfoServlet extends AbstractAdminServlet
{
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String CALLBACK_PARAMETER = "callback";
    private static final String NAME_PARAMETER = "name";
    private final ConsumerService consumerService;
    private final TemplateRenderer renderer;

    public UpdateConsumerInfoServlet(ConsumerService consumerService,
            TemplateRenderer renderer,
            UserManager userManager,
            MessageFactory messageFactory, 
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.consumerService = Check.notNull(consumerService, "consumerService");
        this.renderer = Check.notNull(renderer, "renderer");
    }

    @Override
    public void doRestrictedGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        response.setContentType("text/html;utf-8");
        Consumer consumer = consumerService.getConsumer();
        RendererContextBuilder builder = new RendererContextBuilder()
            .put(NAME_PARAMETER, consumer.getName())
            .put(DESCRIPTION_PARAMETER, consumer.getDescription());
        if (consumer.getCallback() != null)
        {
            builder.put(CALLBACK_PARAMETER, consumer.getCallback().toASCIIString());
        }
        renderer.render("consumer-info/update.vm", builder.build(), response.getWriter());
    }
    
    @Override
    public void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        Map<String, Message> fieldErrorMessages = newHashMap();
        String name = checkRequiredParameter(request, NAME_PARAMETER, fieldErrorMessages, "com.atlassian.oauth.consumer.consumer.name.is.required");
        String description = request.getParameter(DESCRIPTION_PARAMETER);
        URI callback = null;
        try
        {
            callback = getCallbackUri(request);
        }
        catch (IllegalUriException e)
        {
            fieldErrorMessages.put(CALLBACK_PARAMETER, e.getFieldMessage());
        }
        
        if (!fieldErrorMessages.isEmpty())
        {
            response.setContentType("text/html;utf-8");
            RendererContextBuilder builder = new RendererContextBuilder()
                .put("fieldErrorMessages", fieldErrorMessages)
                .put(NAME_PARAMETER, name)
                .put(DESCRIPTION_PARAMETER, description)
                .put(CALLBACK_PARAMETER, request.getParameter(CALLBACK_PARAMETER));
            renderer.render("consumer-info/update.vm", builder.build(), response.getWriter());
            return;
        }
        consumerService.updateHostConsumerInformation(name, description, callback);
        response.sendRedirect(request.getContextPath() + Paths.VIEW_CONSUMER_INFO);
    }
    
    private URI getCallbackUri(HttpServletRequest request)
    {
        String callbackParam = request.getParameter(CALLBACK_PARAMETER);
        if (callbackParam == null || isEmpty(callbackParam))
        {
            return null;
        }
        URI callback;
        try
        {
            callback = new URI(callbackParam);
        }
        catch (URISyntaxException e)
        {
            throw new IllegalUriException(messageFactory.newMessage("invalid.url", e.getMessage()));
        }
        if (!callback.isAbsolute())
        {
            throw new IllegalUriException(messageFactory.newMessage("com.atlassian.oauth.consumer.consumer.callback.must.be.absolute"));
        }
        if (!"http".equals(callback.getScheme()) && !"https".equals(callback.getScheme()))
        {
            throw new IllegalUriException(messageFactory.newMessage("com.atlassian.oauth.consumer.consumer.callback.must.be.http.or.https"));
        }
        return callback;
    }
}
