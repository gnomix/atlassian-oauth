package com.atlassian.oauth.admin.util;

public class Strings
{
    /**
     * Adds new lines to a string every {@code n} characters.
     * 
     * @param str String to add new lines to
     * @param n number of characters between each new line  
     * @return {@code str} with newlines every {@code n} characters 
     */
    public static String addNewLines(String str, int n)
    {
        if (n == str.length())
        {
            return str;
        }
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Math.ceil((double) str.length()/n); i++)
        {
            int partBegin = i * n;
            int partEnd = partBegin + n;
            
            String part = str.substring(partBegin, Math.min(partEnd, str.length()));
            sb.append(part);
            if (part.length() == n && partEnd != str.length())
            {
                sb.append('\n');
            }
        }
        return sb.toString();
    }

}
