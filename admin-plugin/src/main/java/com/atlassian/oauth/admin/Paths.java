package com.atlassian.oauth.admin;

/**
 * Paths of the servlets relative to the application base url.
 */
public final class Paths
{
    private static final String BASE = "/plugins/servlet/oauth";
    public static final String VIEW_CONSUMER_INFO = BASE + "/view-consumer-info";
    public static final String CONSUMER_LIST = BASE + "/consumers/list";
    public static final String SERVICE_PROVIDER_LIST = "/plugins/servlet/oauth/service-providers/list";
}
