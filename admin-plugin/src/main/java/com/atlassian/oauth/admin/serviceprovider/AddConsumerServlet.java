package com.atlassian.oauth.admin.serviceprovider;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;

public class AddConsumerServlet extends AbstractAdminServlet
{
    private final TemplateRenderer renderer;

    public AddConsumerServlet(TemplateRenderer renderer,
            MessageFactory messageFactory,
            UserManager userManager,
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.renderer = renderer;
    }
    
    @Override
    public void doRestrictedGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        response.setContentType("text/html;charset=UTF-8");
        renderer.render("consumers/add.vm", response.getWriter());
    }
}
