package com.atlassian.oauth.admin.serviceprovider;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.shared.servlet.AbstractAdminServlet;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

public class ListConsumersServlet extends AbstractAdminServlet
{
    private final ServiceProviderConsumerStore consumerStore;
    private final TemplateRenderer renderer;
    private final ServiceProviderTokenStore tokenStore;

    public ListConsumersServlet(ServiceProviderConsumerStore consumerStore,
            ServiceProviderTokenStore tokenStore,
            TemplateRenderer renderer,
            UserManager userManager,
            MessageFactory messageFactory,
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.consumerStore = Check.notNull(consumerStore, "consumerStore");
        this.tokenStore = Check.notNull(tokenStore, "tokenStore");
        this.renderer = Check.notNull(renderer, "renderer");
    }

    @Override
    protected void doRestrictedGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        Iterable<Consumer> consumers = consumerStore.getAll();
        renderer.render("consumers/list.vm", ImmutableMap.<String, Object>of("consumers", consumers), response.getWriter());
    }
    
    @Override
    protected void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String consumerKey = request.getParameter("key");
        if (consumerKey == null)
        {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        tokenStore.removeByConsumer(consumerKey);
        consumerStore.remove(consumerKey);
    }
}
