package com.atlassian.oauth.admin.serviceprovider;

import java.io.IOException;
import java.net.URI;
import java.security.PublicKey;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.admin.Paths;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.StoreException;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.Check;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

public class EditConsumerServlet extends AbstractConsumerServlet
{
    private static final String EDIT_TEMPLATE = "consumers/edit.vm";
    private static final String ERROR_TEMPLATE = "consumers/edit-error.vm";
    
    private final ServiceProviderConsumerStore store;
    private final TemplateRenderer renderer;

    public EditConsumerServlet(ServiceProviderConsumerStore store, 
            TemplateRenderer renderer,
            UserManager userManager, 
            MessageFactory messageFactory, 
            LoginUriProvider loginUriProvider,
            WebSudoManager webSudoManager)
    {
        super(userManager, messageFactory, loginUriProvider, webSudoManager);
        this.store = Check.notNull(store, "store");
        this.renderer = Check.notNull(renderer, "renderer");
    }

    @Override
    public void doRestrictedGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String key = request.getParameter(CONSUMER_KEY);
        if (key == null)
        {
            response.sendRedirect(request.getContextPath() + Paths.CONSUMER_LIST);
            return;
        }
        Consumer consumer = store.get(key);
        if (consumer == null)
        {
            response.sendRedirect(request.getContextPath() + Paths.CONSUMER_LIST);
            return;
        }
        response.setContentType("text/html;charset=UTF-8");
        renderer.render(EDIT_TEMPLATE, ImmutableMap.<String, Object>of(
                CONSUMER_KEY, key,
                NAME, consumer.getName(),
                DESCRIPTION, consumer.getDescription(),
                PUBLIC_KEY, RSAKeys.toPemEncoding(consumer.getPublicKey()),
                CALLBACK, consumer.getCallback() != null ? consumer.getCallback().toASCIIString() : ""
            ), response.getWriter());
    }
    
    @Override
    public void doRestrictedPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String key = request.getParameter(CONSUMER_KEY);
        if (key == null)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            renderer.render(ERROR_TEMPLATE, ImmutableMap.<String, Object>of("message", "com.atlassian.oauth.serviceprovider.missing.consumer.key"), response.getWriter());
            return;
        }
        if (store.get(key) == null)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            renderer.render(ERROR_TEMPLATE, ImmutableMap.<String, Object>of("message", "com.atlassian.oauth.serviceprovider.unknown.consumer.key"), response.getWriter());
            return;
        }
        
        Map<String, Message> fieldErrorMessages = Maps.newHashMap();
        String name = checkRequiredParameter(request, NAME, fieldErrorMessages, "missing.consumer.name");
        String description = request.getParameter(DESCRIPTION);
        URI callback = getCallbackUri(request, fieldErrorMessages);
        PublicKey publicKey = getPublicKey(request, fieldErrorMessages);
        
        if (!fieldErrorMessages.isEmpty())
        {
            response.setContentType("text/html;charset=UTF-8");
            renderer.render(EDIT_TEMPLATE, buildErrorMap(request, "fieldErrorMessages", fieldErrorMessages), response.getWriter());
            return;
        }

        Consumer newConsumer = Consumer.key(key).name(name).description(description).callback(callback).publicKey(publicKey).build();
        try
        {
            store.put(newConsumer);
        }
        catch (StoreException e)
        {
            response.setContentType("text/html;charset=UTF-8");
            renderer.render(EDIT_TEMPLATE, buildErrorMap(request, "errorMessages", e.getMessage()), response.getWriter());
            return;
        }
        response.sendRedirect(request.getContextPath() + Paths.CONSUMER_LIST);
    }
}
