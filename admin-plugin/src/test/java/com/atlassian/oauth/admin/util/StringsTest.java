package com.atlassian.oauth.admin.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class StringsTest
{
    @Test
    public void assertThatAddNewLinesAddsNewLines()
    {
        String toAddNewLinesTo = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxZDzGUGk6rElyPm0iOua0lWg84nOlhQN1gmTFTIu5WFyQFHZF6OA4HX7xATttQZ6N21yKMakuNdRvEudyN/coUqe89r3Ae+rkEIn4tCxGpJWX205xVF3Cgsn8ICj6dLUFQPiWXouoZ7HG0sPKhCLXXOvUXmekivtyx4bxVFD9Zy4SQ7IHTx0V0pZYGc6r1gF0LqRmGVQDaQSbivigH4mlVwoAO9Tfccf+V00hYuSvntU+B1ZygMw2rAFLezJmnftTxPuehqWu9xS5NVsPsWgBL7LOi3oY8lhzOYjbMKDWM6zUtpOmWJA52cVJW6zwxCxE28/592IARxlJcq14tjwYwIDAQAB";
        String withNewLines = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxZDzGU\n" +
                      "Gk6rElyPm0iOua0lWg84nOlhQN1gmTFTIu5WFyQFHZF6OA4HX7\n" +
                      "xATttQZ6N21yKMakuNdRvEudyN/coUqe89r3Ae+rkEIn4tCxGp\n" +
                      "JWX205xVF3Cgsn8ICj6dLUFQPiWXouoZ7HG0sPKhCLXXOvUXme\n" +
                      "kivtyx4bxVFD9Zy4SQ7IHTx0V0pZYGc6r1gF0LqRmGVQDaQSbi\n" +
                      "vigH4mlVwoAO9Tfccf+V00hYuSvntU+B1ZygMw2rAFLezJmnft\n" +
                      "TxPuehqWu9xS5NVsPsWgBL7LOi3oY8lhzOYjbMKDWM6zUtpOmW\n" +
                      "JA52cVJW6zwxCxE28/592IARxlJcq14tjwYwIDAQAB";
                      
        assertThat(Strings.addNewLines(toAddNewLinesTo, 50), is(equalTo(withNewLines)));
    }
    
    @Test
    public void assertThatNewLineIsAddedWhenThereIsOneMoreCharacterThanTheLimit()
    {
        String toAddNewLinesTo = "abcdefghijklmnopqrstuvwxyz";
        String withNewLines = "abcdefghijklmnopqrstuvwxy\nz";
        
        assertThat(Strings.addNewLines(toAddNewLinesTo, 25), is(equalTo(withNewLines)));
    }
    
    @Test
    public void assertThatNewLineIsNotAddedWhenTheNumberOfCharactersEqualsTheLimit()
    {
        String toAddNewLinesTo = "abcdefghijklmnopqrstuvwxyz";
        String withNewLines = "abcdefghijklmnopqrstuvwxyz";
        
        assertThat(Strings.addNewLines(toAddNewLinesTo, 26), is(equalTo(withNewLines)));
    }

    @Test
    public void assertThatNewLineIsNotAddedWhenTheNumberOfCharacterssIsLessThanTheLimit()
    {
        String toAddNewLinesTo = "abcdefghijklmnopqrstuvwxyz";
        String withNewLines = "abcdefghijklmnopqrstuvwxyz";
        
        assertThat(Strings.addNewLines(toAddNewLinesTo, 27), is(equalTo(withNewLines)));
    }
}
