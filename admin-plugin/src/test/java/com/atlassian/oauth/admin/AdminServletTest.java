package com.atlassian.oauth.admin;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AdminServletTest
{
    private static final String CONTEXT_PATH = "/context";
    
    @Mock Capabilities capabilities;
    
    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp()
    {
        when(request.getMethod()).thenReturn("GET");
        when(request.getContextPath()).thenReturn(CONTEXT_PATH);
        
        servlet = new AdminServlet(capabilities);
    }
    
    @Test
    public void verifyThatRedirectToConsumerListIsSentWhenServiceProviderConsumerStoreIsAvailable() throws ServletException, IOException
    {
        when(capabilities.isServiceProviderConsumerStoreAvailable()).thenReturn(true);
        
        servlet.service(request, response);
        
        verify(response).sendRedirect(CONTEXT_PATH + Paths.CONSUMER_LIST);
    }
    
    @Test
    public void verifyThatRedirectToConsumerInfoIsSentIfNoServiceProviderConsumerStoreIsAvailableAndConsumerServiceIsAvailable() throws ServletException, IOException
    {
        when(capabilities.isConsumerServiceAvailable()).thenReturn(true);
        
        servlet.service(request, response);
        
        verify(response).sendRedirect(CONTEXT_PATH + Paths.VIEW_CONSUMER_INFO);
    }
    
    @Test
    public void verifyThatNotFoundResponseIsSentIfNoCapabilitiesAreAvailable() throws ServletException, IOException
    {
        servlet.service(request, response);
        
        verify(response).sendError(HttpServletResponse.SC_NOT_FOUND);
    }
}
