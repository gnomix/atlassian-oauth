package com.atlassian.oauth.admin.consumer;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URI;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

@RunWith(MockitoJUnitRunner.class)
public class UpdateConsumerInfoServletTest
{
    private static final String UPDATE_TEMPLATE = "consumer-info/update.vm";
    @Mock ConsumerService consumerService;
    @Mock TemplateRenderer renderer;
    @Mock UserManager userManager;
    @Mock MessageFactory messageFactory;
    @Mock LoginUriProvider loginUriProvider;
    @Mock WebSudoManager webSudoManager;

    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp() throws Exception
    {
        when(response.getWriter()).thenReturn(new PrintWriter(new ByteArrayOutputStream()));
        when(userManager.getRemoteUsername(request)).thenReturn("fred");
        when(userManager.isSystemAdmin("fred")).thenReturn(true);
        when(messageFactory.newMessage(anyString())).thenReturn(new Message(mock(I18nResolver.class), "placeholder", new Serializable[]{}));
        when(messageFactory.newMessage(anyString(), anyString())).thenReturn(new Message(mock(I18nResolver.class), "placeholder", new Serializable[]{}));
        
        servlet = new UpdateConsumerInfoServlet(consumerService, renderer, userManager, messageFactory, loginUriProvider, webSudoManager);
    }
    
    @Test
    public void verifyThatGetRenderersWhenDescriptionIsNull() throws Exception
    {
        Consumer consumer = Consumer.key("c").name("consumer").signatureMethod(SignatureMethod.HMAC_SHA1).callback(URI.create("http://consumer/callback")).build();
        when(request.getMethod()).thenReturn("GET");
        when(consumerService.getConsumer()).thenReturn(consumer);

        servlet.service(request, response);
        
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
    }

    @Test
    public void verifyThatGetRenderersWhenCallbackIsNull() throws Exception
    {
        Consumer consumer = Consumer.key("c").name("consumer").signatureMethod(SignatureMethod.HMAC_SHA1).description("desc").build();
        when(request.getMethod()).thenReturn("GET");
        when(consumerService.getConsumer()).thenReturn(consumer);

        servlet.service(request, response);
        
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
    }
    
    @Test
    public void verifyThatMissingNameParameterCausesUpdateFormToBeRerendered() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("description")).thenReturn("desc");
        when(request.getParameter("callback")).thenReturn("http://consumer/callback");
        servlet.service(request, response);
        
        verify(messageFactory).newMessage("com.atlassian.oauth.consumer.consumer.name.is.required");
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
        verify(consumerService, never()).updateHostConsumerInformation(anyString(), anyString(), (URI) anyObject());
    }
    
    @Test
    public void verifyThatEmptyNameParameterCausesUpdateFormToBeRerendered() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("");
        when(request.getParameter("description")).thenReturn("desc");
        when(request.getParameter("callback")).thenReturn("http://consumer/callback");
        servlet.service(request, response);
        
        verify(messageFactory).newMessage("com.atlassian.oauth.consumer.consumer.name.is.required");
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
        verify(consumerService, never()).updateHostConsumerInformation(anyString(), anyString(), (URI) anyObject());
    }

    @Test
    public void verifyThatInvalidCallbackUriCausesUpdateFormToBeReendered() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("c");
        when(request.getParameter("description")).thenReturn("desc");
        when(request.getParameter("callback")).thenReturn("an invalid uri");
        servlet.service(request, response);
        
        verify(messageFactory).newMessage(eq("invalid.url"), anyString());
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
        verify(consumerService, never()).updateHostConsumerInformation(anyString(), anyString(), (URI) anyObject());
    }

    @Test
    public void verifyThatNonHttpOrHttpsCallbackUriCausesUpdateFormToBeReendered() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("c");
        when(request.getParameter("description")).thenReturn("desc");
        when(request.getParameter("callback")).thenReturn("ftp://consumer/callback");
        servlet.service(request, response);
        
        verify(messageFactory).newMessage(eq("com.atlassian.oauth.consumer.consumer.callback.must.be.http.or.https"));
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
        verify(consumerService, never()).updateHostConsumerInformation(anyString(), anyString(), (URI) anyObject());
    }

    @Test
    public void verifyThatNonAbsoluteCallbackUriCausesUpdateFormToBeReendered() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("c");
        when(request.getParameter("description")).thenReturn("desc");
        when(request.getParameter("callback")).thenReturn("/callback");
        servlet.service(request, response);
        
        verify(messageFactory).newMessage(eq("com.atlassian.oauth.consumer.consumer.callback.must.be.absolute"));
        verify(renderer).render(eq(UPDATE_TEMPLATE), anyMap(), isA(PrintWriter.class));
        verify(consumerService, never()).updateHostConsumerInformation(anyString(), anyString(), (URI) anyObject());
    }
    
    @Test
    public void verifyThatConsumerInfoIsUpdated() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("c");
        when(request.getParameter("description")).thenReturn("desc");
        when(request.getParameter("callback")).thenReturn("http://consumer/callback");
        
        servlet.service(request, response);
        
        verify(consumerService).updateHostConsumerInformation("c", "desc", URI.create("http://consumer/callback"));
    }
}
