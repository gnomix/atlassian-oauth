package com.atlassian.oauth.admin.consumer;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

@RunWith(MockitoJUnitRunner.class)
public class ListServiceProvidersServletTest {
    @Mock ConsumerService service;
    @Mock TemplateRenderer renderer;
    @Mock UserManager userManager;
    @Mock MessageFactory messageFactory;
    @Mock LoginUriProvider loginUriProvider;
    @Mock WebSudoManager webSudoManager;
    
    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp()
    {
        when(userManager.getRemoteUsername(request)).thenReturn("fred");
        when(userManager.isSystemAdmin("fred")).thenReturn(true);

        servlet = new ListServiceProvidersServlet(service, renderer, userManager, messageFactory, loginUriProvider, webSudoManager);
    }
    
    @Test
    public void verifyThatBadRequestResponseIsSentWhenTryingToPostWithoutConsumerKey() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        
        servlet.service(request, response);
        
        verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST);
    }
    
    @Test
    public void verifyThatConsumerAndTokensAreRemovedWhenPostingWithConsumerKey() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn("some-key");
        
        servlet.service(request, response);
        
        verify(service).removeConsumerByKey("some-key");
    }
}
