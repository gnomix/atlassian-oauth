package com.atlassian.oauth.admin.serviceprovider;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

@RunWith(MockitoJUnitRunner.class)
public class AddConsumerByUrlServletTest
{
    @Mock ServiceProviderConsumerStore store;
    @Mock TemplateRenderer renderer;
    @Mock RequestFactory<Request<?, ?>> requestFactory;
    @Mock UserManager userManager;
    @Mock MessageFactory messageFactory;
    @Mock LoginUriProvider loginUriProvider;
    @Mock WebSudoManager webSudoManager;

    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp()
    {
        when(request.getMethod()).thenReturn("POST");
        
        servlet = new AddConsumerByUrlServlet(
            store, renderer, messageFactory, userManager, requestFactory, loginUriProvider, webSudoManager);
    }
    
    @Test
    public void verifyThatBaseUrlWithoutEndingSlashHasSlashAdded() throws Exception
    {
        when(request.getParameter("baseUrl")).thenReturn("http://consumer/base");
        when(userManager.getRemoteUsername(request)).thenReturn("fred");
        when(userManager.isSystemAdmin("fred")).thenReturn(true);
        when(requestFactory.createRequest(isA(MethodType.class), anyString())).thenReturn(mock(Request.class));
        
        servlet.service(request, response);
        
        verify(requestFactory).createRequest(MethodType.GET, "http://consumer/base/" + AddConsumerByUrlServlet.CONSUMER_INFO_PATH);
    }
}
