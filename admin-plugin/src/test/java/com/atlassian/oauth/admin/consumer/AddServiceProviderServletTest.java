package com.atlassian.oauth.admin.consumer;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.Matchers.hasFieldError;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.testing.FakeI18nResolver;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

@RunWith(MockitoJUnitRunner.class)
public class AddServiceProviderServletTest
{
    private static final String LIST_PATH = "/context/plugins/servlet/oauth/service-providers/list";
    private static final String ADD_TEMPLATE = "service-providers/add.vm";

    @Mock ConsumerService service;
    @Mock TemplateRenderer renderer;
    @Mock UserManager userManager;
    @Mock LoginUriProvider loginUriProvider;
    @Mock WebSudoManager webSudoManager;
    
    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    ByteArrayOutputStream baos;
    
    @Before
    public void setUp() throws Exception
    {
        when(userManager.getRemoteUsername(request)).thenReturn("fred");
        when(userManager.isSystemAdmin("fred")).thenReturn(true);
        
        when(request.getContextPath()).thenReturn("/context");

        baos = new ByteArrayOutputStream();
        when(response.getWriter()).thenReturn(new PrintWriter(baos));
        
        servlet = new AddServiceProviderServlet(service, renderer, userManager, new MessageFactory(new FakeI18nResolver()), loginUriProvider, webSudoManager);
    }
    
    @Test
    public void verifyThatMissingNameCausesRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("consumerKey")).thenReturn(HMAC_CONSUMER.getKey());
        when(request.getParameter("sharedSecret")).thenReturn("sekret");

        servlet.service(request, response);
        
        verify(renderer).render(eq(ADD_TEMPLATE), hasFieldError("name", "com.atlassian.oauth.consumer.serviceprovider.name.is.required"), isA(Writer.class));
    }

    @Test
    public void verifyThatMissingConsumerKeyCausesRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("hmac");
        when(request.getParameter("sharedSecret")).thenReturn("sekret");

        servlet.service(request, response);
        
        verify(renderer).render(eq(ADD_TEMPLATE), hasFieldError("consumerKey", "com.atlassian.oauth.consumer.serviceprovider.key.is.required"), isA(Writer.class));
    }

    @Test
    public void verifyThatMissingSharedSecretCausesRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("hmac");
        when(request.getParameter("consumerKey")).thenReturn(HMAC_CONSUMER.getKey());

        servlet.service(request, response);
        
        verify(renderer).render(eq(ADD_TEMPLATE), hasFieldError("sharedSecret", "com.atlassian.oauth.consumer.serviceprovider.shared.secret.is.required"), isA(Writer.class));
    }
    
    @Test
    public void verifyThatConsumerIsAddedWhenAllFieldsAreProvided() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("name")).thenReturn("hmac");
        when(request.getParameter("consumerKey")).thenReturn(HMAC_CONSUMER.getKey());
        when(request.getParameter("sharedSecret")).thenReturn("sekret");
        when(request.getParameter("description")).thenReturn("description");

        servlet.service(request, response);
        
        Consumer consumer = Consumer.key(HMAC_CONSUMER.getKey())
            .name("hmac")
            .description("description")
            .signatureMethod(SignatureMethod.HMAC_SHA1)
            .build();
        
        verify(service).add(eq("hmac"), (Consumer) argThat(is(equalTo(consumer))), eq("sekret"));
        verify(response).sendRedirect(LIST_PATH);
    }
}
