package com.atlassian.oauth.admin.serviceprovider;

import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.testing.FakeI18nResolver;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.shared.servlet.Message;
import com.atlassian.oauth.shared.servlet.MessageFactory;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

@RunWith(MockitoJUnitRunner.class)
public class EditConsumerServletTest
{
    private static final String LIST_PATH = "/context/plugins/servlet/oauth/consumers/list";
    
    @Mock ServiceProviderConsumerStore store;
    @Mock TemplateRenderer renderer;
    @Mock UserManager userManager;
    @Mock LoginUriProvider loginUriProvider;
    @Mock WebSudoManager webSudoManager;

    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    ByteArrayOutputStream baos;
    
    @Before
    public void setUp() throws Exception
    {
        when(request.getContextPath()).thenReturn("/context");
        
        baos = new ByteArrayOutputStream();
        when(response.getWriter()).thenReturn(new PrintWriter(baos));
        
        when(userManager.getRemoteUsername(request)).thenReturn("fred");
        when(userManager.isSystemAdmin("fred")).thenReturn(true);
        
        servlet = new EditConsumerServlet(store, renderer, userManager, new MessageFactory(mockI18nResolver()), loginUriProvider, webSudoManager);
    }

    @Test
    public void verifyThatGetWithoutKeyRedirectsToList() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        
        servlet.service(request, response);
        
        verify(response).sendRedirect(LIST_PATH);
        verifyNoMoreInteractions(response);
    }
    
    @Test
    public void verifyThatGetWithUnknownConsumerKeyRedirectsToList() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(request.getParameter("key")).thenReturn("unknown-key");
        
        servlet.service(request, response);
        
        verify(response).sendRedirect(LIST_PATH);
        verifyNoMoreInteractions(response);
    }
    
    @Test
    public void verifyEditFormIsRenderedWhenValidConsumerKeyIsProvided() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), anyMap(), isA(Writer.class));
    }

    @Test
    public void verifyThatPostWithoutKeyRendersErrorPage() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit-error.vm"), anyMap(), isA(Writer.class));
    }
    
    @Test
    public void verifyThatPostWithUnknownConsumerKeyRendersErrorPage() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn("unknown-key");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit-error.vm"), anyMap(), isA(Writer.class));
    }
    
    @Test
    public void verifyThatPostingWithMissingNameRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("publicKey")).thenReturn(RSAKeys.toPemEncoding(KEYS.getPublic()));
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), hasFieldError("name", "missing.consumer.name"), isA(Writer.class));
    }

    @Test
    public void verifyThatPostingWithMissingPublicKeyRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("name")).thenReturn("name");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), hasFieldError("publicKey", "missing.public.key"), isA(Writer.class));
    }

    @Test
    public void verifyThatPostingWithInvalidPublicKeyRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("name")).thenReturn("name");
        when(request.getParameter("publicKey")).thenReturn("not a valid public key");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), hasFieldError("publicKey", "invalid.public.key"), isA(Writer.class));
    }

    @Test
    public void verifyThatPostingWithInvalidCallbackUriRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("name")).thenReturn("name");
        when(request.getParameter("publicKey")).thenReturn(RSAKeys.toPemEncoding(KEYS.getPublic()));
        when(request.getParameter("callback")).thenReturn("not a valid uri");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), hasFieldError("callback", "com.atlassian.oauth.serviceprovider.invalid.uri"), isA(Writer.class));
    }

    @Test
    public void verifyThatPostingWithNonAbsoluteCallbackUriRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("name")).thenReturn("name");
        when(request.getParameter("publicKey")).thenReturn(RSAKeys.toPemEncoding(KEYS.getPublic()));
        when(request.getParameter("callback")).thenReturn("a/non/absolute/uri");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), hasFieldError("callback", "com.atlassian.oauth.serviceprovider.callback.uri.must.be.absolute"), isA(Writer.class));
    }

    @Test
    public void verifyThatPostingWithNonHttpOrHttpsCallbackUriRedisplaysEditFormWithError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("name")).thenReturn("name");
        when(request.getParameter("publicKey")).thenReturn(RSAKeys.toPemEncoding(KEYS.getPublic()));
        when(request.getParameter("callback")).thenReturn("ftp://consumer/callback");
        
        servlet.service(request, response);
        
        verify(renderer).render(eq("consumers/edit.vm"), hasFieldError("callback", "com.atlassian.oauth.serviceprovider.callback.uri.must.be.http.or.https"), isA(Writer.class));
    }
    
    @Test
    public void verifyThatRedirectToConsumerListIsSentWhenConsumerIsSavedSuccessfully() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("key")).thenReturn(RSA_CONSUMER.getKey());
        when(store.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);

        when(request.getParameter("name")).thenReturn("name");
        when(request.getParameter("publicKey")).thenReturn(RSAKeys.toPemEncoding(KEYS.getPublic()));
        when(request.getParameter("callback")).thenReturn("http://consumer/callback");
        
        servlet.service(request, response);
        
        verify(response).sendRedirect(LIST_PATH);
    }

    private Map<String, Object> hasFieldError(final String key, final String error)
    {
        return (Map<String, Object>) argThat(new TypeSafeDiagnosingMatcher<Map<String, Object>>()
        {
            @SuppressWarnings("unchecked")
            @Override
            protected boolean matchesSafely(Map<String, Object> item, Description mismatchDescription)
            {
                return error.equals(((Map<String, Message>) item.get("fieldErrorMessages")).get(key).toString());
            }

            public void describeTo(Description description)
            {
                description.appendText("map containing[\"fieldErrorMessages\"-><{")
                    .appendValue(key)
                    .appendText("=")
                    .appendValue(error)
                    .appendText("}>]");
            }
        });
    }

    private I18nResolver mockI18nResolver()
    {
        return new FakeI18nResolver();
    }
}
