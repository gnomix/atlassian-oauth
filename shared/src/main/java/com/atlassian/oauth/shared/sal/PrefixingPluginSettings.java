package com.atlassian.oauth.shared.sal;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.pluginsettings.PluginSettings;

public class PrefixingPluginSettings implements PluginSettings
{
    private final PluginSettings settings;
    private final String prefix;

    public PrefixingPluginSettings(PluginSettings settings, String prefix)
    {
        this.settings = Check.notNull(settings, "settings");
        this.prefix = Check.notNull(prefix, "prefix");
    }
    
    public Object get(String key)
    {
        return settings.get(prefix + "." + key);
    }

    public Object put(String key, Object value)
    {
        return settings.put(prefix + "." + key, value);
    }

    public Object remove(String key)
    {
        return settings.remove(prefix + "." + key);
    }
}
