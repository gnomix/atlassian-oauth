package com.atlassian.oauth.shared.servlet;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class RendererContextBuilder
{
    private final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
    
    public RendererContextBuilder put(String name, Object value)
    {
        if (value != null)
        {
            builder.put(name, value);
        }
        return this;
    }
    
    public Map<String, Object> build()
    {
        return builder.build();
    }
}
