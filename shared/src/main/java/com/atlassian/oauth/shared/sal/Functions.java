package com.atlassian.oauth.shared.sal;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.google.common.base.Function;

/**
 * Utility {@link Function}s.
 */
public final class Functions
{
    private Functions() {}
    
    /**
     * Returns a {@link Function} that encodes keys using the {@link URLEncoder}.
     * 
     * @return {@link Function} that encodes keys using the {@link URLEncoder}
     */
    public static Function<String, String> toEncodedKeys()
    {
        return KeyEncoder.INSTANCE;
    }
    
    private static enum KeyEncoder implements Function<String, String>
    {
        INSTANCE;
        
        public String apply(String key)
        {
            try
            {
                return URLEncoder.encode(key, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                throw new RuntimeException("Your JVM is broken", e);
            }
        }
    };
    
    /**
     * Returns a {@link Function} that decodes keys using the {@link URLDecoder}.
     */
    public static final Function<String, String> toDecodedKeys()
    {
        return KeyDecoder.INSTANCE;
    }
    
    private static enum KeyDecoder implements Function<String, String>
    {
        INSTANCE;
        
        public String apply(String encodedKey)
        {
            try
            {
                return URLDecoder.decode(encodedKey, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                throw new RuntimeException("Your JVM is broken", e);
            }
        }
    };
}
