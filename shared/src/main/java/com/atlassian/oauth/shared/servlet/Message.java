package com.atlassian.oauth.shared.servlet;

import java.io.Serializable;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.message.I18nResolver;

public final class Message
{
    private final String key;
    private final Serializable[] params;
    private final I18nResolver resolver;

    public Message(I18nResolver resolver, String key, Serializable[] params)
    {
        this.resolver = Check.notNull(resolver, "resolver");
        this.key = Check.notNull(key, "key");
        this.params = Check.notNull(params, "params");
    }
    
    @Override
    public String toString()
    {
        return resolver.getText(key, params);
    }
}
