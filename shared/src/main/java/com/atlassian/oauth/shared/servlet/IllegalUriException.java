package com.atlassian.oauth.shared.servlet;

/**
 * Thrown if a {@link URI} is invalid in any way. 
 */
public class IllegalUriException extends RuntimeException
{
    private final Message message;

    public IllegalUriException(Message message)
    {
        super(message.toString());
        this.message = message;
    }

    public Message getFieldMessage()
    {
        return message;
    }
}