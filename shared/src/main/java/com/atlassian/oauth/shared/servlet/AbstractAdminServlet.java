package com.atlassian.oauth.shared.servlet;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import org.apache.commons.lang.StringUtils;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;

public abstract class AbstractAdminServlet extends HttpServlet
{
    private final UserManager userManager;
    protected final MessageFactory messageFactory;
    private final LoginUriProvider loginUriProvider;
    private final WebSudoManager webSudoManager;

    public AbstractAdminServlet(UserManager userManager, MessageFactory messageFactory, LoginUriProvider loginUriProvider, WebSudoManager webSudoManager)
    {
        this.userManager = Check.notNull(userManager, "userManager");
        this.messageFactory = Check.notNull(messageFactory, "messageFactory");
        this.loginUriProvider = Check.notNull(loginUriProvider, "loginUriProvider");
        this.webSudoManager = Check.notNull(webSudoManager, "webSudoManager");
    }
    
    protected void doRestrictedPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        super.doPost(req, resp);
    }
    
    protected void doRestrictedGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        super.doGet(req, resp);
    }

    @Override
    protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (!isAdmin(request))
        {
            URI returnUri = URI.create(request.getServletPath() + (request.getQueryString() != null ? "?" + request.getQueryString() : ""));
            response.sendRedirect(loginUriProvider.getLoginUri(returnUri).toString());
            return;
        }

        try {
            webSudoManager.willExecuteWebSudoRequest(request);
            doRestrictedGet(request, response);
        } catch(WebSudoSessionException wes) {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }
    
    @Override
    protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (!isAdmin(request))
        {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        try {
            webSudoManager.willExecuteWebSudoRequest(request);
            doRestrictedPost(request, response);
        } catch(WebSudoSessionException wes) {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }

    private boolean isAdmin(HttpServletRequest req)
    {
        String user = userManager.getRemoteUsername(req);
        return user != null && userManager.isSystemAdmin(user);
    }

    protected final String checkRequiredParameter(HttpServletRequest request, String parameterName, Map<String, Message> errorMessages, String messageKey)
    {
        if (StringUtils.isBlank(request.getParameter(parameterName)))
        {
            errorMessages.put(parameterName, messageFactory.newMessage(messageKey));
        }
        return request.getParameter(parameterName);
    }

    protected URI getParameterAsUri(HttpServletRequest req, String parameterName, Map<String, Message> fieldErrorMessages)
    {
        String uriParam = req.getParameter(parameterName);
        URI callback = null;
        try
        {
            callback = new URI(uriParam);
        }
        catch (URISyntaxException e)
        {
            fieldErrorMessages.put(parameterName, messageFactory.newMessage("invalid.uri", e.getMessage()));
        }
        return callback;
    }
}
