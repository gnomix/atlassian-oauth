package com.atlassian.oauth.shared.servlet;

import java.io.Serializable;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.message.I18nResolver;

public class MessageFactory
{
    private final I18nResolver resolver;

    public MessageFactory(I18nResolver resolver)
    {
        this.resolver = Check.notNull(resolver, "resolver");
    }
    
    public Message newMessage(String key, Serializable... params)
    {
        return new Message(resolver, key, params);
    }
}