package com.atlassian.oauth.shared.servlet;

import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import java.io.Serializable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.sal.api.message.I18nResolver;

@RunWith(MockitoJUnitRunner.class)
public class MessageTest
{
    @Mock I18nResolver resolver;
    
    @Test
    public void verifyThatToStringUsesResolver()
    {
        Serializable[] params = new Serializable[] { "a parameter" };
        Message message = new Message(resolver, "message.key", params);
        when(resolver.getText("message.key", params)).thenReturn("A message with a parameter");
        
        assertThat(message.toString(), is(equalTo("A message with a parameter")));
        verify(resolver).getText("message.key", params);
    }
}
