package com.atlassian.oauth.shared.sal;

import static com.atlassian.oauth.shared.sal.PropertiesHelper.asProperties;
import static com.atlassian.oauth.shared.sal.PropertiesHelper.fromString;
import static com.google.common.collect.Maps.fromProperties;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

import java.util.Map;
import java.util.Properties;

import org.junit.Test;

public class PropertiesHelperTest
{
    final Properties properties = new Properties()
    {{
        put("key1", "value1");
        put("key2", "value2");
        put("key3", "value3");
    }};
    final Map<String, String> propertiesAsMap = fromProperties(properties);
    @Test
    public void assertThatAsPropertiesReturnsPropertiesObjectWithAllMapEntries()
    {
        assertThat(asProperties(propertiesAsMap), is(equalTo(properties)));
    }
    
    @Test
    public void assertThatToStringSerializesProperties()
    {
        assertThat(PropertiesHelper.toString(properties), allOf(containsString("key1=value1"), containsString("key2=value2"), containsString("key3=value3")));
    }
    
    @Test
    public void assertThatToStringSerializesPropertiesWithoutComments()
    {
        assertThat(PropertiesHelper.toString(properties), not(startsWith("#")));
    }    
    
    @Test
    public void assertThatFromStringDeserializesProperties()
    {
        assertThat(fromString("key1=value1\nkey2=value2\nkey3=value3"), is(equalTo(properties)));
    }
}
