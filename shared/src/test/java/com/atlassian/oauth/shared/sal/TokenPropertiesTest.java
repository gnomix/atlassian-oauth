package com.atlassian.oauth.shared.sal;

import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Properties;

import org.junit.Test;

import com.atlassian.oauth.Token;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.google.common.collect.ImmutableMap;

public class TokenPropertiesTest
{
    private static final Map<String, String> TOKEN_PROPERTIES = ImmutableMap.of("key1", "value1", "key2", "value2");
    private static final Token REQUEST_TOKEN = ConsumerToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
    private static final Properties REQUEST_TOKEN_AS_PROPERTIES = asProperties(REQUEST_TOKEN);
    private static final Token ACCESS_TOKEN = ConsumerToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).properties(TOKEN_PROPERTIES).build();
    private static final Properties ACCESS_TOKEN_AS_PROPERTIES = asProperties(ACCESS_TOKEN);

    @Test
    public void assertThatRequestTokenIsConvertedToPropertiesCorrectly()
    {
        assertThat(new SimpleTokenProperties(REQUEST_TOKEN).asProperties(), is(equalTo(REQUEST_TOKEN_AS_PROPERTIES)));
    }

    @Test
    public void assertThatAccessTokenIsConvertedToPropertiesCorrectly()
    {
        assertThat(new SimpleTokenProperties(ACCESS_TOKEN).asProperties(), is(equalTo(ACCESS_TOKEN_AS_PROPERTIES)));
    }

    @Test
    public void assertThatGetTokenReturnsTokenValueFromProperties()
    {
        assertThat(new SimpleTokenProperties(REQUEST_TOKEN_AS_PROPERTIES).getToken(), is(equalTo(REQUEST_TOKEN.getToken())));
    }
    
    @Test
    public void assertThatGetTokenSecretReturnsTokenSecretFromProperties()
    {
        assertThat(new SimpleTokenProperties(REQUEST_TOKEN_AS_PROPERTIES).getTokenSecret(), is(equalTo(REQUEST_TOKEN.getTokenSecret())));
    }
    
    @Test
    public void assertThatIsAccessTokenReturnsTrueForAccessTokenProperties()
    {
        assertTrue(new SimpleTokenProperties(ACCESS_TOKEN_AS_PROPERTIES).isAccessToken());
    }

    @Test
    public void assertThatIsAccessTokenReturnsFalseForRequestTokenProperties()
    {
        assertFalse(new SimpleTokenProperties(REQUEST_TOKEN_AS_PROPERTIES).isAccessToken());
    }
    
    @Test
    public void assertThatGetConsumerKeyReturnsConsumerKeyFromProperties()
    {
        assertThat(new SimpleTokenProperties(REQUEST_TOKEN_AS_PROPERTIES).getConsumerKey(), is(equalTo(REQUEST_TOKEN.getConsumer().getKey())));
    }

    @Test
    public void assertThatGetPropertiesKeyReturnsPropertiesFromTokenProperties()
    {
        assertThat(new SimpleTokenProperties(ACCESS_TOKEN_AS_PROPERTIES).getProperties(), is(equalTo(ACCESS_TOKEN.getProperties())));        
    }

    private static Properties asProperties(Token token)
    {
        Properties properties = new Properties();
        properties.put(TokenProperties.TOKEN, token.getToken());
        properties.put(TokenProperties.TOKEN_SECRET, token.getTokenSecret());
        properties.put(TokenProperties.TYPE, token.isAccessToken() ? "ACCESS" : "REQUEST");
        properties.put(TokenProperties.CONSUMER_KEY, token.getConsumer().getKey());
        if (!token.getProperties().isEmpty())
        {
            properties.put(TokenProperties.PROPERTIES, PropertiesHelper.toString(PropertiesHelper.asProperties(token.getProperties())));
        }
        return properties;
    }
    
    private static final class SimpleTokenProperties extends TokenProperties
    {
        public SimpleTokenProperties(Token token)
        {
            super(token);
        }

        public SimpleTokenProperties(Properties properties)
        {
            super(properties);
        }
    }
}
