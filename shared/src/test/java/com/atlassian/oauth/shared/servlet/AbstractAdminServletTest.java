package com.atlassian.oauth.shared.servlet;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.websudo.WebSudoManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;

@RunWith(MockitoJUnitRunner.class)
public class AbstractAdminServletTest
{
    @Mock UserManager userManager;
    @Mock MessageFactory messageFactory;
    @Mock LoginUriProvider loginUriProvider;
    @Mock WebSudoManager webSudoManager;
    
    SimpleAdminServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp()
    {
        servlet = new SimpleAdminServlet(userManager, messageFactory, loginUriProvider, webSudoManager);
    }
    
    @Test
    public void verifyThatUserIsRedirectedIfNotAnAdminWhenDoingAGet() throws Exception
    {
        URI loginUri = URI.create("http://service/login");
        
        when(request.getMethod()).thenReturn("GET");
        when(loginUriProvider.getLoginUri(isA(URI.class))).thenReturn(loginUri);
        
        servlet.service(request, response);
        
        verify(response).sendRedirect(loginUri.toString());
    }
    
    @Test
    public void verifyThatRedirectUriIsConstructedFromServletRequestUrlForGetByNonAdmin() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(request.getServletPath()).thenReturn("/myservlet");
        when(request.getQueryString()).thenReturn("a=b&c=d");
        when(loginUriProvider.getLoginUri(isA(URI.class))).thenReturn(URI.create("http://service/login"));
        
        servlet.service(request, response);
        
        verify(loginUriProvider).getLoginUri(URI.create("/myservlet?a=b&c=d"));
    }
    
    @Test
    public void assertThatRestrictedGetIsCalledIfUserIsAdmin() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.isSystemAdmin("bob")).thenReturn(true);
        webSudoManager.willExecuteWebSudoRequest(request);
        
        servlet.service(request, response);
        
        assertTrue(servlet.restrictedGetCalled);
    }
    
    @Test
    public void verifyThatPostAsNonAdminSendsUnauthorizedError() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        
        servlet.service(request, response);
        
        verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    @Test
    public void assertThatRestrictedPostIsCalledIfUserIsAdmin() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.isSystemAdmin("bob")).thenReturn(true);
        webSudoManager.willExecuteWebSudoRequest(request);
        
        servlet.service(request, response);
        
        assertTrue(servlet.restrictedPostCalled);
    }
    
    private static final class SimpleAdminServlet extends AbstractAdminServlet
    {
        boolean restrictedPostCalled = false;
        boolean restrictedGetCalled = false;
        
        public SimpleAdminServlet(UserManager userManager, MessageFactory messageFactory, LoginUriProvider loginUriProvider, WebSudoManager webSudoManager)
        {
            super(userManager, messageFactory, loginUriProvider, webSudoManager);
        }

        @Override
        protected void doRestrictedPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
        {
            restrictedPostCalled = true;
        }
        
        @Override
        protected void doRestrictedGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
        {
            restrictedGetCalled = true;
        }
    }
}
