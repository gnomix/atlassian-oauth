package com.atlassian.oauth.signaturegenerator;

import static org.apache.commons.lang.StringUtils.isBlank;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.Request.Parameter;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import static com.google.common.base.Predicates.*;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Iterables.*;

public class SignatureGeneratorServlet extends HttpServlet
{
    // the service provider won't be used for anything in the actual signing process, just to fill in the oauth accessor
    private static final ServiceProvider SP = new ServiceProvider(URI.create("http://localhost"), URI.create("http://localhost"), URI.create("http://localhost"));
    
    private final ConsumerService consumer;
    private final TemplateRenderer renderer;
    
    public SignatureGeneratorServlet(ConsumerService consumer, TemplateRenderer renderer)
    {
        this.consumer = consumer;
        this.renderer = renderer;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        renderer.render("request.vm", response.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        try
        {
            validate(request);
        }
        catch (ValidationException e)
        {
            renderer.render("request.vm", ImmutableMap.of("request", request, "errors", e.getErrors()), response.getWriter());
        }
        Request signedRequest = sign(asOAuthRequest(request), asToken(request));
        renderer.render("signature.vm", ImmutableMap.<String, Object>of(
            "request", signedRequest
        ),response.getWriter());
    }

    private Request asOAuthRequest(final HttpServletRequest request)
    {
        Iterable<Parameter> parameters = filter(transform(Arrays.asList(request.getParameterValues("p")), new Function<String, Parameter>()
        {
            public Parameter apply(String parameterNumber)
            {
                if (isBlank(request.getParameter("p" + parameterNumber)))
                {
                    return null;
                }
                return new Parameter(request.getParameter("p" + parameterNumber), request.getParameter("v" + parameterNumber));
            }
        }), notNull());
        if (!isBlank(request.getParameter("timestamp")))
        {
            parameters = concat(parameters, ImmutableList.of(new Parameter("oauth_timestamp", request.getParameter("timestamp"))));
        }
        if (!isBlank(request.getParameter("nonce")))
        {
            parameters = concat(parameters, ImmutableList.of(new Parameter("oauth_nonce", request.getParameter("nonce"))));
        }
        return new Request(HttpMethod.valueOf(request.getParameter("method")), URI.create(request.getParameter("uri")), parameters);
    }
    
    private ConsumerToken asToken(HttpServletRequest request)
    {
        return ConsumerToken.newAccessToken(request.getParameter("token"))
            .tokenSecret(request.getParameter("token-secret"))
            .consumer(consumer.getConsumer())
            .build();
    }

    private Request sign(Request request, ConsumerToken token)
    {
        return consumer.sign(request, SP, token);
    }
    
    private class ValidationException extends RuntimeException
    {
        private final Map<String, String> errors;
        
        ValidationException(Map<String, String> errors)
        {
            this.errors = errors;
        }
        
        Map<String, String> getErrors()
        {
            return errors;
        }
    }

    private void validate(HttpServletRequest request)
    {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        validateMethod(request, builder);
        validateUri(request, builder);
        validateToken(request, builder);
        validateTokenSecret(request, builder);
        
        Map<String, String> errors = builder.build();
        if (!errors.isEmpty())
        {
            throw new ValidationException(errors);
        }
    }

    private void validateTokenSecret(HttpServletRequest request, ImmutableMap.Builder<String, String> errors)
    {
        if (isBlank(request.getParameter("token-secret")))
        {
            errors.put("token-secret", "Required");
        }
    }

    private void validateToken(HttpServletRequest request, ImmutableMap.Builder<String, String> errors)
    {
        if (isBlank(request.getParameter("token")))
        {
            errors.put("token", "Required");
        }
    }

    private void validateUri(HttpServletRequest request, ImmutableMap.Builder<String, String> errors)
    {
        if (isBlank(request.getParameter("uri")))
        {
            errors.put("uri", "Required");
        }
        else
        {
            try
            {
                URI uri = new URI(request.getParameter("uri"));
                if (!uri.isAbsolute())
                {
                    errors.put("uri", "Must be absolute");
                }
                else if (!"http".equals(uri.getScheme()) && !"https".equals(uri.getScheme()))
                {
                    errors.put("uri", "Must use either http or https");
                }
            }
            catch (URISyntaxException e)
            {
                errors.put("uri", "Must be a valid URI");
            }
        }
    }

    private void validateMethod(HttpServletRequest request, ImmutableMap.Builder<String, String> errors)
    {
        if (isBlank(request.getParameter("method")))
        {
            errors.put("method", "Required");
        }
        else
        {
            try
            {
                HttpMethod.valueOf(request.getParameter("method"));
            }
            catch (IllegalArgumentException e)
            {
                errors.put("method", "Must be one of GET, POST, PUT, DELETE, HEAD, or OPTIONS");
            }
        }
    }
}
