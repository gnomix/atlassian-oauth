package com.atlassian.oauth.serviceprovider.sal;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

public final class MapBackedPluginSettings implements PluginSettings
{
    private final Map<String, Object> settings;

    public MapBackedPluginSettings(Map<String, Object> settings)
    {
        this.settings = settings;
    }

    public Object get(String key)
    {
        return settings.get(key);
    }

    public Object put(String key, Object value)
    {
        if (!(value instanceof String) && !(value instanceof Properties) && !(value instanceof List))
        {
            throw new IllegalArgumentException("value must be of type String, Properties or List, but is of type " + value.getClass().getName());
        }
        settings.put(key, value);
        return value;
    }

    public Object remove(String key)
    {
        Object value = settings.get(key);
        settings.remove(key);
        return value;
    }
}