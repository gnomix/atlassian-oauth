package com.atlassian.oauth.serviceprovider.sal;

import static com.atlassian.oauth.shared.sal.Functions.toDecodedKeys;
import static com.atlassian.oauth.shared.sal.Functions.toEncodedKeys;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;
import java.util.Set;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.StoreException;
import com.atlassian.oauth.serviceprovider.sal.PluginSettingsServiceProviderConsumerStore.Settings.ConsumerProperties;
import com.atlassian.oauth.shared.sal.AbstractSettingsProperties;
import com.atlassian.oauth.shared.sal.HashingLongPropertyKeysPluginSettings;
import com.atlassian.oauth.shared.sal.PrefixingPluginSettings;
import com.atlassian.oauth.util.Check;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

public class PluginSettingsServiceProviderConsumerStore implements ServiceProviderConsumerStore
{    
    private final PluginSettingsFactory pluginSettingsFactory;
    
    public PluginSettingsServiceProviderConsumerStore(PluginSettingsFactory factory)
    {
        pluginSettingsFactory = Check.notNull(factory, "factory");
    }

    public Consumer get(String consumerKey)
    {
        Check.notNull(consumerKey, "consumerKey");

        ConsumerProperties props = settings().getConsumerProperties(consumerKey);
        if (props == null)
        {
            return null;
        }
        
        try
        {
            try
            {
                return Consumer.key(consumerKey)
                    .name(props.getName())
                    .publicKey(props.getPublicKey())
                    .description(props.getDescription())
                    .callback(props.getCallback())
                    .build();
            }
            catch (NoSuchAlgorithmException e)
            {
                throw new StoreException(e);
            }
            catch (InvalidKeySpecException e)
            {
                throw new StoreException(e);
            }
        }
        catch (URISyntaxException e)
        {
            throw new StoreException("callback URI is not valid", e);
        }
    }

    public void put(Consumer consumer)
    {
        Check.notNull(consumer, "consumer");
        Settings settings = settings();
        settings.addConsumerKey(consumer.getKey());
        settings.putConsumerProperties(consumer.getKey(), new ConsumerProperties(consumer));
    }
    
    public void remove(String consumerKey)
    {
        Check.notNull(consumerKey, "consumerKey");
        Settings settings = settings();
        settings.removeConsumerKey(consumerKey);
        settings.removeConsumerProperties(consumerKey);
    }
    
    public Iterable<Consumer> getAll()
    {
        return transform(getConsumerKeys(), new Function<String, Consumer>()
        {
            public Consumer apply(String consumerKey)
            {
                return get(consumerKey);
            }
        });
    }

    private Iterable<String> getConsumerKeys()
    {
        return settings().getConsumerKeys();
    }
    
    private Settings settings()
    {
        return new Settings(pluginSettingsFactory.createGlobalSettings());
    }
    
    static final class Settings
    {
        private final PluginSettings settings;

        Settings(PluginSettings settings)
        {
            this.settings = new PrefixingPluginSettings(new HashingLongPropertyKeysPluginSettings(settings), ServiceProviderConsumerStore.class.getName());
        }

        ConsumerProperties getConsumerProperties(String consumerKey)
        {
            Properties props = (Properties) settings.get(consumerSettingKey(consumerKey));
            if (props == null)
            {
                return null;
            }
            return new ConsumerProperties(props);
        }
        
        void putConsumerProperties(String key, AbstractSettingsProperties consumerProperties)
        {
            // OAUTH-232:  the prefixed key could end up getting hashed, but it's not necessary to embed another
            // copy of the consumer key in the properties here, because we can always look at the main list of
            // consumer keys (which is stored under an invariant key that won't get hashed).
            settings.put(consumerSettingKey(key), consumerProperties.asProperties());
        }
        
        void removeConsumerProperties(String key)
        {
            settings.remove(consumerSettingKey(key));
        }

        private String consumerSettingKey(String key)
        {
            return "consumer." + key;
        }

        Set<String> getConsumerKeys()
        {
            String encodedKeys = (String) settings.get(Keys.CONSUMER_KEYS);
            if (encodedKeys == null)
            {
                return ImmutableSet.of();
            }
            return ImmutableSet.copyOf(transform(asList(encodedKeys.split("/")), toDecodedKeys()));
        }

        void putConsumerKeys(Iterable<String> keys)
        {
            if (isEmpty(keys))
            {
                settings.remove(Keys.CONSUMER_KEYS);
            }
            else
            {
                settings.put(Keys.CONSUMER_KEYS, Joiner.on("/").join(transform(keys, toEncodedKeys())));
            }
        }
        
        void addConsumerKey(String key)
        {
            putConsumerKeys(Sets.union(ImmutableSet.of(key), getConsumerKeys()));
        }
        
        void removeConsumerKey(String key)
        {
            putConsumerKeys(Sets.filter(getConsumerKeys(), not(equalTo(key))));
        }
        
        static final class ConsumerProperties extends AbstractSettingsProperties
        {
            static final String PUBLIC_KEY = "publicKey";
            static final String CALLBACK = "callback";
            static final String DESCRIPTION = "description";
            static final String NAME = "name";
            
            ConsumerProperties(Consumer consumer)
            {
                super();
                putName(consumer.getName());
                putPublicKey(consumer.getPublicKey());
                putDescription(consumer.getDescription());
                putCallback(consumer.getCallback());
            }

            ConsumerProperties(Properties properties)
            {
                super(properties);
            }
            
            String getName()
            {
                return get(NAME);
            }
    
            void putName(String name)
            {
                put(NAME, name);
            }
    
            PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException
            {
                return RSAKeys.fromPemEncodingToPublicKey(get(PUBLIC_KEY));
            }
    
            void putPublicKey(PublicKey publicKey)
            {
                put(PUBLIC_KEY, RSAKeys.toPemEncoding(publicKey));
            }
    
            String getDescription()
            {
                return get(DESCRIPTION);
            }
    
            void putDescription(String description)
            {
                if (description == null)
                {
                    return;
                }
                put(DESCRIPTION, description);
            }
    
            URI getCallback() throws URISyntaxException
            {
                String callback = get(CALLBACK);
                if (callback == null)
                {
                    return null;
                }
                return new URI(callback);
            }
    
            void putCallback(URI callback)
            {
                if (callback == null)
                {
                    return;
                }
                put(CALLBACK, callback.toString());
            }
        }

        static final class Keys
        {
            static final String CONSUMER_KEYS = "allConsumerKeys";
        }
    }
}
