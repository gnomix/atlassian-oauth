package com.atlassian.oauth.serviceprovider.sal;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.Properties;
import java.util.Set;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.ServiceProviderTokenBuilder;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.StoreException;
import com.atlassian.oauth.shared.sal.HashingLongPropertyKeysPluginSettings;
import com.atlassian.oauth.shared.sal.PrefixingPluginSettings;
import com.atlassian.oauth.shared.sal.TokenProperties;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserResolutionException;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session.newSession;
import static com.atlassian.oauth.shared.sal.Functions.toDecodedKeys;
import static com.atlassian.oauth.shared.sal.Functions.toEncodedKeys;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.isBlank;

public class PluginSettingsServiceProviderTokenStore implements ServiceProviderTokenStore
{
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ServiceProviderConsumerStore consumerStore;
    private final UserManager userManager;
    private final Clock clock;
    
    public PluginSettingsServiceProviderTokenStore(PluginSettingsFactory factory,
        ServiceProviderConsumerStore consumerStore, 
        UserManager userManager,
        Clock clock)
    {
        this.pluginSettingsFactory = Check.notNull(factory, "factory");
        this.consumerStore = Check.notNull(consumerStore, "consumerStore");
        this.userManager = Check.notNull(userManager, "userManager");
        this.clock = Check.notNull(clock, "clock");
    }

    public ServiceProviderToken get(String token)
    {
        return get(token, resolveUser());
    }

    ServiceProviderToken get(String token, Function<String, Principal> userResolver)
    {
        Check.notNull(token, "token");
        ServiceProviderTokenProperties props = settings().get(token);
        if (props == null)
        {
            return null;
        }

        Principal user = null;
        if (props.getUserName() != null)
        {
            try
            {
                user = userResolver.apply(props.getUserName());
                
                //user will be null if they have been deleted. the token is invalid
                if (user == null)
                {
                    remove(token);
                    throw new InvalidTokenException("Unknown user " + props.getUserName());
                }
            }
            catch (UserResolutionException e)
            {
                throw new InvalidTokenException("Unknown user " + props.getUserName(), e);
            }
        }
        else if (props.isAccessToken())
        {
            throw new StoreException("Token '" + token + "' is an access token, but has no user associated with it");
        }
        
        if (props.isAccessToken())
        {
            return ServiceProviderToken.newAccessToken(token)
                .tokenSecret(props.getTokenSecret())
                .consumer(consumerStore.get(props.getConsumerKey()))
                .authorizedBy(user)
                .creationTime(props.getCreationTime())
                .timeToLive(props.getTimeToLive())
                .properties(props.getProperties())
                .session(props.getSession())
                .build();
        }
        else
        {
            Version version;
            if (props.getVersion() == null)
            {
                if (props.getCallback() == null)
                {
                    version = Version.V_1_0;
                }
                else
                {
                    version = Version.V_1_0_A;
                }
            }
            else
            {
                version = props.getVersion();
            }
            ServiceProviderTokenBuilder builder = ServiceProviderToken.newRequestToken(token)
                .tokenSecret(props.getTokenSecret())
                .consumer(consumerStore.get(props.getConsumerKey()))
                .callback(props.getCallback())
                .creationTime(props.getCreationTime())
                .timeToLive(props.getTimeToLive())
                .version(version)
                .properties(props.getProperties());
            if (props.getAuthorization() == Authorization.AUTHORIZED)
            {
                builder = builder.authorizedBy(user).verifier(props.getVerifier());
            }
            else if (props.getAuthorization() == Authorization.DENIED)
            {
                builder = builder.deniedBy(user);
            }
            return builder.build();
        }
    }
    
    public Iterable<ServiceProviderToken> getAccessTokensForUser(String username)
    {
        return transform(settings().getUserAccessTokenKeys(username), toTokens(resolveUser()));
    }

    public ServiceProviderToken put(ServiceProviderToken token)
    {
        Check.notNull(token, "token");
        settings().put(token.getToken(), new ServiceProviderTokenProperties(token));
        return token;
    }

    public void remove(String token)
    {
        Check.notNull(token, "token");
        settings().remove(token);
    }
    
    public void removeExpiredTokens()
    {
        removeTokens(hasExpired());
    }
    
    public void removeExpiredSessions()
    {
        removeTokens(hasExpiredSession());
    }
    
    private void removeTokens(Predicate<ServiceProviderToken> p)
    {
        Settings settings = settings();
        Iterable<ServiceProviderToken> tokens = transform(settings.getTokenKeys(settings.validTokenReferences()), toTokens(doNotResolveUser()));
        for (ServiceProviderToken token : filter(tokens, p))
        {
            settings.remove(token.getToken());
        }
    }
    
    public void removeByConsumer(String consumerKey)
    {
        Settings settings = settings();
        for (ServiceProviderToken token : transform(settings.getConsumerTokens(consumerKey, settings.validTokenReferences()), toTokens(doNotResolveUser())))
        {
            settings.remove(token.getToken());
        }
    }

    private Function<String, Principal> resolveUser()
    {
        return new Function<String, Principal>()
        {
            public Principal apply(final String username)
            {
                Principal user;
                try
                {
                    user = userManager.resolve(username);
                }
                catch (UserResolutionException e)
                {
                    throw new InvalidTokenException("Unknown user " + username, e);
                }
                return user;
            }
        };
    }

    private Function<String, Principal> doNotResolveUser()
    {
        return new Function<String, Principal>()
        {
            public Principal apply(final String username)
            {
                return new Principal()
                {
                    public String getName()
                    {
                        return username;
                    }
                };
            }
        };
    }
    
    private Function<String, ServiceProviderToken> toTokens(Function<String, Principal> userResolver)
    {
        return new KeyToToken(userResolver);
    }
    
    private class KeyToToken implements Function<String, ServiceProviderToken>
    {
        private Function<String, Principal> userResolver;

        private KeyToToken(Function<String, Principal> userResolver)
        {
            this.userResolver = userResolver;
        }

        public ServiceProviderToken apply(String tokenKey)
        {
            return get(tokenKey, userResolver);
        }
    }

    private Predicate<ServiceProviderToken> hasExpired()
    {
        return new HasExpired(clock);
    }

    private static class HasExpired implements Predicate<ServiceProviderToken>
    {
        private final Clock clock;

        public HasExpired(Clock clock)
        {
            this.clock = clock;
        }

        public boolean apply(ServiceProviderToken token)
        {
            return token.getSession() == null && token.hasExpired(clock);
        }
    }
    
    private Predicate<ServiceProviderToken> hasExpiredSession()
    {
        return new HasExpiredSession(clock);
    }
    
    private static class HasExpiredSession implements Predicate<ServiceProviderToken>
    {
        private final Clock clock;
        
        public HasExpiredSession(Clock clock)
        {
            this.clock = clock;
        }

        public boolean apply(ServiceProviderToken token)
        {
            return token.getSession() != null && token.getSession().hasExpired(clock);
        }
    }

    private Settings settings()
    {
        return new Settings(pluginSettingsFactory.createGlobalSettings());
    }
    
    /**
     * Low-level PluginSettings-based implementation of token storage for service providers.  Maintains
     * a master list of all known tokens, as well as sublists of token keys associated with a username
     * or a consumer key.
     * <p>
     * Per OAUTH-232, we now use HashingLongPropertyKeysPluginSettings to ensure that all PluginSettings
     * keys are <= 100 characters long even when they have a variable-length part such as a username.
     * If a key ends up being too long, part of it gets changed to a hash.  This means that the original
     * username/consumer key may no longer be discoverable from the PluginSettings key, which becomes a
     * problem if (when) we stop using PluginSettings and need to migrate the data; so we are now
     * storing the username/consumer key redundantly as part of a property set.  Token lists stored by
     * older versions of OAuth won't have that property set, but that's OK because the keys for those
     * lists won't have been hashed (since older versions of OAuth would just fail if a key was too long). 
     */
    static final class Settings
    {
        static final String TOKEN_KEYS = "tokenKeys";
        static final String TOKEN_PREFIX = "token";
        static final String KEY_LIST_PROPERTY = "keys";
        static final String USER_ACCESS_TOKENS = "userAccessTokens";
        static final String USER_ACCESS_TOKENS_USERNAME_PROPERTY = "username";
        static final String CONSUMER_TOKENS = "consumerTokens";
        static final String CONSUMER_TOKENS_CONSUMER_KEY_PROPERTY = "consumerKey";
        
        private final PluginSettings settings;
        private final Predicate<String> isValidTokenReference;
        
        Settings(PluginSettings settings)
        {
            this.settings = new PrefixingPluginSettings(new HashingLongPropertyKeysPluginSettings(settings), ServiceProviderTokenStore.class.getName());
            this.isValidTokenReference = new IsValidTokenReference(this.settings);
        }

        ServiceProviderTokenProperties get(String token)
        {
            Properties props = (Properties) settings.get(TOKEN_PREFIX + "." + token);
            if (props == null)
            {
                return null;
            }
            return new ServiceProviderTokenProperties(props);
        }

        void put(String token, ServiceProviderTokenProperties tokenProperties)
        {
            settings.put(TOKEN_PREFIX + "."  + token, tokenProperties.asProperties());
            addTokenKey(token);
            addConsumerToken(tokenProperties.getConsumerKey(), token);
            if (tokenProperties.isAccessToken())
            {
                addUserAccessToken(tokenProperties.getUserName(), token);
            }
        }

        void remove(String token)
        {
            ServiceProviderTokenProperties tokenProperties = get(token);
            if (tokenProperties == null)
            {
                return;
            }
            settings.remove(TOKEN_PREFIX + "." + token);
            removeTokenKey(token);
            removeConsumerToken(tokenProperties.getConsumerKey(), token);
            if (tokenProperties.isAccessToken())
            {
                removeUserAccessToken(tokenProperties.getUserName(), token);
            }
        }

        Set<String> getTokenKeys(Predicate<String> tokenReferenceValidator)
        {
            return getTokenKeySet(TOKEN_KEYS, tokenReferenceValidator);
        }
        
        private void putTokenKeys(Iterable<String> tokenKeys)
        {
            putTokenKeySet(TOKEN_KEYS, tokenKeys);
        }

        private void addTokenKey(String token)
        {
            putTokenKeys(Sets.union(ImmutableSet.of(token), getTokenKeys(validTokenReferences())));
        }

        private void removeTokenKey(String token)
        {
            putTokenKeys(Sets.filter(getTokenKeys(allTokenReferences()), not(equalTo(token))));
        }
        
        Set<String> getUserAccessTokenKeys(String user)
        {
            return getTokenKeySet(USER_ACCESS_TOKENS + "." + user, validTokenReferences());
        }
        
        private void putUserAccessTokens(String user, Set<String> tokenKeys)
        {
            putTokenKeySet(USER_ACCESS_TOKENS + "." + user, tokenKeys, USER_ACCESS_TOKENS_USERNAME_PROPERTY, user);
        }
        
        private void addUserAccessToken(String userName, String tokenKey)
        {
            putUserAccessTokens(userName, Sets.union(ImmutableSet.of(tokenKey), getUserAccessTokenKeys(userName)));
        }
        
        private void removeUserAccessToken(String userName, String tokenKey)
        {
            putUserAccessTokens(userName, Sets.filter(getUserAccessTokenKeys(userName), not(equalTo(tokenKey))));
        }
        
        Set<String> getConsumerTokens(String consumerKey, Predicate<String> tokenReferenceValidator)
        {
            return getTokenKeySet(CONSUMER_TOKENS + "." + consumerKey, tokenReferenceValidator);
        }
        
        private void putConsumerTokens(String consumerKey, Iterable<String> tokenKeys)
        {
            putTokenKeySet(CONSUMER_TOKENS + "." + consumerKey, tokenKeys, CONSUMER_TOKENS_CONSUMER_KEY_PROPERTY, consumerKey);
        }
        
        private void addConsumerToken(String consumerKey, String tokenKey)
        {
            putConsumerTokens(consumerKey, Sets.union(ImmutableSet.of(tokenKey), getConsumerTokens(consumerKey, validTokenReferences())));
        }
        
        private void removeConsumerToken(String consumerKey, String tokenKey)
        {
            putConsumerTokens(consumerKey, Sets.filter(getConsumerTokens(consumerKey, allTokenReferences()), not(equalTo(tokenKey))));
        }
        
        private Set<String> getTokenKeySet(String setKey, Predicate<String> tokenReferenceValidator)
        {
            Object value = settings.get(setKey);
            String tokenKeys;
            // OAUTH-232:  Transparently handle settings that were stored by earlier versions
            if (value == null)
            {
                return ImmutableSet.of();
            }
            else if (value instanceof String)
            {
                tokenKeys = (String) value;
            }
            else if (value instanceof Properties)
            {
                tokenKeys = (String) ((Properties) value).get(KEY_LIST_PROPERTY);
            }
            else
            {
                throw new IllegalStateException("unexpected value of class " + value.getClass() + " for key " + setKey);
            }
            if (isBlank(tokenKeys))
            {
                return ImmutableSet.of();
            }
            return ImmutableSet.copyOf(filter(transform(asList(tokenKeys.split("\\/")), toDecodedKeys()), tokenReferenceValidator));
        }

        private void putTokenKeySet(String setKey, Iterable<String> tokenSet)
        {
            // store a key set as just a delimited string
            settings.put(setKey, Joiner.on("/").join(transform(tokenSet, toEncodedKeys())));
        }
        
        private void putTokenKeySet(String setKey, Iterable<String> tokenSet, String idPropertyName, String idPropertyValue)
        {
            // store a key set as a property set containing (a) a delimited string and (b) an
            // identifier that says what this key set is for
            String keyListString = Joiner.on("/").join(transform(tokenSet, toEncodedKeys()));
            Properties props = new Properties();
            props.put(KEY_LIST_PROPERTY, keyListString);
            props.put(idPropertyName, idPropertyValue);
            settings.put(setKey, props);
        }
        
        private Predicate<String> validTokenReferences()
        {
            return isValidTokenReference;
        }

        private Predicate<String> allTokenReferences()
        {
            return Predicates.alwaysTrue();
        }

        final class IsValidTokenReference implements Predicate<String>
        {
            private final PluginSettings settings;
            
            IsValidTokenReference(PluginSettings settings)
            {
                this.settings = settings;
            }

            public boolean apply(String token)
            {
                return settings.get(TOKEN_PREFIX + "." + token) != null;
            }
        }
    }
    
    static final class ServiceProviderTokenProperties extends TokenProperties
    {
        static final String AUTHORIZATION = "authorization";
        static final String USER_NAME = "userName";
        static final String VERIFIER = "verifier";
        static final String CALLBACK = "callback";
        static final String CREATION_TIME = "creationTime";
        static final String TIME_TO_LIVE = "timeToLive";
        static final String VERSION = "version";
        static final String SESSION_HANDLE = "session.handle";
        static final String SESSION_CREATION_TIME = "session.creationTime";
        static final String SESSION_LAST_RENEWAL_TIME = "session.lastRenewalTime";
        static final String SESSION_TIME_TO_LIVE = "session.timeToLive";
        
        public ServiceProviderTokenProperties(Properties properties)
        {
            super(properties);
        }

        public ServiceProviderTokenProperties(ServiceProviderToken token)
        {
            super(token);
            putAuthorization(token.getAuthorization());
            if (token.getUser() != null)
            {
                putUserName(token.getUser().getName());
            }
            putVerifier(token.getVerifier());
            putCallback(token.getCallback());
            putCreationTime(token.getCreationTime());
            putTimeToLive(token.getTimeToLive());
            putVersion(token.getVersion());
            putSession(token.getSession());
        }

        public Authorization getAuthorization()
        {
            String authz = get(AUTHORIZATION);
            if (authz != null)
            {
                return Authorization.valueOf(authz);
            }
            // gracefully deal with older installations
            return getUserName() != null ? Authorization.AUTHORIZED : Authorization.NONE;
        }
        
        public void putAuthorization(Authorization authz)
        {
            put(AUTHORIZATION, authz.name());
        }

        /**
         * Returns the user name the token belongs to.
         * 
         * @return the user name the token belongs to
         */
        public String getUserName()
        {
            return get(USER_NAME);
        }

        private void putUserName(String name)
        {
            put(USER_NAME, name);
        }
        
        /**
         * Returns the verifier for the token.
         * 
         * @return verifier for the token
         */
        public String getVerifier()
        {
            return get(VERIFIER);
        }
        
        private void putVerifier(String verifier)
        {
            put(VERIFIER, verifier);
        }
        
        /**
         * Returns the callback for the token.
         * 
         * @return callback for the token
         */
        public URI getCallback()
        {
            String callback = get(CALLBACK);
            if (callback == null)
            {
                return null;
            }
            try
            {
                return new URI(callback);
            }
            catch (URISyntaxException e)
            {
                throw new StoreException("Invalid callback", e);
            }
        }
        
        private void putCallback(URI callback)
        {
            if (callback == null)
            {
                return;
            }
            put(CALLBACK, callback.toString());
        }
        
        /**
         * Returns the creation time of the token.
         * 
         * @return creation time of the token
         */
        public long getCreationTime()
        {
            return Long.parseLong(get(CREATION_TIME));
        }
        
        private void putCreationTime(long creationTime)
        {
            put(CREATION_TIME, Long.toString(creationTime));
        }
        
        /**
         * Returns the time to live of the token.
         * 
         * @return time to live of the token
         */
        public long getTimeToLive()
        {
            return Long.parseLong(get(TIME_TO_LIVE));
        }
        
        private void putTimeToLive(long timeToLive)
        {
            put(TIME_TO_LIVE, Long.toString(timeToLive));
        }

        /**
         * Returns the version of the token
         * 
         * @return version of the token
         */
        public Version getVersion()
        {
            String version = get(VERSION);
            if (version == null)
            {
                return null;
            }
            return Version.valueOf(version);
        }

        private void putVersion(Version version)
        {
            if (version != null)
            {
                put(VERSION, version.name());
            }
        }
        
        /**
         * Returns the session for the token
         */
        public Session getSession()
        {
            String handle = get(SESSION_HANDLE);
            if (handle == null)
            {
                return null;
            }
            return newSession(handle).
                creationTime(Long.parseLong(get(SESSION_CREATION_TIME))).
                lastRenewalTime(Long.parseLong(get(SESSION_LAST_RENEWAL_TIME))).
                timeToLive(Long.parseLong(get(SESSION_TIME_TO_LIVE))).
                build();
        }
        
        private void putSession(Session session)
        {
            if (session != null)
            {
                put(SESSION_HANDLE, session.getHandle());
                put(SESSION_CREATION_TIME, Long.toString(session.getCreationTime()));
                put(SESSION_LAST_RENEWAL_TIME, Long.toString(session.getLastRenewalTime()));
                put(SESSION_TIME_TO_LIVE, Long.toString(session.getTimeToLive()));
            }
        }
    }
}
