package com.atlassian.oauth.serviceprovider.internal.servlet;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER;
import static net.oauth.OAuth.OAUTH_CALLBACK;
import static net.oauth.OAuth.OAUTH_CONSUMER_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.isNull;
import static org.mockito.Matchers.same;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.net.URI;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.internal.util.Paths;
import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.ByteArrayServletOutputStream;
import com.atlassian.oauth.serviceprovider.internal.OAuthConverter;
import com.atlassian.oauth.serviceprovider.internal.PassThruTransactionTemplate;
import com.atlassian.oauth.serviceprovider.internal.TokenFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.ImmutableMap;

@RunWith(MockitoJUnitRunner.class)
public class RequestTokenServletTest
{
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(V_1_0_A).build();
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN_V1 = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(V_1_0).build();

    @Mock ServiceProviderConsumerStore consumerStore;
    @Mock ServiceProviderTokenStore tokenStore;
    @Mock TokenFactory factory;
    @Mock OAuthValidator validator;
    @Mock OAuthConverter converter;
    @Mock ApplicationProperties applicationProperties;
    TransactionTemplate transactionTemplate;
    
    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    ByteArrayOutputStream responseStream;
    
    @Before
    public void setUp() throws Exception
    {
        transactionTemplate = new PassThruTransactionTemplate();

        responseStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ByteArrayServletOutputStream(responseStream));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://test/" + Paths.REQUEST_TOKEN));
        
        servlet = new RequestTokenServlet(consumerStore, tokenStore, factory, validator, converter, applicationProperties, transactionTemplate);
    }

    @Test
    public void verifyThatRequestTokenIsGeneratedStoredAndSentInResponse() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() },
            OAUTH_CALLBACK, new String[] { "http://consumer/callback" }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        when(tokenStore.put(UNAUTHORIZED_REQUEST_TOKEN)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(factory.generateRequestToken(same(RSA_CONSUMER), eq(URI.create("http://consumer/callback")), isA(OAuthMessage.class), eq(V_1_0_A)))
            .thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        
        servlet.service(request, response);
        
        verify(tokenStore).put(same(UNAUTHORIZED_REQUEST_TOKEN));
        verify(response).setContentType("text/plain");
        assertThat(responseStream.toString(), allOf(
            containsString("oauth_token=1234"),
            containsString("oauth_token_secret=5678"),
            containsString("oauth_callback_confirmed=true")
        ));
    }

    @Test
    public void verifyThatVersion1RequestTokenIsSentWhenNoOAuthCallbackParameterIsProvided() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        when(tokenStore.put(UNAUTHORIZED_REQUEST_TOKEN_V1)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN_V1);
        when(factory.generateRequestToken(same(RSA_CONSUMER), (URI) isNull(), isA(OAuthMessage.class), eq(V_1_0)))
            .thenReturn(UNAUTHORIZED_REQUEST_TOKEN_V1);
        
        servlet.service(request, response);
        
        verify(tokenStore).put(same(UNAUTHORIZED_REQUEST_TOKEN_V1));
        verify(response).setContentType("text/plain");
        assertThat(responseStream.toString(), allOf(
            containsString("oauth_token=1234"),
            containsString("oauth_token_secret=5678"),
            not(containsString("oauth_callback_confirmed="))
        ));
    }
    
    @Test
    public void verifyThatConsumerKeyUnknownResponseIsSentForInvalidConsumerKey() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { "invalid-consumer-key" },
            OAUTH_CALLBACK, new String[] { "http://consumer/callback" }
        ));

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=consumer_key_unknown")));
    }
    
    @Test
    public void verifyThatSignatureInvalidResponseIsSentWhenThereIsAProblemValidatingTheMessage() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() },
            OAUTH_CALLBACK, new String[] { "http://consumer/callback" }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        when(converter.toOAuthConsumer(RSA_CONSUMER)).thenReturn(RSA_OAUTH_CONSUMER);
        doThrow(new OAuthProblemException("signature_invalid"))
            .when(validator).validateMessage(isA(OAuthMessage.class), isA(OAuthAccessor.class));

        servlet.service(request, response);
        
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=signature_invalid")));
    }
        
    @Test
    public void verifyThatParameterRejectedResponseIsSentWhenOAutCallbackIsNotAValidUri() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() },
            OAUTH_CALLBACK, new String[] { "an invalid uri" }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        
        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), containsString("oauth_problem=parameter_rejected"));
    }
    
    @Test
    public void verifyThatAnOutOfBandCallbackValueIsAcceptedAndRequestTokenReturned() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() },
            OAUTH_CALLBACK, new String[] { "oob" }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        when(tokenStore.put(UNAUTHORIZED_REQUEST_TOKEN)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(factory.generateRequestToken(same(RSA_CONSUMER), (URI) isNull(), isA(OAuthMessage.class), eq(V_1_0_A)))
            .thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        
        servlet.service(request, response);
        
        verify(tokenStore).put(same(UNAUTHORIZED_REQUEST_TOKEN));
        verify(response).setContentType("text/plain");
        assertThat(responseStream.toString(), allOf(
            containsString("oauth_token=1234"),
            containsString("oauth_token_secret=5678"),
            containsString("oauth_callback_confirmed=true")
        ));
    }
    
    @Test
    public void verifyThatParameterRejectedResponseIsSentWhenOAutCallbackIsNotAnAbsoluteUri() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() },
            OAUTH_CALLBACK, new String[] { "/path/to/callback" }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        
        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), containsString("oauth_problem=parameter_rejected"));
    }

    
    @Test
    public void verifyThatParameterRejectedResponseIsSentWhenOAutCallbackDoesNotHaveAnHttpOrHttpsScheme() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_CONSUMER_KEY, new String[] { RSA_CONSUMER.getKey() },
            OAUTH_CALLBACK, new String[] { "ftp://crazy/callback" }
        ));
        when(consumerStore.get(RSA_CONSUMER.getKey())).thenReturn(RSA_CONSUMER);
        
        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), containsString("oauth_problem=parameter_rejected"));
    }
}
