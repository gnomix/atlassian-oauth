package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.internal.util.Paths;
import net.oauth.OAuthProblemException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.internal.ByteArrayServletOutputStream;
import com.atlassian.oauth.serviceprovider.internal.PassThruTransactionTemplate;
import com.atlassian.oauth.serviceprovider.internal.servlet.TokenLoader;
import com.atlassian.oauth.testing.FakeI18nResolver;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.templaterenderer.TemplateRenderer;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizeServletTest
{
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(V_1_0_A).build();

    @Mock AuthorizationRequestProcessor get;
    @Mock AuthorizationRequestProcessor post;
    @Mock TokenLoader loader;
    @Mock LoginRedirector loginRedirector;
    @Mock ApplicationProperties applicationProperties;
    @Mock TemplateRenderer templateRenderer;
    I18nResolver i18nResolver = new FakeI18nResolver();
    TransactionTemplate transactionTemplate = new PassThruTransactionTemplate();
    
    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    ByteArrayOutputStream responseStream;
    
    @Before
    public void setUp() throws Exception
    {
        responseStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ByteArrayServletOutputStream(responseStream));
        when(response.getWriter()).thenReturn(new PrintWriter(new OutputStreamWriter(responseStream)));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://test/" + Paths.AUTHORIZE));
        
        servlet = new AuthorizeServlet(
            get, post, loader, loginRedirector, applicationProperties, transactionTemplate, templateRenderer, i18nResolver);
    }
    
    @Test
    public void verifyThatGetWithValidTokenAndNonLoggedInUserRedirectsToLoginUri() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(loader.getTokenForAuthorization(request)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(loginRedirector.isLoggedIn(request)).thenReturn(false);
        
        servlet.service(request, response);
        
        verify(loginRedirector).redirectToLogin(request, response);
    }
    
    @Test
    public void verifyThatGetWithValidTokenAndLoggedInUserCausesGetProcessorToBeCalled() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(loader.getTokenForAuthorization(request)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(loginRedirector.isLoggedIn(request)).thenReturn(true);
        
        servlet.service(request, response);
        
        verify(get).process(request, response, UNAUTHORIZED_REQUEST_TOKEN);
    }
    
    @Test
    public void verifyThatCorrectOAuthProblemResponseIsWrittenWhenTokenLoaderThrowsExceptionDuringGet() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(loader.getTokenForAuthorization(request)).thenThrow(new OAuthProblemException("some_problem"));

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith("text/html"));
        verify(templateRenderer).render(
            eq("templates/auth/authorize-error.vm"), 
            hasMessage("com.atlassian.oauth.serviceprovider.authorize.error.generic"),
            isA(PrintWriter.class)
        );
    }

    @Test
    public void verifyThatCorrectOAuthProblemResponseIsWrittenWhenTokenLoaderThrowsExceptionDuringPost() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(loader.getTokenForAuthorization(request)).thenThrow(new OAuthProblemException("some_problem"));

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith("text/html"));
        verify(templateRenderer).render(
            eq("templates/auth/authorize-error.vm"), 
            hasMessage("com.atlassian.oauth.serviceprovider.authorize.error.generic"),
            isA(PrintWriter.class)
        );
    }

    @Test
    public void verifyThatPostWithValidTokenAndNonLoggedInUserRedirectsToLoginUri() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(loader.getTokenForAuthorization(request)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        
        servlet.service(request, response);
        
        verify(loginRedirector).redirectToLogin(request, response);
    }
    
    @Test
    public void verifyThatPostWithApproveParameterAndValidTokenAndLoggedInUserInvokesPostAuthorizationProcessor() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(loader.getTokenForAuthorization(request)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(loginRedirector.isLoggedIn(request)).thenReturn(true);

        servlet.service(request, response);
        
        verify(post).process(request, response, UNAUTHORIZED_REQUEST_TOKEN);
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Object> hasMessage(String message)
    {
        return (Map<String, Object>) argThat(hasEntry("message", (Object) message));
    }
}
