package com.atlassian.oauth.serviceprovider.internal;

import java.net.URI;

import net.oauth.OAuth;
import net.oauth.OAuthMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.Request;
import com.atlassian.oauth.Token;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.serviceprovider.TokenPropertiesFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session.newSession;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static com.atlassian.oauth.serviceprovider.TokenPropertiesFactory.ALTERNAME_CONSUMER_NAME;
import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TokenFactoryImplTest
{
    private static final String FIRST_RANDOM_VALUE = "9876";
    private static final String SECOND_RANDOM_VALUE = "5432";
    private static final URI CALLBACK = URI.create("http://consumer/callback");
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER).verifier(FIRST_RANDOM_VALUE).version(V_1_0_A).build();
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER).session(newSession("abcd").build()).build();

    @Mock TokenPropertiesFactory propertiesFactory;
    @Mock Randomizer randomizer;
    
    TokenFactory tokenFactory;
    
    OAuthMessage message = new OAuthMessage("GET", "http://sp/", ImmutableList.<OAuth.Parameter>of());
    
    @Before
    public void setUp()
    {
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn(FIRST_RANDOM_VALUE).thenReturn(SECOND_RANDOM_VALUE);

        tokenFactory = new TokenFactoryImpl(propertiesFactory, randomizer);
    }
    
    @Test
    public void assertThatTokenReturnedByGenerateRequestTokenIsAnUnauthorizedRequestToken()
    {
        ServiceProviderToken token = tokenFactory.generateRequestToken(RSA_CONSUMER, CALLBACK, message, V_1_0_A);
        assertThat(token.getAuthorization(), is(equalTo(Authorization.NONE)));
    }
    
    @Test
    public void assertThatRequestTokenValueIsGeneratedFromRandomizer()
    {
        Token token = tokenFactory.generateRequestToken(RSA_CONSUMER, CALLBACK, message, V_1_0_A);
        assertThat(token.getToken(), is(equalTo(FIRST_RANDOM_VALUE)));
    }

    @Test
    public void assertThatTokenSecretIsGeneratedFromRandomizer()
    {
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("1234").thenReturn("5678");
        Token token = tokenFactory.generateRequestToken(RSA_CONSUMER, CALLBACK, message, V_1_0_A);
        assertThat(token.getTokenSecret(), is(equalTo("5678")));
    }
    
    @Test
    public void assertThatGeneratingNewRequestTokenUsesConsumer()
    {
        Token token = tokenFactory.generateRequestToken(RSA_CONSUMER, CALLBACK, message, V_1_0_A);
        assertThat(token.getConsumer(), is(equalTo(RSA_CONSUMER)));
    }
    
    @Test
    public void assertThatGeneratingNewRequestTokenUsesMessageProperties()
    {
        when(propertiesFactory.newRequestTokenProperties(isA(Request.class))).thenReturn(ImmutableMap.of("xoauth_app_url", "http://container/gadget.xml"));
        Token token = tokenFactory.generateRequestToken(RSA_CONSUMER, CALLBACK, message, V_1_0_A);
        assertThat(token.getProperties(), hasEntry("xoauth_app_url", "http://container/gadget.xml"));
    }
    
    @Test
    public void assertThatGeneratingNewRequestTokenWithNullCallbackReturnsRequestTokenWithNullCallback()
    {
        ServiceProviderToken token = tokenFactory.generateRequestToken(RSA_CONSUMER, null, message, V_1_0_A);
        assertNull("callback is not null", token.getCallback());
    }
    
    @Test
    public void assertThatTokenReturnedByGenerateAccessTokenIsAnAccessToken()
    {
        Token token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        assertTrue(token.isAccessToken());
    }
    
    @Test
    public void assertThatNewlyGeneratedRequestTokenHasTheRightVersion()
    {
        ServiceProviderToken token = tokenFactory.generateRequestToken(RSA_CONSUMER, null, message, V_1_0);
        assertThat(token.getVersion(), is(equalTo(V_1_0)));
    }

    @Test(expected=NullPointerException.class)
    public void assertThatNullPointerExceptionIsThrownIfVersionIsNull()
    {
        tokenFactory.generateRequestToken(RSA_CONSUMER, null, message, null);
    }
    
    @Test
    public void assertThatGeneratedAccessTokenHasSameConsumerAsRequestToken()
    {
        Token token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        assertThat(token.getConsumer(), is(sameInstance(AUTHORIZED_REQUEST_TOKEN.getConsumer())));
    }

    @Test
    public void assertThatGeneratedAccessTokenHasSameTokenSecretAsRequestToken()
    {
        Token token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        assertThat(token.getTokenSecret(), is(equalTo(AUTHORIZED_REQUEST_TOKEN.getTokenSecret())));
    }

    @Test
    public void assertThatAccessTokenStringIsGeneratedFromRandomizer()
    {
        Token token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        assertThat(token.getToken(), is(equalTo(FIRST_RANDOM_VALUE)));
    }

    @Test
    public void assertThatGeneratedAccessTokenHasDifferentTokenAsRequestToken()
    {
        Token token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        assertThat(token.getToken(), is(not(equalTo(AUTHORIZED_REQUEST_TOKEN.getToken()))));
    }
    
    @Test
    public void assertThatGeneratedAccessTokenHasSameUserAsRequestToken()
    {
        ServiceProviderToken token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        assertThat(token.getUser(), is(sameInstance(AUTHORIZED_REQUEST_TOKEN.getUser())));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void assertThatGeneratingAnAccessTokenForAnUnauthorizedRequestTokenThrowsIllegalArgumentException()
    {
        ServiceProviderToken unauthorizedRequestToken = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(V_1_0_A).build();
        tokenFactory.generateAccessToken(unauthorizedRequestToken);
    }

    @Test
    public void assertThatGeneratingNewAccessTokenUsesAccessTokenPropertiesFromFactory()
    {
        when(propertiesFactory.newAccessTokenProperties(AUTHORIZED_REQUEST_TOKEN))
            .thenReturn(ImmutableMap.of(ALTERNAME_CONSUMER_NAME, "Monkey"));
        
        Token token = tokenFactory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN);
        
        assertThat(token.getProperties(), hasEntry(ALTERNAME_CONSUMER_NAME, "Monkey"));
    }
    
    @Test
    public void assertThatGeneratingNewAccessTokenForExistingAccessTokenCopiesSessionCreationTime()
    {
        ServiceProviderToken token = tokenFactory.generateAccessToken(ACCESS_TOKEN);
        assertThat(token.getSession().getCreationTime(), is(equalTo(ACCESS_TOKEN.getSession().getCreationTime())));
    }
    
    @Test
    public void assertThatGeneratingNewAccessTokenForExistingAccessTokenUsesSameUser()
    {
        ServiceProviderToken token = tokenFactory.generateAccessToken(ACCESS_TOKEN);
        assertThat(token.getUser(), is(sameInstance(ACCESS_TOKEN.getUser())));
    }

    @Test
    public void assertThatGeneratingNewAccessTokenForExistingAccessTokenUsesSameConsumer()
    {
        ServiceProviderToken token = tokenFactory.generateAccessToken(ACCESS_TOKEN);
        assertThat(token.getConsumer(), is(sameInstance(ACCESS_TOKEN.getConsumer())));
    }
    
    @Test
    public void assertThatGeneratingNewAccessTokenForExistingAccessTokenUpdatesSessionLastRenewalTime() throws Exception
    {
        Thread.sleep(1);  // make sure the time is different
        ServiceProviderToken token = tokenFactory.generateAccessToken(ACCESS_TOKEN);
        assertThat(token.getSession().getLastRenewalTime(), is(greaterThan(ACCESS_TOKEN.getSession().getLastRenewalTime())));
    }
}
