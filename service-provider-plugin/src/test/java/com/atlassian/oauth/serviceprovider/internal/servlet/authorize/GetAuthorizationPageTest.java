package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetAuthorizationPageTest
{
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(Version.V_1_0_A).build();
    
    @Mock AuthorizationRenderer renderer;
    
    AuthorizationRequestProcessor get;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp()
    {
        get = new GetAuthorizationPage(renderer);
    }
    
    @Test
    public void verifyThatProcessInvokesRenderer() throws Exception
    {
        get.process(request, response, UNAUTHORIZED_REQUEST_TOKEN);
        
        verify(renderer).render(request, response, UNAUTHORIZED_REQUEST_TOKEN);
    }
}
