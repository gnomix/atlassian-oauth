package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public final class PassThruTransactionTemplate implements TransactionTemplate
{
    public Object execute(TransactionCallback action)
    {
        return action.doInTransaction();
    }
}