package com.atlassian.oauth.serviceprovider.internal.servlet.user;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Locale;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.servlet.authorize.LoginRedirector;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccessTokensServletTest
{
    private static final String TOKEN_VALUE = "1234";
    private static final String TOKEN_SECRET = "5678";
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN_VALUE)
        .tokenSecret(TOKEN_SECRET)
        .consumer(RSA_CONSUMER)
        .authorizedBy(USER)
        .build();

    @Mock ServiceProviderTokenStore store;
    @Mock UserManager userManager;
    @Mock LoginRedirector loginRedirector;
    @Mock TemplateRenderer templateRenderer;
    @Mock LocaleResolver localeResolver;

    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    ByteArrayOutputStream responseOutputStream;
    
    @Before
    public void setUp() throws Exception
    {
        responseOutputStream = new ByteArrayOutputStream();
        when(response.getWriter()).thenReturn(new PrintWriter(responseOutputStream));
        servlet = new AccessTokensServlet(store, userManager, localeResolver, loginRedirector, templateRenderer);
    }
    
    @Test
    public void verifyThatUnauthenticatedGetRequestIsRedirectedToLoginUri() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        
        servlet.service(request, response);
        
        verify(loginRedirector).redirectToLogin(request, response);
    }
    
    @Test
    public void verifyThatUnauthenticatedPostRequestCausesUnauthorizedResponse() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        
        servlet.service(request, response);
        
        verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    @Test
    public void verifyThatRendererIsCalledForAuthenticatedGetRequests() throws Exception
    {
        when(request.getMethod()).thenReturn("GET");
        when(userManager.getRemoteUsername(request)).thenReturn(USER.getName());
        when(localeResolver.getLocale(request)).thenReturn(Locale.US);
        when(store.getAccessTokensForUser(USER.getName())).thenReturn(ImmutableList.<ServiceProviderToken>of(ACCESS_TOKEN));
        
        servlet.service(request, response);
        
        verify(templateRenderer).render(eq("templates/user/access-tokens.vm"), anyMap(), isA(Writer.class));
    }
    
    @Test
    public void verifyThatPostWithNoTokenParameterCausesBadRequestResponse() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(userManager.getRemoteUsername(request)).thenReturn(USER.getName());
        
        servlet.service(request, response);
        
        verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST);
    }
    
    @Test
    public void verifyThatPostWithTokenParameterThatDoesNotExistDoesNothing() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("token")).thenReturn(TOKEN_VALUE);
        when(userManager.getRemoteUsername(request)).thenReturn(USER.getName());
        
        servlet.service(request, response);
        
        verify(store, never()).remove(TOKEN_VALUE);
    }
    
    @Test
    public void verifyThatTryingToRemoveTokenForAnotherUserResultsInNotAuthorizedResponse() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("token")).thenReturn(TOKEN_VALUE);
        when(userManager.getRemoteUsername(request)).thenReturn(USER.getName() + "'s mom");
        when(store.get(TOKEN_VALUE)).thenReturn(ACCESS_TOKEN);
        
        servlet.service(request, response);
        
        verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
        verify(store, never()).remove(TOKEN_VALUE);
    }
    
    @Test
    public void verifyThatUserCanRemoveTokensThatBelongToThem() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameter("token")).thenReturn(TOKEN_VALUE);
        when(userManager.getRemoteUsername(request)).thenReturn(USER.getName());
        when(store.get(TOKEN_VALUE)).thenReturn(ACCESS_TOKEN);
        
        servlet.service(request, response);
        
        verify(store).remove(TOKEN_VALUE);
    }
}
