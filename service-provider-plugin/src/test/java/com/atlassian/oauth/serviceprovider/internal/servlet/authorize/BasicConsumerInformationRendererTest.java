package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.oauth.serviceprovider.ConsumerInformationRenderer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BasicConsumerInformationRendererTest
{
    static final ServiceProviderToken TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(Version.V_1_0_A).build();

    @Mock ApplicationProperties applicationProperties;
    @Mock TemplateRenderer templateRenderer;
    @Mock UserManager userManager;
    
    ConsumerInformationRenderer renderer;
    
    @Mock HttpServletRequest request;
    ByteArrayOutputStream baos;
    
    @Before
    public void setUp()
    {
        baos = new ByteArrayOutputStream();
        renderer = new BasicConsumerInformationRenderer(applicationProperties, templateRenderer, userManager);
        
        when(applicationProperties.getBaseUrl()).thenReturn("http://my.appdomain.com:3200/jira");
    }
    
    @Test
    public void verifyTemplateRendererIsCalledForRendering() throws Exception
    {
        PrintWriter writer = new PrintWriter(baos);
        renderer.render(TOKEN, request, writer);
        
        ImmutableMap<String, Object> context = ImmutableMap.<String, Object>of(
                "consumer", TOKEN.getConsumer(),
                "applicationDomain", "my.appdomain.com",
                "userFullName", "User unknown");
        
        verify(templateRenderer)
            .render("templates/auth/basic-consumer-info.vm", context, writer);
    }
}
