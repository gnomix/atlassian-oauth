package com.atlassian.oauth.serviceprovider.internal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;

import com.atlassian.oauth.util.Check;

public final class ByteArrayServletOutputStream extends ServletOutputStream
{
    private final OutputStream os;
    public ByteArrayServletOutputStream(ByteArrayOutputStream os)
    {
        this.os = Check.notNull(os);
    }
    
    @Override
    public void write(int b) throws IOException
    {
        os.write(b);
    }
}