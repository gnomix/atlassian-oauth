package com.atlassian.oauth.serviceprovider.internal.servlet;

import java.io.ByteArrayOutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.bridge.serviceprovider.ServiceProviderTokens;
import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.ByteArrayServletOutputStream;
import com.atlassian.oauth.serviceprovider.internal.OAuthConverter;
import com.atlassian.oauth.serviceprovider.internal.PassThruTransactionTemplate;
import com.atlassian.oauth.serviceprovider.internal.TokenFactory;
import com.atlassian.oauth.serviceprovider.internal.util.Paths;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.oauth.Request.OAUTH_SESSION_HANDLE;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session.newSession;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.SERVICE_PROVIDER;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static net.oauth.OAuth.OAUTH_VERIFIER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.same;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccessTokenServletTest
{
    private static final String SESSION_HANDLE = "abcd";
    private static final String TOKEN_VALUE = "1234";
    private static final String TOKEN_SECRET = "5678";
    private static final String VERIFIER = "9876";
    
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken(TOKEN_VALUE).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).version(Version.V_1_0_A).build();
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN = UNAUTHORIZED_REQUEST_TOKEN.authorize(USER, VERIFIER);
    private static final ServiceProviderToken EXPIRED_AUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken(TOKEN_VALUE).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).creationTime(System.currentTimeMillis() - ServiceProviderToken.DEFAULT_REQUEST_TOKEN_TTL *2).authorizedBy(USER).verifier(VERIFIER).version(Version.V_1_0_A).build();
    private static final OAuthAccessor AUTHORIZED_REQUEST_ACCESSOR = ServiceProviderTokens.asOAuthAccessor(AUTHORIZED_REQUEST_TOKEN, SERVICE_PROVIDER);
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN_VALUE).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).authorizedBy(USER).session(newSession(SESSION_HANDLE).build()).build();
    private static final ServiceProviderToken ACCESS_TOKEN_WITH_EXPIRED_SESSION = ServiceProviderToken.newAccessToken(TOKEN_VALUE).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).authorizedBy(USER).session(newSession(SESSION_HANDLE).creationTime(System.currentTimeMillis() - ServiceProviderToken.DEFAULT_SESSION_TTL * 3).lastRenewalTime(System.currentTimeMillis() - ServiceProviderToken.DEFAULT_SESSION_TTL * 2).build()).build();
    private static final ServiceProviderToken RENEWED_ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN_VALUE+"r1").tokenSecret(TOKEN_SECRET+"r1").consumer(RSA_CONSUMER).authorizedBy(USER).session(newSession(SESSION_HANDLE+"r1").build()).build();
    
    private static final ServiceProviderToken V1_UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken(TOKEN_VALUE).tokenSecret(TOKEN_SECRET).consumer(RSA_CONSUMER).version(Version.V_1_0).build();
    private static final ServiceProviderToken V1_AUTHORIZED_REQUEST_TOKEN = V1_UNAUTHORIZED_REQUEST_TOKEN.authorize(USER, VERIFIER);
    
    @Mock ServiceProviderTokenStore tokenStore;
    @Mock TokenFactory factory;
    @Mock OAuthValidator validator;
    @Mock ApplicationProperties applicationProperties;
    @Mock OAuthConverter converter;
    TransactionTemplate transactionTemplate = new PassThruTransactionTemplate();
    @Mock Clock clock;
    
    HttpServlet servlet;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    ByteArrayOutputStream responseStream;
    
    @Before
    public void setUp() throws Exception
    {
        responseStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ByteArrayServletOutputStream(responseStream));
        
        when(converter.toOAuthAccessor(AUTHORIZED_REQUEST_TOKEN)).thenReturn(AUTHORIZED_REQUEST_ACCESSOR);
        when(converter.toOAuthAccessor(V1_AUTHORIZED_REQUEST_TOKEN)).thenReturn(AUTHORIZED_REQUEST_ACCESSOR);

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://test/" + Paths.ACCESS_TOKEN));

        servlet = new AccessTokenServlet(tokenStore, factory, validator, applicationProperties, converter, transactionTemplate, clock);
    }
    
    @Test
    public void verifyThatAuthorizedRequestTokenCanBeSwappedForAccessToken() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(AUTHORIZED_REQUEST_TOKEN);
        when(tokenStore.put(ACCESS_TOKEN)).thenReturn(ACCESS_TOKEN);
        when(factory.generateAccessToken(AUTHORIZED_REQUEST_TOKEN)).thenReturn(ACCESS_TOKEN);
        
        servlet.service(request, response);
        
        verify(validator).validateMessage(isA(OAuthMessage.class), same(AUTHORIZED_REQUEST_ACCESSOR));
        verify(tokenStore).put(ACCESS_TOKEN);
        verify(tokenStore).remove(AUTHORIZED_REQUEST_TOKEN.getToken());
        verify(response).setContentType("text/plain");
        assertThat(responseStream.toString(), is(equalTo("oauth_token=1234&oauth_token_secret=5678&oauth_expires_in=3600&oauth_session_handle=abcd&oauth_authorization_expires_in=2592000")));
    }
    
    @Test
    public void verifyThatTokenRejectedResponseIsSentForNonExistentRequestTokens() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=token_rejected")));
    }
    
    @Test
    public void verifyThatTokenExpiredResponseIsSentForExpiredRequestTokens() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(EXPIRED_AUTHORIZED_REQUEST_TOKEN);
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());
        
        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=token_expired")));
    }

    @Test
    public void verifyThatPermissionUnknownResponseIsSentIfRequestTokenIsUnauthorized() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);

        servlet.service(request, response);
        
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=permission_unknown")));
    }
    
    @Test
    public void verifyThatMessageValidationFailureIsHandled() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(AUTHORIZED_REQUEST_TOKEN);
        doThrow(new OAuthProblemException("signature_invalid")).when(validator).validateMessage(isA(OAuthMessage.class), same(AUTHORIZED_REQUEST_ACCESSOR));

        servlet.service(request, response);
        
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=signature_invalid")));
    }
    
    @Test
    public void verifyThatTokenRejectedResponseIsSentWhenInvalidTokenExceptionIsThrownByStore() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenThrow(new InvalidTokenException("Invalid token"));
        
        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=token_rejected")));
    }

    @Test
    public void verifyThatTokenExpiredResponseIsSentWhenAuthorizedRequestTokenHasExpired() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(EXPIRED_AUTHORIZED_REQUEST_TOKEN);
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());
        
        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), is(equalTo("oauth_problem=token_expired")));
    }
    
    @Test
    public void verifyThatParameterAbsentIsSentWhenOAuthTokenIsNotPresent() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_VERIFIER, new String[] { VERIFIER }
        ));

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), allOf(
            containsString("oauth_problem=parameter_absent"),
            containsString("oauth_parameters_absent=oauth_token"))
        );
    }

    @Test
    public void verifyThatParameterAbsentIsSentWhenOAuthVerifierIsNotPresent() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(AUTHORIZED_REQUEST_TOKEN);

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), allOf(
            containsString("oauth_problem=parameter_absent"),
            containsString("oauth_parameters_absent=oauth_verifier"))
        );
    }
    
    @Test
    public void verifyThatTokenRejectedIsSentWhenOAuthVerifierDoesNotMatchTokenVerifier() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE },
            OAUTH_VERIFIER, new String[] { "not the verifier" }
        ));

        servlet.service(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), containsString("oauth_problem=token_rejected"));
    }
    
    @Test
    public void verifyThatAuthorizedVersion1RequestTokenCanBeSwappedForAccessTokenWithoutVerifier() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE } 
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(V1_AUTHORIZED_REQUEST_TOKEN);
        when(tokenStore.put(ACCESS_TOKEN)).thenReturn(ACCESS_TOKEN);
        when(factory.generateAccessToken(V1_AUTHORIZED_REQUEST_TOKEN)).thenReturn(ACCESS_TOKEN);
        
        servlet.service(request, response);
        
        verify(validator).validateMessage(isA(OAuthMessage.class), same(AUTHORIZED_REQUEST_ACCESSOR));
        verify(tokenStore).put(ACCESS_TOKEN);
        verify(tokenStore).remove(V1_AUTHORIZED_REQUEST_TOKEN.getToken());
        verify(response).setContentType("text/plain");
        assertThat(responseStream.toString(), is(equalTo("oauth_token=1234&oauth_token_secret=5678&oauth_expires_in=3600&oauth_session_handle=abcd&oauth_authorization_expires_in=2592000")));
    }
    
    @Test
    public void verifyThatAccessTokenWithValidSessionCanBeRenewed() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_SESSION_HANDLE, new String[] { SESSION_HANDLE }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(ACCESS_TOKEN);
        when(tokenStore.put(RENEWED_ACCESS_TOKEN)).thenReturn(RENEWED_ACCESS_TOKEN);
        when(factory.generateAccessToken(ACCESS_TOKEN)).thenReturn(RENEWED_ACCESS_TOKEN);
        
        servlet.service(request, response);
        
        verify(tokenStore).put(RENEWED_ACCESS_TOKEN);
        verify(tokenStore).remove(ACCESS_TOKEN.getToken());
        verify(response).setContentType("text/plain");
        assertThat(responseStream.toString(), is(equalTo("oauth_token=1234r1&oauth_token_secret=5678r1&oauth_expires_in=3600&oauth_session_handle=abcdr1&oauth_authorization_expires_in=2592000")));
    }
    
    @Test
    public void verifyThatParameterAbsentIsSentWhenRenewingAccessTokenWithoutSessionHandle() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(ACCESS_TOKEN);
        
        servlet.service(request, response);
        
        verify(response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), allOf(
                containsString("oauth_problem=parameter_absent"),
                containsString("oauth_parameters_absent=oauth_session_handle")));
    }
    
    @Test
    public void verifyThatTokenRejectedWhenRenewingAccessTokenWithSessionHandleMismatch() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_SESSION_HANDLE, new String[] { SESSION_HANDLE+"r1" }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(ACCESS_TOKEN);
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());
        
        servlet.service(request, response);
        
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), containsString("oauth_problem=token_rejected"));
    }
    
    @Test
    public void verifyThatPermissionDeniedIsSentWhenRenewingAccessTokenForExpiredSession() throws Exception
    {
        when(request.getMethod()).thenReturn("POST");
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
            OAUTH_TOKEN, new String[] { TOKEN_VALUE }, 
            OAUTH_SESSION_HANDLE, new String[] { SESSION_HANDLE }
        ));
        when(tokenStore.get(TOKEN_VALUE)).thenReturn(ACCESS_TOKEN_WITH_EXPIRED_SESSION);
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());
        
        servlet.service(request, response);
        
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(response).setContentType(startsWith(OAuth.FORM_ENCODED));
        assertThat(responseStream.toString(), containsString("oauth_problem=permission_denied"));
    }
}
