package com.atlassian.oauth.serviceprovider.internal.servlet;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static net.oauth.OAuth.Problems.PARAMETER_ABSENT;
import static net.oauth.OAuth.Problems.TOKEN_EXPIRED;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;
import static net.oauth.OAuth.Problems.TOKEN_USED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.oauth.serviceprovider.internal.util.Paths;
import net.oauth.OAuthProblemException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.collect.ImmutableMap;

@RunWith(MockitoJUnitRunner.class)
public class TokenLoaderImplTest
{
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(Version.V_1_0_A).build();
    private static final ServiceProviderToken EXPIRED_UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).creationTime(System.currentTimeMillis() - ServiceProviderToken.DEFAULT_REQUEST_TOKEN_TTL * 2).version(Version.V_1_0_A).build();
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN = UNAUTHORIZED_REQUEST_TOKEN.authorize(USER, "9876");
    private static final ServiceProviderToken DENIED_REQUEST_TOKEN = UNAUTHORIZED_REQUEST_TOKEN.deny(USER);
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).authorizedBy(USER).build();

    @Mock ServiceProviderTokenStore store;
    @Mock Clock clock;
    @Mock ApplicationProperties applicationProperties;

    TokenLoader loader;
    
    @Mock HttpServletRequest request;
    
    @Before
    public void setUp()
    {
        loader = new TokenLoaderImpl(store, clock);
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://test/" + Paths.AUTHORIZE));
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorizationWithNoOAuthTokenParameterCausesParameterAbsentOAuthProblemException() throws Exception
    {
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(PARAMETER_ABSENT)));
            throw e;
        }
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorizationCausesTokenRejectOAuthProblemExceptionWhenTokenDoesNotExist() throws Exception
    {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(OAUTH_TOKEN, new String[] { "1234" }));
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(TOKEN_REJECTED)));
            throw e;
        }
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorizationCausesTokenRejectedOAuthProblemExceptionWhenTokenIsAnAccessToken() throws Exception
    {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(OAUTH_TOKEN, new String[] { "1234" }));
        when(store.get("1234")).thenReturn(ACCESS_TOKEN);
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(TOKEN_REJECTED)));
            throw e;
        }
    }

    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorizationCausesTokenUsedOAuthProblemExceptionWhenTokenHasBeenAuthorized() throws Exception
    {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(OAUTH_TOKEN, new String[] { "1234" }));
        when(store.get("1234")).thenReturn(AUTHORIZED_REQUEST_TOKEN);
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(TOKEN_USED)));
            throw e;
        }
    }

    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorizationCausesTokenUsedOAuthProblemExceptionWhenTokenHasBeenDenied() throws Exception
    {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(OAUTH_TOKEN, new String[] { "1234" }));
        when(store.get("1234")).thenReturn(DENIED_REQUEST_TOKEN);
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(TOKEN_USED)));
            throw e;
        }
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorizationCausesTokenRejectedOAuthProblemExceptionWhenInvalidTokenExceptionIsThrownByStore() throws Exception
    {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(OAUTH_TOKEN, new String[] { "1234" }));
        when(store.get("1234")).thenThrow(new InvalidTokenException("Invalid token"));
        
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(TOKEN_REJECTED)));
            throw e;
        }
    }

    @Test(expected=OAuthProblemException.class)
    public void assertThatLoadingTokenForAuthorationCausesTokenExpiredOAuthProblemExceptionWhenTokenHasExpired() throws Exception
    {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(OAUTH_TOKEN, new String[] { "1234" }));
        when(store.get("1234")).thenReturn(EXPIRED_UNAUTHORIZED_REQUEST_TOKEN);
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());
        
        try
        {
            loader.getTokenForAuthorization(request);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(TOKEN_EXPIRED)));
            throw e;
        }
    }
}
