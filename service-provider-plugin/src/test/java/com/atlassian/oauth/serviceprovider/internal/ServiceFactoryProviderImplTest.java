package com.atlassian.oauth.serviceprovider.internal;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.serviceprovider.internal.util.Paths;
import com.atlassian.sal.api.ApplicationProperties;

@RunWith(MockitoJUnitRunner.class)
public class ServiceFactoryProviderImplTest
{
    @Mock ApplicationProperties applicationProperties;
    
    ServiceProviderFactory factory;

    @Before
    public void setUp()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("http://base/url");
        factory = new ServiceProviderFactoryImpl(applicationProperties);
    }
    
    @Test
    public void assertThatReturnedServiceProvidersHaveUrisWhoseBaseIsFromApplicationProperties()
    {
        ServiceProvider sp = new ServiceProvider(
            URI.create("http://base/url" + Paths.REQUEST_TOKEN),
            URI.create("http://base/url" + Paths.AUTHORIZE),
            URI.create("http://base/url" + Paths.ACCESS_TOKEN)
        );
        assertThat(factory.newServiceProvider(), is(equalTo(sp)));
    }
}
