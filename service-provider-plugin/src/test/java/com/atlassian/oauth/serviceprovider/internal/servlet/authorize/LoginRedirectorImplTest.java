package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;

@RunWith(MockitoJUnitRunner.class)
public class LoginRedirectorImplTest
{
    @Mock UserManager userManager;
    @Mock LoginUriProvider loginUriProvider;

    LoginRedirector loginRedirector;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    @Before
    public void setUp()
    {
        loginRedirector = new LoginRedirectorImpl(userManager, loginUriProvider);
    }
    
    @Test
    public void assertThatIsLoggedInReturnsFalseIfUserManagerReturnsNullUsername()
    {
        when(userManager.getRemoteUsername(request)).thenReturn(null);
        assertFalse(loginRedirector.isLoggedIn(request));
    }
    
    @Test
    public void assertThatIsLoggedInReturnsTrueUserManagerReturnsNonNullUsername()
    {
        when(userManager.getRemoteUsername(request)).thenReturn("fred");
        assertTrue(loginRedirector.isLoggedIn(request));
    }
    
    @Test
    public void verifyThatRedirectToLoginRedirectsUserToLoginUriWithCorrectReturnUri() throws Exception
    {
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://sp/authorize"));
        when(request.getQueryString()).thenReturn("oauth_token=1234");
        when(loginUriProvider.getLoginUri(URI.create("http://sp/authorize?oauth_token=1234"))).thenReturn(URI.create("http://sp/login"));
        
        loginRedirector.redirectToLogin(request, response);
        
        verify(loginUriProvider).getLoginUri(URI.create("http://sp/authorize?oauth_token=1234"));
        verify(response).sendRedirect("http://sp/login");
    }
}
