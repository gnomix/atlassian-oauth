package com.atlassian.oauth.serviceprovider.internal;

import static com.atlassian.hamcrest.DeepIsEqual.deeplyEqualTo;
import static com.atlassian.oauth.testing.TestData.USER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.internal.OAuthProblem.Problem;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.sal.api.auth.Authenticator.Result;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Maps;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticatorImplTest
{
    private static final String TOKEN = "1234";
    private static final ServiceProviderToken REQUEST_TOKEN = ServiceProviderToken.newRequestToken(TOKEN)
        .tokenSecret("5678")
        .consumer(RSA_CONSUMER)
        .version(Version.V_1_0_A)
        .build();
    private static final ServiceProviderToken ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN)
        .tokenSecret("5678")
        .consumer(RSA_CONSUMER)
        .authorizedBy(USER)
        .build();
    private static final ServiceProviderToken EXPIRED_ACCESS_TOKEN = ServiceProviderToken.newAccessToken(TOKEN)
        .tokenSecret("5678")
        .consumer(RSA_CONSUMER)
        .authorizedBy(USER)
        .creationTime(System.currentTimeMillis() - ServiceProviderToken.DEFAULT_ACCESS_TOKEN_TTL * 2)
        .build();

    
    @Mock ServiceProviderTokenStore store;
    @Mock OAuthValidator validator;
    @Mock OAuthConverter converter;
    @Mock AuthenticationController authenticationController;
    @Mock ApplicationProperties applicationProperties;
    TransactionTemplate transactionTemplate;
    @Mock Clock clock;
    
    Authenticator authenticator;
    
    @Mock HttpServletRequest request;
    @Mock HttpServletResponse response;
    
    Map<String, String[]> parameterMap;
    ByteArrayOutputStream responseOutputStream;
    
    @Before
    public void setUp() throws IOException
    {
        parameterMap = Maps.newHashMap();
        parameterMap.put("oauth_token", new String[] { TOKEN });
        when(request.getParameterMap()).thenReturn(parameterMap);
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://host/service"));

        responseOutputStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ByteArrayServletOutputStream(responseOutputStream));
        
        when(converter.toOAuthAccessor(ACCESS_TOKEN)).thenReturn(new OAuthAccessor(RSA_OAUTH_CONSUMER));
        
        when(clock.timeInMilliseconds()).thenReturn(System.currentTimeMillis());
        
        transactionTemplate = new PassThruTransactionTemplate();
        authenticator = new AuthenticatorImpl(store, validator, converter, authenticationController, transactionTemplate, applicationProperties, clock);
    }
    
    @Test
    public void assertThatSuccessIsReturnedForValidAccessTokenWhenUserCanLogIn()
    {
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(true);
        Result success = new Result.Success(USER);
        assertThat(authenticator.authenticate(request, response), is(equalTo(success)));
    }
    
    @Test
    public void assertThatFailureResultWithInvalidTokenMessageIsReturnedWhenTokenDoesNotExist()
    {
        Result failure = new Result.Failure(new OAuthProblem.InvalidToken(TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    @Test
    public void assertThatFailureResultWithTokenRejectedMessageIsReturnedWhenTokenIsNotAnAccessToken()
    {
        when(store.get(TOKEN)).thenReturn(REQUEST_TOKEN);
        Result failure = new Result.Failure(new OAuthProblem(Problem.TOKEN_REJECTED, TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    @Test
    public void assertThatFailureResultIsReturnedWhenThereIsAnOAuthProblemDuringMessageValidation() throws Exception
    {
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        doThrow(new OAuthProblemException(OAuth.Problems.SIGNATURE_INVALID)).when(validator).validateMessage(isA(OAuthMessage.class), isA(OAuthAccessor.class));
        Result failure = new Result.Failure(new OAuthProblem(Problem.SIGNATURE_INVALID, "1234"));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    @Test
    public void assertThatErrorResultIsReturnedWhenThereIsAGeneralExceptionDuringMessageValidation() throws Exception
    {
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        OAuthException toBeThrown = new OAuthException("Unknown problem");
        doThrow(toBeThrown).when(validator).validateMessage(isA(OAuthMessage.class), isA(OAuthAccessor.class));
        Result error = new Result.Error(new OAuthProblem.System(toBeThrown));
        assertThat(authenticator.authenticate(request, response), is(equalTo(error)));
        verify(response).setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    @Test
    public void assertThatFailureResultWithPermissionDeniedMessageIsReturnedForUserWithValidTokenThatCannotLogIn()
    {
        when(store.get(TOKEN)).thenReturn(ACCESS_TOKEN);
        when(authenticationController.canLogin(USER, request)).thenReturn(false);
        Result failure = new Result.Failure(new OAuthProblem.PermissionDenied(USER));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    @Test
    public void assertThatFailureResultIsReturnedWhenInvalidTokenExceptionIsThrownWhileFetchingToken()
    {
        when(store.get(TOKEN)).thenThrow(new InvalidTokenException("token is invalid"));
        Result failure = new Result.Failure(new OAuthProblem.InvalidToken(TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    @Test
    public void asserThatFailureResultIsReturnedWhenTokenIsExpired()
    {
        when(store.get(TOKEN)).thenReturn(EXPIRED_ACCESS_TOKEN);

        Result failure = new Result.Failure(new OAuthProblem.TokenExpired(TOKEN));
        assertThat(authenticator.authenticate(request, response), is(equalTo(failure)));
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).addHeader(eq("WWW-Authenticate"), startsWith(OAuthMessage.AUTH_SCHEME));
    }
    
    static Matcher<? super Result> equalTo(final Result result)
    {
        return deeplyEqualTo(result);
    }
    
    static <T> Matcher<? super T> equalTo(T o)
    {
        return Matchers.equalTo(o);
    }
}
