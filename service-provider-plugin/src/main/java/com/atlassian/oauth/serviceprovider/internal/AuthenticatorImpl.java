package com.atlassian.oauth.serviceprovider.internal;

import static net.oauth.OAuth.Problems.TOKEN_EXPIRED;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.oauth.serviceprovider.internal.servlet.OAuthProblemUtils.logOAuthProblem;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.server.OAuthServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.OAuthProblem.Problem;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public class AuthenticatorImpl implements Authenticator
{
    /**
     * The request attribute key that the request dispatcher uses to store the
     * original URL for a forwarded request.
     */
    private static final String FORWARD_REQUEST_URI = "javax.servlet.forward.request_uri";
    
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ServiceProviderTokenStore store;
    private final OAuthValidator validator;
    private final OAuthConverter converter;
    private final AuthenticationController authenticationController;
    private final TransactionTemplate transactionTemplate;
    private final ApplicationProperties applicationProperties;
    private final Clock clock;

    public AuthenticatorImpl(@Qualifier("tokenStore") ServiceProviderTokenStore store, 
        OAuthValidator validator,
        OAuthConverter converter,
        AuthenticationController authenticationController,
        TransactionTemplate transactionTemplate,
        ApplicationProperties applicationProperties,
        Clock clock)
    {
        this.store = Check.notNull(store, "store");
        this.validator = Check.notNull(validator, "validator");
        this.converter = Check.notNull(converter, "converter");
        this.authenticationController = Check.notNull(authenticationController, "authenticationController");
        this.transactionTemplate = Check.notNull(transactionTemplate, "transactionTemplate");
        this.applicationProperties = Check.notNull(applicationProperties, "applicationProperties");
        this.clock = Check.notNull(clock, "clock");
    }
    
    public Authenticator.Result authenticate(HttpServletRequest request, HttpServletResponse response)
    {
        OAuthMessage message = OAuthServlet.getMessage(request, getLogicalUri(request));
        
        String tokenStr;
        try
        {
            tokenStr = message.getToken();
        }
        catch (IOException e)
        {
            // this would be really strange if it happened, but take precautions just in case
            log.error("Failed to read token from request", e);
            sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
            return new Result.Error(new OAuthProblem.UnreadableToken(e));
        }

        ServiceProviderToken token = null;
        try
        {
            try
            {
                token = getToken(tokenStr);
            }
            catch (InvalidTokenException e)
            {
                throw new OAuthProblemException(TOKEN_REJECTED);
            }
            if (token == null || !token.isAccessToken())
            {
                throw new OAuthProblemException(TOKEN_REJECTED);
            }
            if (token.hasExpired(clock))
            {
                throw new OAuthProblemException(TOKEN_EXPIRED);
            }
            validator.validateMessage(message, converter.toOAuthAccessor(token));
        }
        catch (OAuthProblemException ope)
        {
            logOAuthProblem(message, ope, log);
            try
            {
                OAuthServlet.handleException(response, ope, applicationProperties.getBaseUrl());
            }
            catch (Exception e)
            {
                // there was an IOE or ServletException, nothing more we can really do
                log.error("Failure reporting OAuth error to client", e);
            }
            return new Result.Failure(new OAuthProblem(Problem.valueOf(ope.getProblem().toUpperCase()), tokenStr));
        }
        catch (Exception e)
        {
            // this isn't likely to happen, it would result from some unknown error with the request that the OAuth.net
            // library couldn't handle appropriately
            log.error("Failed to validate OAuth message", e);
            sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
            return new Result.Error(new OAuthProblem.System(e));
        }
        
        final Principal user = token.getUser();
        if (!authenticationController.canLogin(user, request))
        {
            // user exists but is not allowed to login
            log.warn("Access denied to user '{}' using token '{}' because that user cannot login", token.getUser().getName(), token.getToken());
            sendError(response, HttpServletResponse.SC_UNAUTHORIZED, message);
            return new Result.Failure(new OAuthProblem.PermissionDenied(user));
        }
        return new Result.Success(user);
    }

    private ServiceProviderToken getToken(final String tokenStr)
    {
        return (ServiceProviderToken) transactionTemplate.execute(new TransactionCallback() { public Object doInTransaction() {
            return store.get(tokenStr);
        }});
    }

    private String getLogicalUri(HttpServletRequest request)
    {
        String uriPathBeforeForwarding = (String) request.getAttribute(FORWARD_REQUEST_URI);
        if (uriPathBeforeForwarding == null)
        {
            return null;
        }
        URI newUri = URI.create(request.getRequestURL().toString());
        try
        {
            return new URI(newUri.getScheme(), newUri.getAuthority(),
                    uriPathBeforeForwarding,
                    newUri.getQuery(),
                    newUri.getFragment()).toString();
        }
        catch (URISyntaxException e)
        {
            log.warn("forwarded request had invalid original URI path: " + uriPathBeforeForwarding);
            return null;
        }
    }

    private void sendError(HttpServletResponse response, int status, OAuthMessage message)
    {
        response.setStatus(status);
        try
        {
            response.addHeader("WWW-Authenticate", message.getAuthorizationHeader(applicationProperties.getBaseUrl()));
        }
        catch (IOException e)
        {
            log.error("Failure reporting OAuth error to client", e);
        }
    }
}
