package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.StoreException;

/**
 * An implementation of the {@code ServiceProviderTokenStore} that delegates to an actual implementation.  The primary
 * use for this delegate token store is to catch {@code InvalidTokenException}s when {@link #get(String)} is called
 * and deleting the token.  This allows the underlying token store to be as "dumb" as possible and allows us to
 * centralize the handling of that exception in one place.
 */
public class DelegatingTokenStoreImpl implements ServiceProviderTokenStore
{
    private final ServiceProviderTokenStore store;

    public DelegatingTokenStoreImpl(ServiceProviderTokenStore store)
    {
        this.store = store;
    }
    
    public ServiceProviderToken get(String token) throws StoreException
    {
        try
        {
            return store.get(token);
        }
        catch (InvalidTokenException e)
        {
            store.remove(token);
            throw e;
        }
    }

    public ServiceProviderToken put(ServiceProviderToken token) throws StoreException
    {
        return store.put(token);
    }

    public void remove(String token) throws StoreException
    {
        store.remove(token);
    }
    
    public void removeExpiredTokens() throws StoreException
    {
        store.removeExpiredTokens();
    }
    
    public void removeExpiredSessions() throws StoreException
    {
        store.removeExpiredSessions();
    }

    public Iterable<ServiceProviderToken> getAccessTokensForUser(String username)
    {
        return store.getAccessTokensForUser(username);
    }

    public void removeByConsumer(String consumerKey)
    {
        store.removeByConsumer(consumerKey);
    }
}
