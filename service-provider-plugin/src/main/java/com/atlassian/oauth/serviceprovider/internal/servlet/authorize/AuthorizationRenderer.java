package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;

/**
 * Handles rendering the authorization page.
 */
public interface AuthorizationRenderer
{
    /**
     * Render the authorization page for the request and token.
     * 
     * @param request users request for authorization 
     * @param response response to write the authorization to
     * @param token request token the consumer wants the user to authorize
     * @throws IOException thrown if there are any IO problems writing the authorization page
     */
    void render(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException;
}
