package com.atlassian.oauth.serviceprovider.internal;

import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.RSA_SHA1;
import static net.oauth.OAuth.Problems.SIGNATURE_METHOD_REJECTED;

import java.io.IOException;
import java.net.URISyntaxException;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.SimpleOAuthValidator;

/**
 * A validator that wraps an instance of the {@link SimpleOAuthValidator} and verifies that the OAuth signature method
 * is RSA_SHA1.
 */
public class OAuthValidatorImpl implements OAuthValidator
{
    private OAuthValidator simpleValidator = new SimpleOAuthValidator();

    public void validateMessage(OAuthMessage message, OAuthAccessor accessor)
        throws OAuthException, IOException, URISyntaxException
    {
        message.requireParameters(OAUTH_SIGNATURE_METHOD, OAuth.OAUTH_CONSUMER_KEY);
        if (!message.getParameter(OAUTH_SIGNATURE_METHOD).equals(RSA_SHA1))
        {
            throw new OAuthProblemException(SIGNATURE_METHOD_REJECTED);
        }
        simpleValidator.validateMessage(message, accessor);
    }
}
