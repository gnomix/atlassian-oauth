package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static net.oauth.OAuth.OAUTH_CALLBACK;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static net.oauth.OAuth.OAUTH_VERIFIER;
import static net.oauth.OAuth.addParameters;
import static org.apache.commons.lang.StringUtils.isEmpty;

import java.io.IOException;
import java.net.URI;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.serviceprovider.internal.Randomizer;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

/**
 * Authorizes the token and redirects to consumer
 */
final class PostAuthorization implements AuthorizationRequestProcessor
{
    private static final String AUTH_NO_CALLBACK_APPROVAL_V1_TEMPLATE = "templates/auth/no-callback-approval-v1.vm";
    private static final String AUTH_NO_CALLBACK_APPROVAL_V1A_TEMPLATE = "templates/auth/no-callback-approval-v1a.vm";
    private static final String AUTH_NO_CALLBACK_DENIED_TEMPLATE = "templates/auth/no-callback-denied.vm";

    private final ServiceProviderTokenStore store;
    private final Randomizer randomizer;
    private final UserManager userManager;
    private final AuthorizationRenderer renderer;
    private final TemplateRenderer templateRenderer;
    
    public PostAuthorization(@Qualifier("tokenStore") ServiceProviderTokenStore store,
        Randomizer randomizer,
        UserManager userManager,
        AuthorizationRenderer renderer,
        TemplateRenderer templateRenderer)
    {
        this.store = Check.notNull(store, "store");
        this.randomizer = Check.notNull(randomizer, "randomizer");
        this.userManager = Check.notNull(userManager, "userManager");
        this.renderer = Check.notNull(renderer, "renderer");
        this.templateRenderer = Check.notNull(templateRenderer, "templateRenderer");
    }

    public void process(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException
    {
        ServiceProviderToken newToken;
        if (request.getParameter("approve") != null)
        {
            String verifier = randomizer.randomAlphanumericString(AuthorizeServlet.VERIFIER_LENGTH);
            newToken = token.authorize(getLoggedInUser(request), verifier);
        }
        else if (request.getParameter("deny") != null)
        {
            newToken = token.deny(getLoggedInUser(request));
        }
        else
        {
            renderer.render(request, response, token);
            return;
        }
        redirectBackToConsumer(request, response, store.put(newToken));
    }

    private Principal getLoggedInUser(HttpServletRequest request)
    {
        return userManager.resolve(userManager.getRemoteUsername(request));
    }

    private void redirectBackToConsumer(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException
    {
        if (V_1_0_A.equals(token.getVersion()))
        {
            redirectBackToConsumerVersion1a(request, response, token);
        }
        else
        {
            redirectBackToConsumerVersion1(request, response, token);
        }
    }
    
    private void redirectBackToConsumerVersion1(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException
    {
        String callback = request.getParameter(OAUTH_CALLBACK);
        if (isEmpty(callback) && token.getConsumer().getCallback() != null)
        {
            callback = token.getConsumer().getCallback().toString();
        }
        
        if(isEmpty(callback))
        {
            // no call back it must be a client
            response.setContentType("text/html");
            if (token.getAuthorization() == Authorization.AUTHORIZED)
            {
                // no call back, display the verification code so the user can enter it manually
                templateRenderer.render(AUTH_NO_CALLBACK_APPROVAL_V1_TEMPLATE, ImmutableMap.<String, Object>of("token", token), response.getWriter());
            }
            else
            {
                templateRenderer.render(AUTH_NO_CALLBACK_DENIED_TEMPLATE, ImmutableMap.<String, Object>of("token", token), response.getWriter());
            }
        }
        else
        {
            if (token.getToken() != null)
            {
                callback = addParameters(callback, "oauth_token", token.getToken());
            }
            response.sendRedirect(callback);
        }
    }

    private void redirectBackToConsumerVersion1a(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException
    {
        URI callback = token.getCallback() == null ? token.getConsumer().getCallback() : token.getCallback();
        
        if (callback == null)
        {
            response.setContentType("text/html");
            if (token.getAuthorization() == Authorization.AUTHORIZED)
            {
                // no call back, display the verification code so the user can enter it manually
                templateRenderer.render(AUTH_NO_CALLBACK_APPROVAL_V1A_TEMPLATE, ImmutableMap.<String, Object>of("token", token), response.getWriter());
            }
            else
            {
                templateRenderer.render(AUTH_NO_CALLBACK_DENIED_TEMPLATE, ImmutableMap.<String, Object>of("token", token), response.getWriter());
            }
        }
        else
        {
            // add the token and verifier parameters to the callback and send the redirect
            // if the token was denied, then the verifier is set to a placeholder value in case consumers don't handle
            // a blank or missing verifier parameter
            response.sendRedirect(addParameters(callback.toString(), 
                OAUTH_TOKEN, token.getToken(), 
                OAUTH_VERIFIER, token.getAuthorization() == Authorization.AUTHORIZED ? token.getVerifier() : "denied")
            );
        }
    }
    
    
}