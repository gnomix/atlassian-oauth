package com.atlassian.oauth.serviceprovider.internal;

import static com.atlassian.oauth.serviceprovider.internal.util.Paths.ACCESS_TOKEN;
import static com.atlassian.oauth.serviceprovider.internal.util.Paths.AUTHORIZE;
import static com.atlassian.oauth.serviceprovider.internal.util.Paths.REQUEST_TOKEN;

import java.net.URI;

import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.ApplicationProperties;

public class ServiceProviderFactoryImpl implements ServiceProviderFactory
{
    private final ApplicationProperties applicationProperties;

    public ServiceProviderFactoryImpl(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = Check.notNull(applicationProperties, "applicationProperties");
    }

    public ServiceProvider newServiceProvider()
    {
        return new ServiceProvider(
            URI.create(applicationProperties.getBaseUrl() + REQUEST_TOKEN),
            URI.create(applicationProperties.getBaseUrl() + AUTHORIZE),
            URI.create(applicationProperties.getBaseUrl() + ACCESS_TOKEN)
        );
    }
}
