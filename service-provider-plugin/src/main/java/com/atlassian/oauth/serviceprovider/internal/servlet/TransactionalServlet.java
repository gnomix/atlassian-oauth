package com.atlassian.oauth.serviceprovider.internal.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public abstract class TransactionalServlet extends HttpServlet
{
    private final TransactionTemplate transactionTemplate;

    public TransactionalServlet(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = Check.notNull(transactionTemplate, "transactionTemplate");
    }
    
    @Override
    protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        serve(GET, request, response);
    }

    protected void doGetInTransaction(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {}
    
    @Override
    protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        serve(POST, request, response);
    }
    
    protected void doPostInTransaction(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {}
    
    
    private final ServeInTransaction GET = new ServeInTransaction()
    {
        public void serve(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
        {
            doGetInTransaction(request, response);
        }
    };

    private final ServeInTransaction POST = new ServeInTransaction()
    {
        public void serve(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
        {
            doPostInTransaction(request, response);
        }
    };

    private void serve(final ServeInTransaction inTransaction, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
    {
        try
        {
            transactionTemplate.execute(new TransactionCallback()
            {
                public Object doInTransaction()
                {
                    try
                    {
                        inTransaction.serve(request, response);
                    }
                    catch (IOException e)
                    {
                        throw new TransactionException(e);
                    }
                    catch (ServletException e)
                    {
                        throw new TransactionException(e);
                    }
                    return null;
                }
            });
        }
        catch (TransactionException e)
        {
            if (e.getCause() instanceof IOException)
            {
                throw (IOException) e.getCause();
            }
            else
            {
                throw (ServletException) e.getCause();
            }
        }

    }
    
    private interface ServeInTransaction
    {
        void serve(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
    }
}
