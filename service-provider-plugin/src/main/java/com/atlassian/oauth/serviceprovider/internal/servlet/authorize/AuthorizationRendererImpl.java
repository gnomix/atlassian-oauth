package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.osgi.service.ServiceUnavailableException;

import com.atlassian.oauth.serviceprovider.ConsumerInformationRenderException;
import com.atlassian.oauth.serviceprovider.ConsumerInformationRenderer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.internal.servlet.user.AccessTokensServlet;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;

import static com.google.common.collect.Iterables.find;
import static net.oauth.OAuth.OAUTH_CALLBACK;

/**
 * Handles rendering the authorization page.
 */
public final class AuthorizationRendererImpl implements AuthorizationRenderer
{
    private final TemplateRenderer renderer;
    private final Iterable<ConsumerInformationRenderer> consumerInfoRenderers;
    private final ConsumerInformationRenderer basicConsumerInformationRenderer;
    private final ApplicationProperties applicationProperties;

    public AuthorizationRendererImpl(ApplicationProperties applicationProperties,
            TemplateRenderer renderer,
            Iterable<ConsumerInformationRenderer> consumerInfoRenderers,
            @Qualifier("basicConsumerInformationRenderer") ConsumerInformationRenderer basicConsumerInformationRenderer)
    {
        this.renderer = Check.notNull(renderer, "renderer");
        this.consumerInfoRenderers = Check.notNull(consumerInfoRenderers, "consumerInfoRenderers");
        this.basicConsumerInformationRenderer = Check.notNull(basicConsumerInformationRenderer, "basicConsumerInformationRenderer");
        this.applicationProperties = Check.notNull(applicationProperties, "applicationProperties");
    }

    public void render(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        ConsumerInformationRenderer consumerInfoRenderer = findConsumerInfoRenderer(request, token);

        ImmutableMap.Builder<String, Object> contextBuilder = ImmutableMap.builder();
        contextBuilder.put("applicationProperties", applicationProperties);
        contextBuilder.put("token", token.getToken());
        contextBuilder.put("consumer", token.getConsumer());
        contextBuilder.put("consumerRenderer", new AuthorizationConsumerRenderer(consumerInfoRenderer, token, request, response.getWriter()));
        contextBuilder.put("accessTokensAdminUri", applicationProperties.getBaseUrl() + AccessTokensServlet.PATH);
        
        String callback = request.getParameter(OAUTH_CALLBACK);
        
        if (callback != null)
        {
            contextBuilder.put("callback", callback);
        }
        
        try
        {
            renderer.render("templates/auth/authorize.vm", contextBuilder.build(), response.getWriter());
        }
        catch (RenderingException e)
        {
            throw new ConsumerInformationRenderException("Could not render consumer information", e);
        }
    }

    /**
     * Find the most appropriate consumer info renderer for the request and token.  If none can be found from the
     * dynamically imported services, falls back to the {@code basicConsumerInformationRenderer}.
     * 
     * @param request users request
     * @param token token being authorized
     * @return {@code ConsumerInformationRenderer} to use for rendering consumer info on authorization page
     */
    private ConsumerInformationRenderer findConsumerInfoRenderer(HttpServletRequest request, ServiceProviderToken token)
    {
        try
        {
            return new DynamicSafeConsumerInformationRenderer(find(consumerInfoRenderers, canRender(token, request)), basicConsumerInformationRenderer);
        }
        catch (NoSuchElementException e)
        {
            return basicConsumerInformationRenderer;
        }
    }

    public static interface Renderable
    {
        void render() throws IOException;
    }
    
    /**
     * A wrapper to supply to the template which encapsulates the consumer information renderer and all the information
     * it needs so we don't have to supply it all in the template context.
     */
    private static final class AuthorizationConsumerRenderer implements Renderable
    {
        private final ConsumerInformationRenderer renderer;
        private final ServiceProviderToken token;
        private final HttpServletRequest request;
        private final PrintWriter writer;
        
        public AuthorizationConsumerRenderer(ConsumerInformationRenderer renderer, ServiceProviderToken token,
                HttpServletRequest request, PrintWriter writer)
        {
            this.renderer = renderer;
            this.token = token;
            this.request = request;
            this.writer = writer;
        }
        
        public final void render() throws IOException
        {
            renderer.render(token, request, writer);
        }
    }
    
    /**
     * Consumer info renderer that tries to use the dynamic renderer first and if it is not available - possibly because
     * the plugin providing it has gone away since we first found it - fallbacks to another renderer that is
     * guaranteed to be available.
     */
    private static final class DynamicSafeConsumerInformationRenderer implements ConsumerInformationRenderer
    {
        private final ConsumerInformationRenderer dynamicRenderer;
        private final ConsumerInformationRenderer fallbackRenderer;

        public DynamicSafeConsumerInformationRenderer(ConsumerInformationRenderer dynamicRenderer,
                ConsumerInformationRenderer fallbackRenderer)
        {
            this.dynamicRenderer = dynamicRenderer;
            this.fallbackRenderer = fallbackRenderer;
        }

        public boolean canRender(ServiceProviderToken token, HttpServletRequest request)
        {
            return true;
        }

        public void render(ServiceProviderToken token, HttpServletRequest request, Writer writer) throws IOException
        {
            try
            {
                dynamicRenderer.render(token, request, writer);
            }
            catch (ServiceUnavailableException e)
            {
                fallbackRenderer.render(token, request, writer);
            }
        }
    }
    
    private static final Predicate<ConsumerInformationRenderer> canRender(ServiceProviderToken token, HttpServletRequest request)
    {
        return new CanRender(token, request);
    }
    
    private static class CanRender implements Predicate<ConsumerInformationRenderer>
    {
        private final ServiceProviderToken token;
        private final HttpServletRequest request;

        public CanRender(ServiceProviderToken token, HttpServletRequest request)
        {
            this.token = token;
            this.request = request;
        }

        public boolean apply(ConsumerInformationRenderer renderer)
        {
            return renderer.canRender(token, request);
        }
    }
}
