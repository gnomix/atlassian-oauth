package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.util.Check;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.server.OAuthServlet;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;

import static net.oauth.OAuth.OAUTH_TOKEN;
import static net.oauth.OAuth.Problems.TOKEN_EXPIRED;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;
import static net.oauth.OAuth.Problems.TOKEN_USED;

public final class TokenLoaderImpl implements TokenLoader
{
    private final ServiceProviderTokenStore store;
    private final Clock clock;

    public TokenLoaderImpl(@Qualifier ("tokenStore") ServiceProviderTokenStore store, Clock clock)
    {
        this.store = Check.notNull(store, "store");
        this.clock = Check.notNull(clock, "clock");
    }
    
    public ServiceProviderToken getTokenForAuthorization(HttpServletRequest request) throws OAuthProblemException, IOException
    {
        OAuthMessage requestMessage = OAuthServlet.getMessage(request, null);
        requestMessage.requireParameters(OAUTH_TOKEN);
        ServiceProviderToken token;
        try
        {
            token = store.get(requestMessage.getToken());
        }
        catch (InvalidTokenException e)
        {
            throw new OAuthProblemException(TOKEN_REJECTED);
        }
        if (token == null || token.isAccessToken())
        {
            throw new OAuthProblemException(TOKEN_REJECTED);
        }
        if (token.getAuthorization() == Authorization.AUTHORIZED || token.getAuthorization() == Authorization.DENIED)
        {
            throw new OAuthProblemException(TOKEN_USED);
        }
        if (token.hasExpired(clock))
        {
            throw new OAuthProblemException(TOKEN_EXPIRED);
        }
        return token;
    }
    
}
