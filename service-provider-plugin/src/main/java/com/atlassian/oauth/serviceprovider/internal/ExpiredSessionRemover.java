package com.atlassian.oauth.serviceprovider.internal;

import static com.atlassian.oauth.serviceprovider.internal.ExpiredSessionRemoverScheduler.TOKEN_STORE_KEY;

import java.util.Map;

import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.sal.api.scheduling.PluginJob;

/**
 * Calls the {@link ServiceProviderTokenStore#removeExpiredTokens()} and
 * {@link ServiceProviderTokenStore#removeExpiredSessions()} method to remove expired tokens without session information
 * (legacy tokens) and tokens whose sessions have expired.
 */
public class ExpiredSessionRemover implements PluginJob
{
    public void execute(Map<String, Object> jobDataMap)
    {
        ServiceProviderTokenStore tokenStore = (ServiceProviderTokenStore) jobDataMap.get(TOKEN_STORE_KEY);
        tokenStore.removeExpiredTokens();
        tokenStore.removeExpiredSessions();
    }
}
