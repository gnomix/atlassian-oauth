package com.atlassian.oauth.serviceprovider.internal;

import java.io.Serializable;
import java.security.Principal;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.message.Message;

/**
 * {@code OAuthProblem}s are reported when a client makes an OAuth request and an error or failure occurs.
 */
public class OAuthProblem implements Message
{
    private final Problem problem;
    private final String[] arguments;

    OAuthProblem(Problem problem, String... arguments)
    {
        Check.notNull(problem, "problem");
        Check.notNull(arguments, "arguments");
        for (int i = 0; i < arguments.length; i++)
        {
            Check.notNull(String.valueOf(i), arguments[i]);
        }

        this.problem = problem;
        this.arguments = arguments;
    }

    public OAuthProblem(Problem problem)
    {
        this(problem, new String[0]);
    }

    OAuthProblem(Problem problem, String param)
    {
        this(problem, new String[] { param });
    }

    OAuthProblem(Problem problem, String one, String two)
    {
        this(problem, new String[] { one, two });
    }

    OAuthProblem(Problem problem, String one, String two, String three)
    {
        this(problem, new String[] { one, two, three });
    }

    /**
     * Returns the problem enum value.
     *  
     * @return the problem enum value
     */
    public Problem getProblem()
    {
        return problem;
    }
    
    public String getKey()
    {
        return "com.atlassian.oauth.serviceprovider.oauth.problem." + problem.name().toLowerCase();
    }

    /**
     * Returns the parameters that will be used to parameterize messages.
     * 
     * @return the parameters that will be used to parameterize messages
     */
    public String[] getArguments()
    {
        return (String[]) arguments.clone();
    }
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(problem.toString().toLowerCase());
        builder.append(": ");
        for (Serializable argument : arguments)
        {
            builder.append(argument);
            builder.append(", ");
        }
        return builder.toString();
    }
    
    /**
     * Generic problem to use when an unknown error occurs performing an OAuth request  
     */
    public static class System extends OAuthProblem
    {
        public System(Throwable cause)
        {
            super(Problem.SYSTEM, cause.toString());
        }
    }

    /**
     * For use when the OAuth token cannot be read from the HTTP request.  
     */
    public static class UnreadableToken extends OAuthProblem
    {
        public UnreadableToken(Throwable cause)
        {
            super(Problem.UNREADABLE_TOKEN, cause.toString());
        }
    }

    /**
     * For use when a user does not have permission to access the requested resource.
     */
    public static class PermissionDenied extends OAuthProblem
    {
        public PermissionDenied(Principal user)
        {
            super(Problem.PERMISSION_DENIED, user.getName());
        }
    }
    
    /**
     * For use when an OAuth token in a request is not valid, i.e. it doesn't exist in our key store.
     */
    public static class InvalidToken extends OAuthProblem
    {
        public InvalidToken(String token)
        {
            super(Problem.TOKEN_REJECTED, token);
        }
    }

    /**
     * For use when an OAuth token has expired.
     */
    public static class TokenExpired extends OAuthProblem
    {
        public TokenExpired(String token)
        {
            super(Problem.TOKEN_EXPIRED, token);
        }
    }

    /**
     * Problems that can occur during an OAuth operation with parameterized error messages.  Pulled mostly from the
     * {@link http://wiki.oauth.net/ProblemReporting OAuth Problem Reporting spec}.
     * 
     * NOTE: When upgrading to a new version of the OAuth.net library, make sure all the OAuth.Problems are here as well. 
     */
    public enum Problem
    {
        /**
         * The token could not be from the request.
         */
        UNREADABLE_TOKEN,
        
        /**
         * A system error occurred and the request could not be processed.
         */
        SYSTEM,

        /**
         * The oauth_version isn't supported by the Service Provider. In this case, the response SHOULD also contain an
         * oauth_acceptable_versions parameter (described below).
         */
        VERSION_REJECTED,
        
        /**
         * A required parameter wasn't received. In this case, the response SHOULD also contain an
         * oauth_parameters_absent parameter.
         */
        PARAMETER_ABSENT, 
        
        /**
         * An unexpected parameter was received. In this case, the response SHOULD also contain an 
         * oauth_parameters_rejected parameter.
         */
        PARAMETER_REJECTED,
        
        /**
         * The oauth_timestamp value is unacceptable to the Service Provider. In this case, the response SHOULD also
         * contain an oauth_acceptable_timestamps parameter.
         */
        TIMESTAMP_REFUSED,
        
        /**
         * The oauth_nonce value was used in a previous request, and consequently can't be used now.
         */
        NONCE_USED,
        
        /**
         * The oauth_signature_method is unacceptable to the Service Provider.
         */
        SIGNATURE_METHOD_REJECTED,
        
        /**
         * The oauth_signature is invalid. That is, it doesn't match the signature computed by the Service Provider.
         */
        SIGNATURE_INVALID,
        
        /**
         * The oauth_consumer_key is unknown to the Service Provider.
         */
        CONSUMER_KEY_UNKNOWN,
        
        /**
         * The oauth_consumer_key is permanently unacceptable to the Service Provider. For example, the Consumer may be
         * black listed.
         */
        CONSUMER_KEY_REJECTED,
        
        /**
         * The oauth_consumer_key is temporarily unacceptable to the Service Provider. For example, the Service Provider
         * may be throttling the Consumer.
         */
        CONSUMER_KEY_REFUSED,
        
        /**
         * The oauth_token has been consumed. That is, it can't be used any more because it has already been used in a
         * previous request or requests.
         */
        TOKEN_USED,
        
        /**
         * The oauth_token has expired. That is, it was issued too long ago to be used now. If the ScalableOAuth
         * extensions are supported by the Consumer, it can pass on the oauth_session_handle it received in the
         * previous Access Token request to obtain a renewed Access token and secret.
         */
        TOKEN_EXPIRED,
        
        /**
         * The oauth_token has been revoked. That is, the Service Provider has unilaterally decided it will never accept
         * this token.
         */
        TOKEN_REVOKED,
        
        /**
         * The oauth_token is unacceptable to the Service Provider. The reason is unspecified. It might mean that the
         * token was never issued, or consumed or expired and then subsequently forgotten by the Service Provider.
         */
        TOKEN_REJECTED,
        
        /**
         * User needs to give additional permissions before the Consumer is allowed access to the resource. If the
         * ScalableOAuth extensions are supported by the Consumer, it can use the oauth_token (access token) it already
         * has as the request token to initiate the authorization process again, in which case it must use the
         * oauth_token_secret (access token secret) to sign the request for a new access token once the user finishes
         * giving authorization.
         */
        ADDITIONAL_AUTHORIZATION_REQUIRED,
        
        /**
         * The User hasn't decided whether to permit this Consumer to access Protected Resources. Usually happens when
         * the Consumer requests Access Token before the user completes authorization process.
         */
        PERMISSION_UNKNOWN,
        
        /**
         * The User refused to permit this Consumer to access Protected Resources.
         */
        PERMISSION_DENIED,
        
        /**
         * The User (in most cases it's just user's IP) is temporarily unacceptable to the Service Provider. For
         * example, the Service Provider may be rate limiting the IP based on number of requests. This error condition
         * applies to the Authorization process where the user interacts with Service Provider directly. The Service
         * Provider might return this error in the authorization response or in the Access Token request response.
         */
        USER_REFUSED;

    }
}
