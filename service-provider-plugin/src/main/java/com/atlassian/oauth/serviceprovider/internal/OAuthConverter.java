package com.atlassian.oauth.serviceprovider.internal;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.bridge.Consumers;
import com.atlassian.oauth.bridge.serviceprovider.ServiceProviderTokens;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.util.Check;

/**
 * Converts between our own, immutable OAuth objects and those implemented in the oauth.net library. 
 */
public class OAuthConverter
{
    private final ServiceProviderFactory serviceProviderFactory;

    public OAuthConverter(ServiceProviderFactory serviceProviderFactory)
    {
        this.serviceProviderFactory = Check.notNull(serviceProviderFactory, "serviceProviderFactor");
    }

    /**
     * Converts a {@code ServiceProviderToken} to an {@code OAuthAccessor}, setting the {@code requestToken} or 
     * {@code accessToken} accordingly to the type of the {@code ServiceProviderToken}.
     * 
     * @param token {@code ServiceProviderToken} to convert to {@code OAuthAccessor}
     * @param serviceProvider {@code OAuthServiceProvider} for the application
     * @return {@code OAuthAccessor} converted from the {@code ServiceProviderToken}
     */
    public OAuthAccessor toOAuthAccessor(ServiceProviderToken token)
    {
        return ServiceProviderTokens.asOAuthAccessor(token, serviceProviderFactory.newServiceProvider());
    }

    /**
     * Converts a {@code Consumer} to an {@code OAuthConsumer}.
     * 
     * @param consumer {@code Consumer} to convert to {@code OAuthConsumer}
     * @param serviceProvider {@code OAuthServiceProvider} for the application
     * @return {@code OAuthConsumer} converted from the {@code Consumer}
     */
    public OAuthConsumer toOAuthConsumer(Consumer consumer)
    {
        return Consumers.asOAuthConsumer(consumer, serviceProviderFactory.newServiceProvider());
    }
}
