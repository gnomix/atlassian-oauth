package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.IOException;
import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;

public final class LoginRedirectorImpl implements LoginRedirector
{
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;

    public LoginRedirectorImpl(UserManager userManager, LoginUriProvider loginUriProvider)
    {
        this.userManager = Check.notNull(userManager, "userManager");
        this.loginUriProvider = Check.notNull(loginUriProvider, "loginUriProvider");
    }
    
    public boolean isLoggedIn(HttpServletRequest request)
    {
        return userManager.getRemoteUsername(request) != null;
    }

    public void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }
    
    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
