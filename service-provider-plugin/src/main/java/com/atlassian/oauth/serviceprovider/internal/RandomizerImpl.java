package com.atlassian.oauth.serviceprovider.internal;

import org.apache.commons.lang.RandomStringUtils;

public class RandomizerImpl implements Randomizer
{
    public String randomAlphanumericString(int length)
    {
        return RandomStringUtils.randomAlphanumeric(length);
    }
}
