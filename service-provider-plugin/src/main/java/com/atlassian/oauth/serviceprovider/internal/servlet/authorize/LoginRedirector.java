package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Helper to determine if a user is logged in or not and if not, redirect them to the login page.
 */
public interface LoginRedirector
{
    /**
     * Returns {@code true} if the user is logged in, {@code false} otherwise.
     * 
     * @param request current http request
     * @return {@code true} if the user is logged in, {@code false} otherwise
     */
    boolean isLoggedIn(HttpServletRequest request);
    
    /**
     * Redirects the user to the login page with a redirection back to the page the request was made to.
     * 
     * @param request request the user should be redirected after login 
     * @param response response to use to redirect the user
     * @throws IOException thrown if there is a problem when sending the redirect
     */
    void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
