package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.util.Check;

/**
 * Displays the authorization form
 */
final class GetAuthorizationPage implements AuthorizationRequestProcessor
{
    private final AuthorizationRenderer renderer;
    
    public GetAuthorizationPage(AuthorizationRenderer renderer)
    {
        this.renderer = Check.notNull(renderer, "renderer");
    }

    public void process(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException
    {
        renderer.render(request, response, token);
    }
}