package com.atlassian.oauth.serviceprovider.internal;

import java.net.URI;

import javax.annotation.Nullable;

import net.oauth.OAuthMessage;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;

/**
 * Provides methods for generating request and access tokens.  
 */
public interface TokenFactory
{
    /**
     * Generate an unauthorized request token.
     * 
     * @param consumer Consumer information for generating the request token
     * @param callback parsed and validated OAuth callback {@code URI} 
     * @param message OAuth message that can be used to grab any additional parameters to use when creating the request token
     * @param version OAuth token version to be created
     */
    ServiceProviderToken generateRequestToken(Consumer consumer, @Nullable URI callback, OAuthMessage message, Version version);
    
    /**
     * Returns a newly generated access token for the authorized request token.
     * 
     * @param token an authorized request token
     * @throws IllegalArgumentException thrown if the request token in is not authorized
     */
    ServiceProviderToken generateAccessToken(ServiceProviderToken token);
}
