package com.atlassian.oauth.serviceprovider.internal;

import java.util.Map;

import org.springframework.osgi.service.ServiceUnavailableException;

import com.atlassian.oauth.Request;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.TokenPropertiesFactory;
import com.atlassian.oauth.util.Check;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * Creates token properties from requests by aggregating the properties returned from a set of other
 * {@code TokenPropertiesFactory}s.  The {@code TokenPropertiesFactory}s are dynamic imports from other plugins.  If
 * multiple {@code TokenPropertiesFactory}s return properties with the same key, it is indeterminant which value will
 * be used.
 */
public final class AggregateTokenPropertiesFactory implements TokenPropertiesFactory
{
    private final Iterable<TokenPropertiesFactory> propertyFactories;
    
    public AggregateTokenPropertiesFactory(Iterable<TokenPropertiesFactory> propertyFactories)
    {
        this.propertyFactories = Check.notNull(propertyFactories, "propertyFactories");
    }
    
    public Map<String, String> newRequestTokenProperties(Request request)
    {
        Map<String, String> properties = Maps.newHashMap();
        for (TokenPropertiesFactory propertiesFactory : propertyFactories)
        {
            try
            {
                properties.putAll(propertiesFactory.newRequestTokenProperties(request));
            }
            catch (ServiceUnavailableException e)
            {
                // ignore it and move on to the next
            }
        }
        return ImmutableMap.copyOf(properties);
    }

    public Map<String, String> newAccessTokenProperties(ServiceProviderToken requestToken)
    {
        Map<String, String> properties = Maps.newHashMap();
        for (TokenPropertiesFactory propertiesFactory : propertyFactories)
        {
            try
            {
                properties.putAll(propertiesFactory.newAccessTokenProperties(requestToken));
            }
            catch (ServiceUnavailableException e)
            {
                // ignore it and move on to the next
            }
        }
        return ImmutableMap.copyOf(properties);
    }
}
