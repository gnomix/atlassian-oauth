package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;

interface AuthorizationRequestProcessor
{
    void process(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException;
}