package com.atlassian.oauth.serviceprovider.internal.util;

/**
 * Paths to the OAuth endpoints for requesting tokens, authorizing tokens and exchanging request tokens for access
 * tokens.  The paths are relative to the applications base URL.
 */
public class Paths
{
    /**  Path to the OAuth endpoint for requesting a token, relative to the application base URL **/ 
    public static final String REQUEST_TOKEN = "/plugins/servlet/oauth/request-token";

    /**  Path to the OAuth endpoint for authorizing a token, relative to the application base URL **/ 
    public static final String AUTHORIZE = "/plugins/servlet/oauth/authorize";

    /**  Path to the OAuth endpoint for swapping a request token for an access token, relative to the application base URL **/ 
    public static final String ACCESS_TOKEN = "/plugins/servlet/oauth/access-token";    
}
