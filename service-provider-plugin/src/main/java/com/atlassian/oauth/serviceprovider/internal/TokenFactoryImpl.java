package com.atlassian.oauth.serviceprovider.internal;

import java.net.URI;

import javax.annotation.Nullable;

import net.oauth.OAuthMessage;

import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.bridge.Requests;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Session;
import com.atlassian.oauth.serviceprovider.TokenPropertiesFactory;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.util.Check;

/**
 * Simple {@code TokenFactory} which uses the consumer key and the System time to generate tokens.
 */
public class TokenFactoryImpl implements TokenFactory
{
    private final TokenPropertiesFactory propertiesFactory;
    private final Randomizer randomizer;
    
    public TokenFactoryImpl(@Qualifier("aggregatePropertiesFactory") TokenPropertiesFactory propertiesFactory, Randomizer randomizer)
    {
        this.propertiesFactory = Check.notNull(propertiesFactory, "propertiesFactory");
        this.randomizer = Check.notNull(randomizer, "randomizer");
    }
    
    public ServiceProviderToken generateRequestToken(Consumer consumer, @Nullable URI callback, OAuthMessage message, Version version)
    {
        Check.notNull(consumer, "consumer");
        
        String token = randomizer.randomAlphanumericString(32);
        String secret = randomizer.randomAlphanumericString(32);
        
        return ServiceProviderToken.newRequestToken(token)
            .tokenSecret(secret)
            .consumer(consumer)
            .callback(callback)
            .version(version)
            .properties(propertiesFactory.newRequestTokenProperties(Requests.fromOAuthMessage(message)))
            .build();
    }

    public ServiceProviderToken generateAccessToken(ServiceProviderToken token)
    {
        Check.notNull(token, "token");
        // make sure token is authorized
        if (token.isRequestToken() && token.getAuthorization() != Authorization.AUTHORIZED)
        {
            throw new IllegalArgumentException("token is not an authorized request token");
        }
        
        String t = randomizer.randomAlphanumericString(32);

        return ServiceProviderToken.newAccessToken(t)
            .tokenSecret(token.getTokenSecret())
            .consumer(token.getConsumer())
            .authorizedBy(token.getUser())
            .properties(propertiesFactory.newAccessTokenProperties(token))
            .session(newSession(token))
            .build();
    }

    private Session newSession(ServiceProviderToken token)
    {
        Session.Builder builder = Session.newSession(randomizer.randomAlphanumericString(32));
        if (token.getSession() != null)
        {
            builder.creationTime(token.getSession().getCreationTime());
        }
        return builder.build();
    }
}
