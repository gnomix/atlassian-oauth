package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.ServiceProvider;

/**
 * Creates {@link ServiceProvider} objects to be used when doing OAuth operations.  A new
 * {@code ServiceProvider} should be created for each operation in case the applications base URL changes.
 */
public interface ServiceProviderFactory
{
    /**
     * Returns a new {@code ServiceProvider} object which defines the URLs consumers and users should use as the
     * service provider endpoints for requesting tokens, authorizing tokens and swapping request tokens for access
     * tokens in the OAuth dance.
     *  
     * @return new {@code OAuthServiceProvider} object
     */
    ServiceProvider newServiceProvider();
}
