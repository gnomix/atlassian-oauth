package com.atlassian.oauth.serviceprovider.internal;

import java.util.Date;

import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.google.common.collect.ImmutableMap;

/**
 * Schedules the {@link ExpiredSessionRemover} to run every 8 hours.
 */
public final class ExpiredSessionRemoverScheduler implements LifecycleAware
{
    public static final String TOKEN_STORE_KEY = "tokenStore";
    
    private static final long EIGHT_HOURS = 28800000;
    private final ServiceProviderTokenStore store;
    private final PluginScheduler pluginScheduler;

    public ExpiredSessionRemoverScheduler(@Qualifier("tokenStore") ServiceProviderTokenStore store,
        PluginScheduler pluginScheduler)
    {
        this.store = store;
        this.pluginScheduler = pluginScheduler;
    }

    public void onStart()
    {
        pluginScheduler.scheduleJob(
            "Service Provider Session Remover",
            ExpiredSessionRemover.class,
            ImmutableMap.<String, Object>of(TOKEN_STORE_KEY, store),
            new Date(System.currentTimeMillis() + EIGHT_HOURS), 
            EIGHT_HOURS
        );
    }
}
