package com.atlassian.oauth.serviceprovider.internal.servlet;

import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import org.slf4j.Logger;

/**
 * Utility methods for servlets that implement OAuth.
 *
 * @since   1.0.13
 */
public class OAuthProblemUtils
{
    public static void logOAuthProblem(final OAuthMessage message,
                                       final OAuthProblemException ope,
                                       final Logger logger)
    {
        if (OAuth.Problems.TIMESTAMP_REFUSED.equals(ope.getProblem()))
        {
            logger.warn("Rejecting OAuth request for url \"{}\" due to invalid timestamp ({}). " +
                    "This is most likely due to our system clock not being " +
                    "synchronized with the consumer's clock.",
                    new Object[] { message.URL, ope.getParameters() });
        }
        else if (logger.isDebugEnabled())
        {
            // include the full stacktrace
            logger.warn(
                    "Problem encountered authenticating OAuth client request for url \"" +
                    message.URL + "\", error was \"" + ope.getProblem() +
                    "\", with parameters \"" + ope.getParameters() + "\"", ope);
        }
        else
        {
            // omit the stacktrace
            logger.warn(
                "Problem encountered authenticating OAuth client for url \"{}\", error was \"{}\", with parameters \"{}\"",
                new Object[] { message.URL, ope.getProblem(), ope.getParameters() }
            );
        }
    }
}
