package com.atlassian.oauth.serviceprovider.internal.servlet.user;

import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.servlet.authorize.LoginRedirector;
import com.atlassian.oauth.util.Check;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.google.common.collect.Iterables.transform;

@SuppressWarnings("serial")
public class AccessTokensServlet extends HttpServlet
{
    public static final String PATH = "/plugins/servlet/oauth/users/access-tokens";
    public static final String TEMPLATE = "templates/user/access-tokens.vm";

    private static final String URL_REGEX_PATTERN = "((.*?)(https?://\\S+))";
    
    private final ServiceProviderTokenStore store;
    private final UserManager userManager;
    private final LoginRedirector loginRedirector;
    private final TemplateRenderer templateRenderer;
    private final LocaleResolver localeResolver;

    public AccessTokensServlet(@Qualifier("tokenStore") ServiceProviderTokenStore store,
        UserManager userManager,
        LocaleResolver localeResolver,
        LoginRedirector loginRedirector,
        TemplateRenderer templateRenderer)
    {
        this.store = Check.notNull(store, "store");
        this.userManager = Check.notNull(userManager, "userManager");
        this.localeResolver = Check.notNull(localeResolver, "localeResolver");
        this.loginRedirector = Check.notNull(loginRedirector, "loginRedirector");
        this.templateRenderer = Check.notNull(templateRenderer, "templateRenderer");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null)
        {
            loginRedirector.redirectToLogin(request, response);
            return;
        }
        Locale locale = localeResolver.getLocale(request);
        Map<String, Object> context = ImmutableMap.<String, Object>of(
            "tokenItems", ImmutableList.copyOf(getTokenRepresentations(username)),
            "dateFormat", DateFormat.getDateInstance(DateFormat.MEDIUM, locale),
            "timeFormat", DateFormat.getTimeInstance(DateFormat.SHORT, locale)
        );
        response.setContentType("text/html;charset=UTF-8");
        templateRenderer.render(TEMPLATE, context, response.getWriter());
    }
    
    private Iterable<TokenRepresentation> getTokenRepresentations(String username)
    {
        return transform(store.getAccessTokensForUser(username), toTokenRepresentation);
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null)
        {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        String tokenParam = request.getParameter("token");
        if (tokenParam == null)
        {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        ServiceProviderToken token = store.get(tokenParam);
        if (token == null)
        {
            return;
        }
        if (!username.equals(token.getUser().getName()))
        {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        store.remove(tokenParam);
    }
    
    /**
     * Representation of a token/consumer item for our Velocity template.
     *
     */
    public static final class TokenRepresentation
    {
        private final ServiceProviderToken token;
        private final URI consumerUri;

        TokenRepresentation(ServiceProviderToken token)
        {
            this.token = token;
            this.consumerUri = parseUriFromDescription(getDescription());
        }

        /**
         * The token string.
         */
        public String getToken()
        {
            return token.getToken();
        }
        
        /**
         * The human-readable name of the consumer application.
         */
        public String getConsumerName()
        {
            if (token.hasProperty("alternate.consumer.name"))
            {
                return token.getProperty("alternate.consumer.name");
            }
            return token.getConsumer().getName();
        }
        
        /**
         * The hostname of the consumer application, or null if unknown.
         */
        public String getConsumerHostName()
        {
            URI uri = getConsumerUri();
            return (uri == null) ? null : uri.getHost();
        }
        
        /**
         * The base URI of the consumer application, or null if unknown.
         * <p>
         * This is currently always null because we are not storing a URI in
         * the OAuth consumer object; it is included in the consumer description
         * string if the consumer was created by Applinks, but there's no reliable
         * way to parse that string.
         */
        public URI getConsumerUri()
        {
            return consumerUri;
        }

        /**
         * Additional descriptive text about the consumer application.
         */
        public String getDescription()
        {
            return token.getConsumer().getDescription();
        }
        
        /**
         * The date/time that the token was created.
         */
        public Date getCreationTime()
        {
            return new Date(token.getCreationTime());
        }
        
        /**
         * The date/time at which the token will expire.
         */
        public Date getExpirationTime()
        {
            return new Date(token.getCreationTime() + token.getTimeToLive());
        }
        
        /**
         * The number of days until the token expires, rounded down to an integer.
         */
        public int getDaysTillExpiration()
        {
            long millisTillExpiration = (token.getCreationTime() + token.getTimeToLive())
                    - System.currentTimeMillis();
            if (millisTillExpiration < 0)
            {
                millisTillExpiration = 0;
            }
            return (int)(millisTillExpiration / (1000 * 60 * 60 * 24));
        }
    }

    private static URI parseUriFromDescription(String description)
    {
        Pattern p = Pattern.compile(URL_REGEX_PATTERN);
        Matcher m = p.matcher(description.trim());

        if (!m.matches())
        {
            return null;
        }

        try
        {
            return URI.create(m.group(3));
        }
        catch (IllegalArgumentException e)
        {
            // return null if we can't parse the URI
            return null;
        }
    }

    private static final Function<ServiceProviderToken, TokenRepresentation> toTokenRepresentation =
        new Function<ServiceProviderToken, AccessTokensServlet.TokenRepresentation>()
    {

        public TokenRepresentation apply(ServiceProviderToken from)
        {
            return new TokenRepresentation(from);
        }
    };
}
