package com.atlassian.oauth.serviceprovider.internal.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import net.oauth.OAuthProblemException;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;

/**
 * Centralizes the logic of fetching and validating tokens for use.
 */
public interface TokenLoader
{
    /**
     * Gets the token specified in the request and validates that it is ok to proceed with authorization.
     * 
     * @param request http request for authorization of a token
     * @return token to be authorized
     * @throws IOException thrown if there is a problem reading the token parameter from the request
     * @throws OAuthProblemException thrown if the token is not capable of being validated
     */
    ServiceProviderToken getTokenForAuthorization(HttpServletRequest request) throws OAuthProblemException, IOException;
}
