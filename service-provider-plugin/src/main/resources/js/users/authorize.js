
AJS.$(function($) {
    
    $("#oauth-login-and-authorize").each(function() {
        $form = $(this);
        $form.find("#approve").click(function(event) {
            $form.submit();
        });
        $form.find("#deny").click(function(event) {
            $("#oauth-deny").submit();
        });
    });
});
