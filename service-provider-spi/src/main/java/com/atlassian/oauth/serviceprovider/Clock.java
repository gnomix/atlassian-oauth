package com.atlassian.oauth.serviceprovider;

/**
 * Service for determining the current time.
 */
public interface Clock
{
    /**
     * Return the current time in milliseconds.
     * 
     * @return current time in milliseconds
     */
    long timeInMilliseconds();
}
