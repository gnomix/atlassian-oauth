package com.atlassian.oauth.serviceprovider;

import com.atlassian.oauth.Consumer;

/**
 * Provides persistent storage for OAuth consumers.  The implementation of this store should only concern itself
 * with the immediate task that it is being asked to perform. As an example, if a {@link Consumer} is being removed,
 * the implementation should not concern itself with removing the tokens created using that {@code Consumer}.  They
 * should be removed by the caller of the {@link #remove(String)} method.  It is the sole task of the store to
 * save objects to a persistent backend and retrieve or remove them when requested.  
 */
public interface ServiceProviderConsumerStore
{
    /**
     * Put the consumer in the store, keyed by the value returned by {@link Consumer#getKey()}.
     * 
     * @param consumer consumer instance to store
     * @throws StoreException thrown if there is a problem storing the {@code Consumer}
     */
    void put(Consumer consumer) throws StoreException;
    
    /**
     * Retrieve a {@code Consumer} from the store that was previously put with the {@code consumerKey}.
     *  
     * @param consumerKey key of the {@code Consumer} to retrieve
     * @return {@code Consumer} that was stored with the {@code consumerKey} using {@link OAuthStore#putConsumer(String, Consumer),
     *         {@code null} if no {@code Consumer} has previously been stored with the {@code consumerKey}
     * @throws StoreException thrown if there is a problem loading the {@code Consumer}
     */
    Consumer get(String consumerKey) throws StoreException;

    /**
     * Remove the {@code Consumer} from the store that is associated with the {@code consumerKey}.  If there is no
     * {@code Consumer} associated with the {@code consumerKey}, this is a no-op.
     *  
     * @param consumerKey the key of the consumer to be removed
     * @throws StoreException thrown if there is an exception while removing the consumer
     */
    void remove(String consumerKey) throws StoreException;
    
    /**
     * Retrieve all the {@code Consumers} from the store.
     * 
     * @return all the {@code Consumers} from the store
     * @throws StoreException thrown if there is a problem retrieving one or more of the {@code Consumer}s
     */
    Iterable<Consumer> getAll() throws StoreException;
}
