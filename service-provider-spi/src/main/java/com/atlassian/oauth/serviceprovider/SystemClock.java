package com.atlassian.oauth.serviceprovider;

/**
 * A clock that uses the {@link System} methods to determine the current time. 
 */
public final class SystemClock implements Clock
{
    public long timeInMilliseconds()
    {
        return System.currentTimeMillis();
    }
}
