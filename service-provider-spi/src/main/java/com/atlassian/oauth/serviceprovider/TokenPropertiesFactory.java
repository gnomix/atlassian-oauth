package com.atlassian.oauth.serviceprovider;

import java.util.Map;

import com.atlassian.oauth.Request;

/**
 * Provides a way to create token properties that will be associated with request tokens when they are created. 
 */
public interface TokenPropertiesFactory
{
    /**
     * The alternate consumer name is a property that can be used in token properties to indicate a more specific
     * consumer name that can be used when displaying information about the token to the user (such as when they
     * are looking at their access tokens and trying to find one they want to revoke).
     */
    String ALTERNAME_CONSUMER_NAME = "alternate.consumer.name";
    
    /**
     * Create and return any properties that should be associated with the request token that are found in the request.
     * 
     * @param request request to create a token that might contain additional information to associate with the token
     * @return any properties that should be associated with the request token that are found in the request
     */
    Map<String, String> newRequestTokenProperties(Request request);

    /**
     * Create and return any properties that should be associated with the access token.
     * 
     * @param requestToken request token that is being exchanged for an access token
     * @return any properties that should be associated with the access token
     */
    Map<String, String> newAccessTokenProperties(ServiceProviderToken requestToken);
}
