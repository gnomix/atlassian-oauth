package com.atlassian.oauth.serviceprovider;

import static com.atlassian.hamcrest.DeepIsEqual.deeplyEqualTo;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.security.Principal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.google.common.collect.ImmutableMap;

@RunWith(MockitoJUnitRunner.class)
public class ServiceProviderTokenTest
{
    static final Consumer CONSUMER = Consumer.key("consumer-hmac")
        .name("Consumer using HMAC")
        .description("description")
        .signatureMethod(SignatureMethod.HMAC_SHA1)
        .callback(URI.create("http://consumer/default-callback"))
        .build();

    static final Principal USER = new Principal()
    {
        public String getName()
        {
            return "derf";
        }
    };

    static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234")
        .tokenSecret("5678")
        .consumer(CONSUMER)
        .callback(URI.create("http://consumer/callback"))
        .creationTime(1234567890)
        .timeToLive(10000)
        .properties(ImmutableMap.of("some", "property"))
        .version(V_1_0_A)
        .build();

    static final ServiceProviderToken V1_UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234")
        .tokenSecret("5678")
        .consumer(CONSUMER)
        .callback(URI.create("http://consumer/callback"))
        .creationTime(1234567890)
        .timeToLive(10000)
        .properties(ImmutableMap.of("some", "property"))
        .version(V_1_0)
        .build();

    @Mock Clock clock;

    @Test
    public void assertThatAuthorizedByAssignsUserAndVerifierAndCopiesAllOtherAttributes()
    {
        ServiceProviderToken authorizedRequestToken = ServiceProviderToken.newRequestToken("1234")
            .tokenSecret("5678")
            .consumer(CONSUMER)
            .authorizedBy(USER)
            .verifier("9876")
            .callback(URI.create("http://consumer/callback"))
            .creationTime(1234567890)
            .timeToLive(10000)
            .properties(ImmutableMap.of("some", "property"))
            .version(V_1_0_A)
            .build();
        
        assertThat(UNAUTHORIZED_REQUEST_TOKEN.authorize(USER, "9876"), is(deeplyEqualTo(authorizedRequestToken)));
    }

    @Test
    public void assertThatDeniedByAssignsUserAndCopiesAllOtherAttributes()
    {
        ServiceProviderToken deniedRequestToken = ServiceProviderToken.newRequestToken("1234")
            .tokenSecret("5678")
            .consumer(CONSUMER)
            .deniedBy(USER)
            .callback(URI.create("http://consumer/callback"))
            .creationTime(1234567890)
            .timeToLive(10000)
            .properties(ImmutableMap.of("some", "property"))
            .version(V_1_0_A)
            .build();
        
        assertThat(UNAUTHORIZED_REQUEST_TOKEN.deny(USER), is(deeplyEqualTo(deniedRequestToken)));
    }
    
    @Test
    public void assertThatHasExpiredReturnsFalseWhenTokensLifetimeHasNotPassed()
    {
        when(clock.timeInMilliseconds())
            .thenReturn(UNAUTHORIZED_REQUEST_TOKEN.getCreationTime() + UNAUTHORIZED_REQUEST_TOKEN.getTimeToLive() - 5);
        assertFalse(UNAUTHORIZED_REQUEST_TOKEN.hasExpired(clock));
    }

    @Test
    public void assertThatHasExpiredReturnsTrueWhenTokensLifetimeHasPassed()
    {
        when(clock.timeInMilliseconds())
            .thenReturn(UNAUTHORIZED_REQUEST_TOKEN.getCreationTime() + UNAUTHORIZED_REQUEST_TOKEN.getTimeToLive() + 5);
        assertTrue(UNAUTHORIZED_REQUEST_TOKEN.hasExpired(clock));
    }
    
    @Test
    public void assertThatIsValidCallbackReturnsTrueForAbsoluteHttpUris()
    {
        assertTrue(ServiceProviderToken.isValidCallback(URI.create("http://consumer/callback")));
    }

    @Test
    public void assertThatIsValidCallbackReturnsTrueForAbsoluteHttpsUris()
    {
        assertTrue(ServiceProviderToken.isValidCallback(URI.create("https://consumer/callback")));
    }

    @Test
    public void assertThatIsValidCallbackReturnsFalseForNonAbsoluteUris()
    {
        assertFalse(ServiceProviderToken.isValidCallback(URI.create("/callback")));
    }

    @Test
    public void assertThatIsValidCallbackReturnsFalseForNonHttpOrHttpsUris()
    {
        assertFalse(ServiceProviderToken.isValidCallback(URI.create("ftp://crazy/callback")));
    }
    
    @Test
    public void assertThatVerifierIsNotRequiredForVersion1RequestTokens()
    {
        assertThat(V1_UNAUTHORIZED_REQUEST_TOKEN.authorize(USER, null).getAuthorization(), is(equalTo(Authorization.AUTHORIZED)));
    }
}
