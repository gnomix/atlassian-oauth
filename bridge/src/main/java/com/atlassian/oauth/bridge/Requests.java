package com.atlassian.oauth.bridge;

import static com.google.common.collect.Iterables.transform;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

import net.oauth.OAuth;
import net.oauth.OAuthMessage;
import net.oauth.OAuth.Parameter;

import com.atlassian.oauth.Request;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.util.Check;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

/**
 * A utility class for converting between Atlassian {@link Request} objects and the OAuth.net library
 * {@link OAuthMessage} objects.
 */
public final class Requests
{
    private Requests() {}
    
    /**
     * Converts the list of {@code Request.Parameter}s to {@code OAuth.Parameter}s.
     * 
     * @param requestParameters {@code Request.Parameter}s to be converted to {@code OAuth.Parameter}s
     * @return {@code OAuth.Parameter}s converted from the {@code Request.Parameter}s 
     */
    public static Iterable<OAuth.Parameter> asOAuthParameters(Iterable<Request.Parameter> requestParameters)
    {
        Check.notNull(requestParameters, "requestParameters");
        return transform(requestParameters, toOAuthParameters);
    }

    /**
     * Converts the list of {@code OAuth.Parameter}s to {@code Request.Parameter}s.
     * 
     * @param oauthParameters {@code OAuth.Parameter}s to be converted to {@code Request.Parameter}s
     * @return {@code Request.Parameter}s converted from the {@code OAuth.Parameter}s
     */
    public static Iterable<Request.Parameter> fromOAuthParameters(List<? extends Map.Entry<String, String>> oauthParameters)
    {
        Check.notNull(oauthParameters, "oauthParameters");
        return transform(oauthParameters, toRequestParameters);
    }

    /**
     * Converts the {@code Request} to an {@code OAuthMessage}.
     * 
     * @param request {@code Request} to be converted to an {@code OAuthMessage}
     * @return {@code OAuthMessage} converted from the {@code Request}
     */
    public static OAuthMessage asOAuthMessage(Request request)
    {
        Check.notNull(request, "request");
        return new OAuthMessage(
            request.getMethod().name(),
            request.getUri().toString(), 
            // We'd rather not do the copy, but since we need a Collection of these things we don't have much choice 
            ImmutableList.copyOf(asOAuthParameters(request.getParameters()))
        );
    }
    
    /**
     * Converts the {@code OAuthMessage} to an {@code Request}.
     * 
     * @param message {@code OAuthMessage} to be converted to an {@code Request}
     * @return {@code Request} converted from {@code OAuthMessage}
     */
    public static Request fromOAuthMessage(OAuthMessage message)
    {
        Check.notNull(message, "message");
        try
        {
            return new Request(
                HttpMethod.valueOf(message.method.toUpperCase()),
                URI.create(message.URL),
                fromOAuthParameters(message.getParameters())
            );
        }
        catch (IOException e)
        {
            throw new RuntimeException("Failed to convert from OAuthMessage", e);
        }
    }

    private static final Function<Map.Entry<String, String>, Request.Parameter> toRequestParameters = new Function<Map.Entry<String, String>, Request.Parameter>()
    {
        public Request.Parameter apply(Map.Entry<String, String> p)
        {
            Check.notNull(p, "parameter");
            return new Request.Parameter(p.getKey(), p.getValue());
        }
    };
    
    private static final Function<Request.Parameter, Parameter> toOAuthParameters = new Function<Request.Parameter, Parameter>()
    {
        public Parameter apply(com.atlassian.oauth.Request.Parameter p)
        {
            Check.notNull(p, "parameter");
            return new Parameter(p.getName(), p.getValue());
        }
    };
}
