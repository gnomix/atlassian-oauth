package com.atlassian.oauth.bridge;

import java.net.URI;

import net.oauth.OAuthServiceProvider;

import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.util.Check;

/**
 * A utility class for converting between Atlassian {@link ServiceProvider} objects and the OAuth.net library
 * {@link OAuthServiceProvider} objects.
 */
public final class ServiceProviders
{
    private ServiceProviders() {}

    /**
     * Converts a {@code ServiceProvider} to an {@code OAuthServiceProvider}.
     * 
     * @param serviceProvider {@code ServiceProvider} to convert to {@code OAuthServiceProvider}
     * @return {@code OAuthServiceProvider} converted from the {@code ServiceProvider}
     */
    public static OAuthServiceProvider asOAuthServiceProvider(ServiceProvider serviceProvider)
    {
        Check.notNull(serviceProvider, "serviceProvider");
        return new OAuthServiceProvider(
            serviceProvider.getRequestTokenUri().normalize().toString(),
            serviceProvider.getAuthorizeUri().normalize().toString(),
            serviceProvider.getAccessTokenUri().normalize().toString()
        );
    }

    /**
     * Converts an {@code OAuthServiceProvider} to a {@code ServiceProvider}.
     * 
     * @param serviceProvider {@code OAuthServiceProvider} to convert to {@code ServiceProvider}
     * @return {@code ServiceProvider} converted from the {@code OAuthServiceProvider}
     */
    public static ServiceProvider fromOAuthServiceProvider(OAuthServiceProvider serviceProvider)
    {
        Check.notNull(serviceProvider, "serviceProvider");
        return new ServiceProvider(
            URI.create(serviceProvider.requestTokenURL),
            URI.create(serviceProvider.userAuthorizationURL),
            URI.create(serviceProvider.accessTokenURL)
        );
    }
}
