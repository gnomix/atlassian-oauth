package com.atlassian.oauth.bridge;

import java.net.URI;
import java.security.PrivateKey;
import java.security.PublicKey;

import net.oauth.OAuth;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthServiceProvider;
import net.oauth.signature.RSA_SHA1;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.Consumer.InstanceBuilder;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.util.Check;

/**
 * A utility class for converting between Atlassian {@link Consumer} objects and the OAuth.net library
 * {@link OAuthConsumer} objects.
 */
public final class Consumers
{
    private Consumers() {}
    
    /**
     * Converts a {@code Consumer} to an {@code OAuthConsumer}.
     * 
     * @param consumer {@code Consumer} to be converted to an {@code OAuthConsumer}
     * @param serviceProvider {@code ServiceProvider} to convert to {@code OAuthServiceProvider}, and set as the
     *                        {@link OAuthConsumer#serviceProvider} attribute
     * @return {@code OAuthConsumer} converted from the {@code Consumer}
     */
    public static OAuthConsumer asOAuthConsumer(Consumer consumer, ServiceProvider serviceProvider)
    {
        return asOAuthConsumer(consumer, (String) null, serviceProvider);
    }

    /**
     * Converts a {@code Consumer} to an {@code OAuthConsumer}.
     * 
     * @param consumer {@code Consumer} to be converted to an {@code OAuthConsumer}
     * @param oauthServiceProvider {@code OAuthServiceProvider} to set as the {@link OAuthConsumer#serviceProvider} attribute
     * @return {@code OAuthConsumer} converted from the {@code Consumer}
     */
    public static OAuthConsumer asOAuthConsumer(Consumer consumer, OAuthServiceProvider oauthServiceProvider)
    {
        return asOAuthConsumer(consumer, (String) null, oauthServiceProvider);
    }

    /**
     * Converts a {@code Consumer} to an {@code OAuthConsumer}, includes the {@code privateKey} in the
     * {@code OAuthConsumer}s properties so that it is suitable for signing.
     * 
     * @param consumer {@code Consumer} to be converted to an {@code OAuthConsumer}
     * @param privateKey {@code PrivateKey} to use to sign requests
     * @param serviceProvider {@code ServiceProvider} to convert to {@code OAuthServiceProvider}, and set as the
     *                        {@link OAuthConsumer#serviceProvider} attribute
     * @return {@code OAuthConsumer} converted from the {@code Consumer}
     */
    public static OAuthConsumer asOAuthConsumer(Consumer consumer, PrivateKey privateKey, ServiceProvider serviceProvider)
    {
        OAuthConsumer oauthConsumer = asOAuthConsumer(consumer, serviceProvider);
        oauthConsumer.setProperty(RSA_SHA1.PRIVATE_KEY, privateKey);
        return oauthConsumer;
    }
    
    /**
     * Converts a {@code Consumer} to an {@code OAuthConsumer}, uses the {@code sharedSecret} in the
     * {@code OAuthConsumer}s constructor so that it is suitable for signing.
     * 
     * @param consumer {@code Consumer} to be converted to an {@code OAuthConsumer}
     * @param sharedSecret shared secret to use to sign requests
     * @param serviceProvider {@code ServiceProvider} to convert to {@code OAuthServiceProvider}, and set as the
     *                        {@link OAuthConsumer#serviceProvider} attribute
     * @return {@code OAuthConsumer} converted from the {@code Consumer}
     */
    public static OAuthConsumer asOAuthConsumer(Consumer consumer, String sharedSecret, ServiceProvider serviceProvider)
    {
        Check.notNull(serviceProvider, "serviceProvider");
        return asOAuthConsumer(consumer, sharedSecret, ServiceProviders.asOAuthServiceProvider(serviceProvider));
    }
    
    /**
     * Converts a {@code Consumer} to an {@code OAuthConsumer}, uses the {@code sharedSecret} in the
     * {@code OAuthConsumer}s constructor so that it is suitable for signing.
     * 
     * @param consumer {@code Consumer} to be converted to an {@code OAuthConsumer}
     * @param sharedSecret shared secret to use to sign requests
     * @param serviceProvider {@code OAuthServiceProvider} to set as the {@link OAuthConsumer#serviceProvider} attribute
     * @return {@code OAuthConsumer} converted from the {@code Consumer}
     */
    public static OAuthConsumer asOAuthConsumer(Consumer consumer, String sharedSecret, OAuthServiceProvider oauthServiceProvider)
    {
        Check.notNull(consumer, "consumer");
        Check.notNull(oauthServiceProvider, "oauthServiceProvider");

        String callback = consumer.getCallback() != null ? consumer.getCallback().toString() : null;
        
        OAuthConsumer oauthConsumer = new OAuthConsumer(callback, consumer.getKey(), sharedSecret, oauthServiceProvider);
        oauthConsumer.setProperty(ConsumerProperty.NAME, consumer.getName());
        oauthConsumer.setProperty(ConsumerProperty.DESCRIPTION, consumer.getDescription());
        if (consumer.getSignatureMethod() == SignatureMethod.RSA_SHA1)
        {
            oauthConsumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);
            oauthConsumer.setProperty(RSA_SHA1.PUBLIC_KEY, consumer.getPublicKey());
        }
        else
        {
            oauthConsumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.HMAC_SHA1);
        }
        return oauthConsumer;
    }

    /**
     * Converts an {@code OAuthConsumer} to a {@code Consumer}.
     * 
     * @param consumer {@code OAuthConsumer} to convert to a {@code Consumer}
     * @return {@code Consumer} converted from the {@code OAuthConsumer}
     */
    public static Consumer fromOAuthConsumer(OAuthConsumer consumer)
    {
        Check.notNull(consumer, "consumer");
        String name = (String) consumer.getProperty(ConsumerProperty.NAME);
        
        URI callback = consumer.callbackURL != null ? URI.create(consumer.callbackURL) : null;
        String description = (String) consumer.getProperty(ConsumerProperty.DESCRIPTION);

        InstanceBuilder builder = Consumer.key(consumer.consumerKey).name(name).description(description).callback(callback);

        if (consumer.getProperty(OAuth.OAUTH_SIGNATURE_METHOD).equals(OAuth.HMAC_SHA1))
        {
            builder.signatureMethod(SignatureMethod.HMAC_SHA1);
        }
        else
        {
            // we could check if it's a string or byte[] and do the conversion, but for now let's keep things simple
            PublicKey publicKey = (PublicKey) consumer.getProperty(RSA_SHA1.PUBLIC_KEY);
            builder.publicKey(publicKey);
        }
        
        return builder.build();
    }

    static final class ConsumerProperty
    {
        static final String NAME = "name";
        static final String DESCRIPTION = "description";
    }
}
