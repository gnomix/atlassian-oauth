package com.atlassian.oauth.bridge.serviceprovider;

import net.oauth.OAuthAccessor;

import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.bridge.Consumers;
import com.atlassian.oauth.bridge.Tokens;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.util.Check;

public final class ServiceProviderTokens
{
    /**
     * Converts a {@code ServiceProviderToken} to an {@code OAuthAccessor}, setting the {@code requestToken} or 
     * {@code accessToken} accordingly to the type of the {@code ServiceProviderToken}.
     * 
     * @param token {@code ServiceProviderToken} to convert to {@code OAuthAccessor}
     * @param serviceProvider {@code OAuthServiceProvider} for the application
     * @return {@code OAuthAccessor} converted from the {@code ServiceProviderToken}
     */
    public static OAuthAccessor asOAuthAccessor(ServiceProviderToken token, ServiceProvider serviceProvider)
    {
        Check.notNull(token, "token");
        Check.notNull(serviceProvider, "serviceProvider");
    
        OAuthAccessor accessor = new OAuthAccessor(Consumers.asOAuthConsumer(token.getConsumer(), serviceProvider));
        setTokenData(accessor, token);
        return accessor;
    }

    private static void setTokenData(OAuthAccessor accessor, ServiceProviderToken token)
    {
        Tokens.setCommonTokenData(accessor, token);
        if (token.isRequestToken())
        {
            if (token.getAuthorization() == Authorization.AUTHORIZED)
            {
                accessor.setProperty(Tokens.AccessorProperty.USER, token.getUser());
                accessor.setProperty(Tokens.AccessorProperty.AUTHORIZED, true);
            }
            else if (token.getAuthorization() == Authorization.DENIED)
            {
                accessor.setProperty(Tokens.AccessorProperty.USER, token.getUser());
                accessor.setProperty(Tokens.AccessorProperty.AUTHORIZED, false);
            }
        }
        else
        {
            accessor.accessToken = token.getToken();
            accessor.setProperty(Tokens.AccessorProperty.USER, token.getUser());
            accessor.setProperty(Tokens.AccessorProperty.AUTHORIZED, true);
        }
        accessor.tokenSecret = token.getTokenSecret();
        accessor.setProperty(Tokens.AccessorProperty.CREATION_TIME, token.getCreationTime());
    }
}
