package com.atlassian.oauth.bridge.consumer;

import java.security.PrivateKey;

import net.oauth.OAuthAccessor;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.bridge.Consumers;
import com.atlassian.oauth.bridge.Tokens;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.util.Check;

public final class ConsumerTokens
{

    /**
     * Converts a {@code ConsumerToken} to an {@code OAuthAccessor}, setting the {@code requestToken} or 
     * {@code accessToken} accordingly to the type of the {@code ConsumerToken}.
     * 
     * @param token {@code ConsumerToken} to convert to {@code OAuthAccessor}
     * @param serviceProvider {@code OAuthServiceProvider} for the application
     * @return {@code OAuthAccessor} converted from the {@code ConsumerToken}
     */
    public static OAuthAccessor asOAuthAccessor(ConsumerToken token, ServiceProvider serviceProvider)
    {
        Check.notNull(token, "token");
        Check.notNull(serviceProvider, "serviceProvider");
    
        OAuthAccessor accessor = new OAuthAccessor(Consumers.asOAuthConsumer(token.getConsumer(), serviceProvider));
        Tokens.setCommonTokenData(accessor, token);
        return accessor;
    }

    /**
     * Converts a {@code ConsumerToken} to an {@code OAuthAccessor}. Sets the {@code requestToken} or {@code accessToken}
     * accordingly to the type of the {@code ConsumerToken}.  Also uses the {@code privateKey} when converting the
     * {@code Consumer} to make the returned {@code OAuthAccessor} suitable for signing requests.
     * 
     * @param token {@code ConsumerToken} to convert to {@code OAuthAccessor}
     * @param privateKey {@code PrivateKey} to use when converting the {@code Consumer}
     * @param serviceProvider {@code OAuthServiceProvider} for the application
     * @return {@code OAuthAccessor} converted from the {@code ConsumerToken}, suitable for signing requests
     */
    public static OAuthAccessor asOAuthAccessor(ConsumerToken token, PrivateKey privateKey, ServiceProvider serviceProvider)
    {
        Check.notNull(token, "token");
        Check.notNull(serviceProvider, "serviceProvider");
    
        OAuthAccessor accessor = new OAuthAccessor(Consumers.asOAuthConsumer(token.getConsumer(), privateKey, serviceProvider));
        Tokens.setCommonTokenData(accessor, token);
        return accessor;
    }

    /**
     * Converts a {@code ConsumerToken} to an {@code OAuthAccessor}. Sets the {@code requestToken} or {@code accessToken}
     * accordingly to the type of the {@code ConsumerToken}.  Also uses the {@code sharedSecret} when converting the
     * {@code Consumer} to make the returned {@code OAuthAccessor} suitable for signing requests.
     * 
     * @param token {@code ConsumerToken} to convert to {@code OAuthAccessor}
     * @param sharedSecret shared secret to use when converting the {@code Consumer}
     * @param serviceProvider {@code OAuthServiceProvider} for the application
     * @return {@code OAuthAccessor} converted from the {@code ConsumerToken}, suitable for signing requests
     */
    public static OAuthAccessor asOAuthAccessor(ConsumerToken token, String sharedSecret, ServiceProvider serviceProvider)
    {
        Check.notNull(token, "token");
        Check.notNull(serviceProvider, "serviceProvider");
    
        OAuthAccessor accessor = new OAuthAccessor(Consumers.asOAuthConsumer(token.getConsumer(), sharedSecret, serviceProvider));
        Tokens.setCommonTokenData(accessor, token);
        return accessor;
    }

    /**
     * Converts an {@code OAuthAccessor} to a {@code ConsumerToken}.  The token will be set to either an access
     * or request token depending on whether the {@code accessToken} or {@code requestToken} field is set on the
     * {@code accessor}.
     * 
     * <p>Warning: It is impossible to get all the properties that have been set on an {@code OAuthAccessor} so we 
     * cannot copy them into the {@code ConsumerToken}.
     * 
     * @param access {@code OAuthAccessor} to convert to a {@code ConsumerToken}
     * @return {@code ConsumerToken} converted from the {@code OAuthAccessor}
     */
    public static ConsumerToken asConsumerToken(OAuthAccessor accessor)
    {
        Check.notNull(accessor, "accessor");
    
        Consumer consumer = Consumers.fromOAuthConsumer(accessor.consumer);
        if (accessor.accessToken != null)
        {
            return ConsumerToken.newAccessToken(accessor.accessToken)
                .tokenSecret(accessor.tokenSecret)
                .consumer(consumer)
                .build();
        }
        else
        {
            return ConsumerToken.newRequestToken(accessor.requestToken)
                .tokenSecret(accessor.tokenSecret)
                .consumer(consumer)
                .build();
        }
    }
}
