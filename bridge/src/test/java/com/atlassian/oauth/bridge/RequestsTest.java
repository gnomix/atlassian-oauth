package com.atlassian.oauth.bridge;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

import java.net.URI;
import java.util.List;

import net.oauth.OAuth;
import net.oauth.OAuthMessage;

import org.junit.Test;

import com.atlassian.oauth.Request;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.testing.TestData;
import com.google.common.collect.Lists;

public class RequestsTest
{
    @Test
    public void assertThatWeCanConvertFromRequestParametersToOAuthParameters()
    {
        assertThat(Requests.fromOAuthParameters(TestData.OAuthParameters.list), contains(TestData.RequestParameters.array));
    }
    
    @Test(expected=NullPointerException.class)
    public void assertThatNullPointerExceptionIsThrownIfConvertingOAuthParametersWithNullEntry()
    {
        List<OAuth.Parameter> oauthParameters = Lists.newArrayList(
            new OAuth.Parameter("param1", "value1"),
            null,
            new OAuth.Parameter("param3", "value3")
        );
        // have to do something with the converted iterable because the transform is done lazily
        for (Request.Parameter p : Requests.fromOAuthParameters(oauthParameters)) { p.getName(); };
    }
    
    @Test
    public void assertThatWeCanConvertFromOAuthParametersToRequestParameters()
    {
        assertThat(Requests.asOAuthParameters(TestData.RequestParameters.iterable), contains(TestData.OAuthParameters.array));
    }
    
    @Test(expected=NullPointerException.class)
    public void assertThatNullPointerExceptionIsThrownIfConvertingRequestParametersWithNullEntry()
    {
        List<Request.Parameter> requestParameters = Lists.newArrayList(
            new Request.Parameter("param1", "value1"),
            null,
            new Request.Parameter("param3", "value3")
        );
        // have to do something with the converted iterable because the transform is done lazily
        for (OAuth.Parameter p : Requests.asOAuthParameters(requestParameters)) { p.getKey(); };
    }
    
    @Test
    public void assertThatWeCanConvertRequestsToOAuthMessages()
    {
        Request request = new Request(HttpMethod.GET, URI.create("http://domain/path"), TestData.RequestParameters.iterable);
        OAuthMessage message = new OAuthMessage("GET", "http://domain/path", TestData.OAuthParameters.list);
        
        assertThat(Requests.asOAuthMessage(request), is(equalTo(message)));
    }
    
    @Test
    public void assertThatWeCanConvertOAuthMessagesToRequests()
    {
        OAuthMessage message = new OAuthMessage("GET", "http://domain/path", TestData.OAuthParameters.list);
        Request request = new Request(HttpMethod.GET, URI.create("http://domain/path"), TestData.RequestParameters.iterable);
        
        assertThat(Requests.fromOAuthMessage(message), is(equalTo(request)));
    }
}
