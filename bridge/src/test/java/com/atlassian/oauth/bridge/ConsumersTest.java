package com.atlassian.oauth.bridge;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.HMAC_OAUTH_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.HMAC_OAUTH_CONSUMER_WITH_SECRET;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER_WITH_PRIVATE_KEY;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.SERVICE_PROVIDER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class ConsumersTest
{
    @Test
    public void assertThatConsumerUsingRsaSha1EncodingIsConvertedToOAuthConsumer() throws Exception
    {
        assertThat(Consumers.asOAuthConsumer(RSA_CONSUMER, SERVICE_PROVIDER), is(equalTo(RSA_OAUTH_CONSUMER)));
    }
    
    @Test
    public void assertThatConsumerUsingHmacSha1EncodingIsConvertedToOAuthConsumer()
    {
        assertThat(Consumers.asOAuthConsumer(HMAC_CONSUMER, SERVICE_PROVIDER), is(equalTo(HMAC_OAUTH_CONSUMER)));
    }

    @Test
    public void assertThatConsumerUsingRsaSha1AndPrivateKeyIsConvertedToOAuthConsumer() throws Exception
    {
        assertThat(
            Consumers.asOAuthConsumer(RSA_CONSUMER, KEYS.getPrivate(), SERVICE_PROVIDER),
            is(equalTo(RSA_OAUTH_CONSUMER_WITH_PRIVATE_KEY))
        );
    }
    
    @Test
    public void assertThatConsumerUsingHmacSha1WithSharedSecretIsConvertedToOAuthConsumer()
    {
        assertThat(
            Consumers.asOAuthConsumer(HMAC_CONSUMER, "secret", SERVICE_PROVIDER), 
            is(equalTo(HMAC_OAUTH_CONSUMER_WITH_SECRET))
        );
    }
    
    @Test
    public void assertThatOAuthConsumerUsingRsaSha1IsConvertedIntoConsumer()
    {
        assertThat(Consumers.fromOAuthConsumer(RSA_OAUTH_CONSUMER), is(equalTo(RSA_CONSUMER)));
    }

    @Test
    public void assertThatOAuthConsumerUsingHmacSha1IsConvertedIntoConsumer()
    {
        assertThat(Consumers.fromOAuthConsumer(HMAC_OAUTH_CONSUMER), is(equalTo(HMAC_CONSUMER)));
    }
}
