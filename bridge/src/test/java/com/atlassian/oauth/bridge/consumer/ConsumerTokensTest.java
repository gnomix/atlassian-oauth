package com.atlassian.oauth.bridge.consumer;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.bridge.OAuthAccessorBuilder.accessAccessorFor;
import static com.atlassian.oauth.bridge.OAuthAccessorBuilder.unauthorizedRequestAccessorFor;
import static com.atlassian.oauth.testing.TestData.HMAC_SECRET;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.HMAC_OAUTH_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.HMAC_OAUTH_CONSUMER_WITH_SECRET;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.RSA_OAUTH_CONSUMER_WITH_PRIVATE_KEY;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.SERVICE_PROVIDER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerToken;

public class ConsumerTokensTest
{
    @Test
    public void assertThatWeCanConvertUnauthorizedRequestConsumerTokensToOAuthAccessorsWithoutTheConsumerPrivateKey()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(unauthorizedRequestConsumerTokenFor(RSA_CONSUMER), SERVICE_PROVIDER),
            is(equalTo(unauthorizedRequestAccessorFor(RSA_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertAccessConsumerTokensToOAuthAccessorsWithoutTheConsumerPrivateKey()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(accessConsumerTokenFor(RSA_CONSUMER), SERVICE_PROVIDER),
            is(equalTo(accessAccessorFor(RSA_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertUnauthorizedConsumerRequestTokensToOAuthAccessorsWithoutTheConsumerSecret()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(unauthorizedRequestConsumerTokenFor(HMAC_CONSUMER), SERVICE_PROVIDER),
            is(equalTo(unauthorizedRequestAccessorFor(HMAC_OAUTH_CONSUMER)))
        );
    }

    @Test
    public void assertThatWeCanConvertAccessConsumerTokensToOAuthAccessorsWithoutTheConsumerSecret()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(accessConsumerTokenFor(HMAC_CONSUMER), SERVICE_PROVIDER),
            is(equalTo(accessAccessorFor(HMAC_OAUTH_CONSUMER)))
        );
    }
    
    @Test
    public void assertThatWeCanConvertUnauthorizedRequestConsumerTokensToOAuthAccessorsWithTheConsumerPrivateKey()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(unauthorizedRequestConsumerTokenFor(RSA_CONSUMER), KEYS.getPrivate(), SERVICE_PROVIDER), 
            is(equalTo(unauthorizedRequestAccessorFor(RSA_OAUTH_CONSUMER_WITH_PRIVATE_KEY)))
        );
    }

    @Test
    public void assertThatWeCanConvertAccessConsumerTokensToOAuthAccessorsWithTheConsumerPrivateKey()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(accessConsumerTokenFor(RSA_CONSUMER), KEYS.getPrivate(), SERVICE_PROVIDER),
            is(equalTo(accessAccessorFor(RSA_OAUTH_CONSUMER_WITH_PRIVATE_KEY)))
        );
    }

    @Test
    public void assertThatWeCanConvertUnauthorizedRequestConsumerTokensToOAuthAccessorsWithTheConsumerSecret()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(unauthorizedRequestConsumerTokenFor(HMAC_CONSUMER), HMAC_SECRET, SERVICE_PROVIDER),
            is(equalTo(unauthorizedRequestAccessorFor(HMAC_OAUTH_CONSUMER_WITH_SECRET)))
        );
    }

    @Test
    public void assertThatWeCanConvertAccessConsumerTokensToOAuthAccessorsWithTheConsumerSecret()
    {
        assertThat(
            ConsumerTokens.asOAuthAccessor(accessConsumerTokenFor(HMAC_CONSUMER), HMAC_SECRET, SERVICE_PROVIDER), 
            is(equalTo(accessAccessorFor(HMAC_OAUTH_CONSUMER_WITH_SECRET)))
        );
    }
    
    @Test
    public void assertThatWeCanConvertOAuthAccessorsToUnauthorizedRequestConsumerTokens()
    {
        assertThat(
            ConsumerTokens.asConsumerToken(unauthorizedRequestAccessorFor(HMAC_OAUTH_CONSUMER_WITH_SECRET)),
            is(equalTo(unauthorizedRequestConsumerTokenFor(HMAC_CONSUMER)))
        );
    }
    
    @Test
    public void assertThatWeCanConvertOAuthAccessorsToAccessConsumerTokens()
    {
        assertThat(
            ConsumerTokens.asConsumerToken(accessAccessorFor(RSA_OAUTH_CONSUMER)),
            is(equalTo(accessConsumerTokenFor(RSA_CONSUMER)))
        );
    }

    
    private ConsumerToken unauthorizedRequestConsumerTokenFor(Consumer consumer)
    {
        return ConsumerToken.newRequestToken("1234").tokenSecret("5678").consumer(consumer).build();
    }

    private ConsumerToken accessConsumerTokenFor(Consumer consumer)
    {
        return ConsumerToken.newAccessToken("1234").tokenSecret("5678").consumer(consumer).build();
    }
}
