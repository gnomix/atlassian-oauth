package com.atlassian.oauth.bridge;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.OAUTH_SERVICE_PROVIDER;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.SERVICE_PROVIDER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class ServiceProvidersTest
{
    @Test
    public void assertThatWeCanConvertFromOAuthServiceProviderToServiceProvider()
    {
        assertThat(ServiceProviders.fromOAuthServiceProvider(OAUTH_SERVICE_PROVIDER), is(equalTo(SERVICE_PROVIDER)));
    }
    
    @Test
    public void assertThatWeCanConvertFromServiceProvidersToOAuthServiceProviders()
    {
        assertThat(ServiceProviders.asOAuthServiceProvider(SERVICE_PROVIDER), is(equalTo(OAUTH_SERVICE_PROVIDER)));
    }
}
