package com.atlassian.oauth.bridge;

import static com.atlassian.oauth.testing.TestData.USER;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;

import com.atlassian.oauth.bridge.Tokens.AccessorProperty;

public class OAuthAccessorBuilder
{
    public static OAuthAccessor unauthorizedRequestAccessorFor(OAuthConsumer consumer)
    {
        OAuthAccessor accessor = new OAuthAccessor(consumer);
        accessor.requestToken = "1234";
        accessor.tokenSecret = "5678";
        accessor.setProperty(AccessorProperty.CREATION_TIME, 1234567890L);
        return accessor;
    }
    
    public static  OAuthAccessor authorizedRequestAccessorFor(OAuthConsumer consumer)
    {
        OAuthAccessor accessor = unauthorizedRequestAccessorFor(consumer);
        accessor.setProperty(AccessorProperty.USER, USER);
        accessor.setProperty(AccessorProperty.AUTHORIZED, true);
        accessor.setProperty(AccessorProperty.VERIFIER, "9876");
        return accessor;
    }

    public static  OAuthAccessor accessAccessorFor(OAuthConsumer consumer)
    {
        OAuthAccessor accessor = new OAuthAccessor(consumer);
        accessor.accessToken = "1234";
        accessor.tokenSecret = "5678";
        accessor.setProperty(AccessorProperty.USER, USER);
        accessor.setProperty(AccessorProperty.AUTHORIZED, true);
        accessor.setProperty(AccessorProperty.CREATION_TIME, 1234567890L);
        return accessor;
    }
}
