package it.com.atlassian.oauth;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import net.oauth.OAuth;
import net.oauth.OAuth.Parameter;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthServiceProvider;
import net.oauth.client.OAuthClient;
import net.oauth.client.httpclient4.HttpClient4;
import net.oauth.signature.RSA_SHA1;

import org.apache.commons.httpclient.NameValuePair;
import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.FormEncodingType;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequestSettings;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.google.common.collect.ImmutableList;

import static com.atlassian.oauth.Request.OAUTH_AUTHORIZATION_EXPIRES_IN;
import static com.atlassian.oauth.Request.OAUTH_EXPIRES_IN;
import static com.atlassian.oauth.Request.OAUTH_SESSION_HANDLE;
import static net.oauth.OAuth.OAUTH_CALLBACK;
import static net.oauth.OAuth.OAUTH_CALLBACK_CONFIRMED;
import static net.oauth.OAuth.OAUTH_TOKEN_SECRET;
import static net.oauth.OAuth.OAUTH_VERIFIER;
import static net.oauth.OAuth.Problems.PARAMETER_ABSENT;
import static net.oauth.OAuth.Problems.PERMISSION_DENIED;
import static net.oauth.OAuth.Problems.PERMISSION_UNKNOWN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class OAuthDanceTest
{
    private static final String BASE_URL = System.getProperty("baseurl") != null 
        ? System.getProperty("baseurl") : "http://localhost:8080/oauth";
        
    private static final String SERVLET_BASE_URL = BASE_URL + "/plugins/servlet";
    private static final String REQUEST_TOKEN_URL = SERVLET_BASE_URL + "/oauth/request-token";
    private static final String AUTHORIZE_URL = SERVLET_BASE_URL + "/oauth/authorize";
    private static final String ACCESS_TOKEN_URL = SERVLET_BASE_URL + "/oauth/access-token";
    private static final String LOGIN_URL = SERVLET_BASE_URL + "/login";
    private static final String WHOAMI_URL = SERVLET_BASE_URL + "/whoami";
    private static final String STRICT_SECURITY_URL = SERVLET_BASE_URL + "/oauth/integration-test/strict-security";
    
    private static final String CALLBACK_URL = "http://localhost:8080/consumer/oauthcallback";
    private static final String CONSUMER_KEY = "hardcoded-consumer";
    
    private static final String NOCALLBACK_CONSUMER_KEY = "hardcoded-nocallback";
    
    private static final String CONSUMER_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFkPMZQaTqsSXI+bSI65rSVaDzic6WFA3WCZMVMi7lYXJAUdkXo4DgdfvEBO21Bno3bXIoxqS411G8S53I39yhSp7z2vcB76uQQifi0LEaklZfbTnFUXcKCyfwgKPp0tQVA+JZei6hnscbSw8qEItdc69ReZ6SK+3LHhvFUUP1nLhJDsgdPHRXSllgZzqvWAXQupGYZVANpBJuK+KAfiaVXCgA71N9xx/5XTSFi5K+e1T4HVnKAzDasAUt7Mmad+1PE+56Gpa73FLk1Ww+xaAEvss6LehjyWHM5iNswoNYzrNS2k6ZYkDnZxUlbrPDELETbz/n3YgBHGUlyrXi2PBjAgMBAAECggEAAtMctqq6meRofuQbEa4Uq5cv0uuQeZLV086VPMNX6k2nXYYODYl36T2mmNndMC5khvBYpn6Ykk/5yjBmlB2nQOMZPLFPwMZVdJ2Nhm+naJLZC0o7fje49PrN2mFsdoZeI+LHVLIrgoILpLdBAz/zTiW+RvLvMnXQU4wdp4eO6i8J/Jwh0AY8rWsAGkk1mdZDwklPZZiwR3z+DDsDwPxFs8z6cE5rWJd2c/fhAQrHwOXyrQPsGyLHTOqS3BkjtEZrKRUlfdgV76VlThwrE5pAWuO0GPyfK/XCklwcNS1a5XxCOq3uUogWRhCsqUX6pYfAVS6xzX56MGDndQVlp7U5uQKBgQDyTDwhsNTWlmr++FyYrc6liSF9NEMBNDubrfLJH1kaOp590bE8fu3BG0UlkVcueUr05e33Kx1DMSFW72lR4dht1jruWsbFp6LlT3SUtyW2kcSet3fC8gySs2r6NncsZ2XFPoxTkalKpQ1atGoBe3XIKeT8RDZtgoLztQy7/7yANQKBgQDQvSHEKS5SttoFFf4YkUh2QmNX5m7XaDlTLB/3xjnlz8NWOweK1aVysb4t2Tct/SR4ZZ/qZDBlaaj4X9h9nlxxIMoXEyX6Ilc4tyCWBXxn6HFMSa/Rrq662Vzz228cPvW2XGOQWdj7IqwKO9cXgJkI5W84YtMtYrTPLDSjhfpxNwKBgGVCoPq/iSOpN0wZhbE1KiCaP8mwlrQhHSxBtS6CkF1a1DPm97g9n6VNfUdnB1Vf0YipsxrSBOe416MaaRyUUzwMBRLqExo1pelJnIIuTG+RWeeu6zkoqUKCAxpQuttu1uRo8IJYZLTSZ9NZhNfbveyKPa2D4G9B1PJ+3rSO+ztlAoGAZNRHQEMILkpHLBfAgsuC7iUJacdUmVauAiAZXQ1yoDDo0Xl4HjcvUSTMkccQIXXbLREh2w4EVqhgR4G8yIk7bCYDmHvWZ2o5KZtD8VO7EVI1kD0z4Zx4qKcggGbp2AINnMYqDetopX7NDbB0KNUklyiEvf72tUCtyDk5QBgSrqcCgYEAnlg3ByRd/qTFz/darZi9ehT68Cq0CS7/B9YvfnF7YKTAv6J2Hd/i9jGKcc27x6IMi0vf7zrqCyTMq56omiLdu941oWfsOnwffWRBInvrUWTj6yGHOYUtg2z4xESUoFYDeWwe/vX6TugL3oXSX3Sy3KWGlJhn/OmsN2fgajHRip0=";
    
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING = "bb6dd1391ce33b5bd3ecad1175139a39";
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_AUTHORIZING = "29c3005cc5fbe5d431f27b29d6191ea3";
    
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING = "iezied5IEeh0IoquuGh9riexUenei4Ai";
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_WITH_NO_CALLBACK_FOR_AUTHORIZING = "xei1kohXEepheed3Hemie7AhpoiG2cum";
    
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_FOR_DENYING = "RiZie2UaooXee5siJi6gee0tmeeBe0cu";
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_DENYING = "ew0kaiK1Eetekee2Ahjah2hoAif5eu9P";
    
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING = "Ga9zoo0Ger0oa0IuNaeShoh4eiShae6a";
    private static final String NON_AUTHORIZED_REQUEST_TOKEN_SECRET_WITH_NO_CALLBACK_FOR_DENYING = "Zijae1XuoT5AYooneingi4NoXiw0uvee";
    
    private static final String SPARE_NON_AUTHORIZED_REQUEST_TOKEN = "cc7ee2402df44c6ce4fdbe2286240b40";
    private static final String SPARE_NON_AUTHORIZED_REQUEST_TOKEN_SECRET = "30d4116dd6acf6e542a38c30e7202fb4";
    
    private static final String AUTHORIZED_REQUEST_TOKEN_FOR_SWAPPING = "5c09d8d4e50065eb49a05200035bd780";
    private static final String AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING = "870abbc4847d9b5790cff56a2e9b8279";
    private static final String AUTHORIZED_REQUEST_TOKEN_VERIFIER_FOR_SWAPPING = "cu3Aechunoo1CeluKiK1Tielphooci7I";
    
    private static final String SPARE_AUTHORIZED_REQUEST_TOKEN = "6b10e9e5f61176fc50b16311146ce891";
    private static final String SPARE_AUTHORIZED_REQUEST_TOKEN_SECRET = "981bccd5958e0c6801daa67b3f0c9380";
    private static final String SPARE_AUTHORIZED_REQUEST_TOKEN_VERIFIER = "deShay0zai2YeenishooTa7iaB9suph1";
    
    private static final String ACCESS_TOKEN = "71b5607f60c0aae6161ce251dd55e8ed";
    private static final String ACCESS_TOKEN_SECRET = "160881ffbe3c4ff0f6a1ba9078e92e83";

    private static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_AUTHORIZING = "Quie9Tooico3ahShpagh0Voodoh1Phah";
    private static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING = "iqu3Ti8siet7uoShcoonaeT6zu5woh3H";
    private static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_DENYING = "eep2eeTaich5aiQuroos6IegIer2eife";
    private static final String NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING = "ieXiiN8jIec8ohbioopaqu4Athei2eiT";
    private static final String AUTHORIZED_V1_REQUEST_TOKEN_FOR_SWAPPING = "laim9XailocaZoh3Aeph2aekweesi5Ie";
    
    private static final String RENEWABLE_ACCESS_TOKEN = "oeRah6xePai6cou3Phiequ2atoo0OhJu";
    private static final String RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE = "Bohro1ziaefaJ1FiAifaKai8Phah5ahH";
    
    private static final String NON_RENEWABLE_ACCESS_TOKEN = "loP1shofiDoo5eeGiequ8oZurei8Ahch";
    private static final String NON_RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE = "Ohs5ux1kzohJu4Eeaiv0no3Ujoowae8F";
    
    private static final String CALLBACK_URI = "http://consumer/callback";

    private WebClient webClient;

    @Before
    public final void setUp()
    {
        // OAUTH-240: Please remove FIREFOX_3 as soon as the tests pass with the default setting.
        // The root cause is in AUI - see AJS-808.
        // All tests succeed in the real browsers (Only HtmlUnit fails).
        webClient = new WebClient(BrowserVersion.FIREFOX_3);
        webClient.setRedirectEnabled(false);
        webClient.setThrowExceptionOnFailingStatusCode(false);
    }

    @Test
    public void assertThatConsumerCanGetRequestToken() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        OAuthMessage message = client.getRequestTokenResponse(accessor, "POST", 
            ImmutableList.of(new Parameter(OAUTH_CALLBACK, CALLBACK_URI)));
        
        assertNotNull("request token", accessor.requestToken);
        assertThat(message.getParameter(OAUTH_CALLBACK_CONFIRMED), is(equalTo("true")));
    }
    
    @Test
    public void assertThatNonAuthenticatedUserGetsRedirectedToServiceProviderLogin() throws Exception
    {
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING;

        Page page = webClient.getPage(authorizeUrl);
        WebResponse response = page.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        assertThat(response.getResponseHeaderValue("Location"), is(equalTo(LOGIN_URL + "?redir=" + uriEncode(authorizeUrl))));
    }
    
    @Test
    public void assertThatUserCanApproveAuthorizationAndWillBeRedirectedToConsumer() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING;
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer")));
        assertFalse(page.getElementsByName("approve").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("approve").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        assertThat(
            response.getResponseHeaderValue("Location"),
            allOf(
                startsWith("http://consumer/callback?"),
                containsString("oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_AUTHORIZING),
                containsString("oauth_verifier=") // the verifier value is randomly generated, no way to guess it
            )
        );
    }
    
    @Test
    public void assertThatUserCanApproveAuthorizationWithoutCallbacksAndVerifierWillBeDisplayed() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING;
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertFalse(page.getElementsByName("approve").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("approve").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_OK)));
        assertThat(response.getContentAsString(), allOf(
            containsString("You have successfully "),
            containsString("&#39;Hardcoded Consumer - Without Callback&#39;"),
            containsString("Your verification code is &#39;")
        ));
    }
    
    @Test
    public void assertThatUserCanDenyAuthorizationAndWillBeRedirectedToConsumer() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_DENYING;
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer")));
        assertFalse(page.getElementsByName("deny").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("deny").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        assertThat(
            response.getResponseHeaderValue("Location"),
            allOf(
                startsWith("http://consumer/callback?"),
                containsString("oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_FOR_DENYING),
                containsString("oauth_verifier=") // the verifier value is randomly generated, no way to guess it
            )
        );
    }
    
    @Test
    public void assertThatUserCanDenyAuthorizationWithoutCallbacksAndDeniedMessageIsDisplayed() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING;
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertFalse(page.getElementsByName("deny").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("deny").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_OK)));
        assertThat(response.getContentAsString(),
            containsString("You have denied access to &#39;Hardcoded Consumer - Without Callback&#39;")
        );
    }
    
    @Test
    public void assertThatConsumerCanSwapAuthorizedRequestTokenForAccessToken() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.requestToken = AUTHORIZED_REQUEST_TOKEN_FOR_SWAPPING;
        accessor.tokenSecret = AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING;
        OAuthMessage message = client.getAccessToken(accessor, "POST",
             ImmutableList.of(new Parameter(OAuth.OAUTH_VERIFIER, AUTHORIZED_REQUEST_TOKEN_VERIFIER_FOR_SWAPPING)));
        
        assertNotNull(message.getToken());
        assertNotNull(message.getParameter(OAUTH_TOKEN_SECRET));
        assertNotNull(message.getParameter(OAUTH_SESSION_HANDLE));
        assertNotNull(message.getParameter(OAUTH_EXPIRES_IN));
        assertNotNull(message.getParameter(OAUTH_AUTHORIZATION_EXPIRES_IN));
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatConsumerCanNotSwapUnauthorizedRequestTokenForAccessToken() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.requestToken = SPARE_NON_AUTHORIZED_REQUEST_TOKEN;
        accessor.tokenSecret = SPARE_NON_AUTHORIZED_REQUEST_TOKEN_SECRET;
        try
        {
            client.getAccessToken(accessor, "POST", 
                ImmutableList.of(new Parameter(OAuth.OAUTH_VERIFIER, SPARE_AUTHORIZED_REQUEST_TOKEN_VERIFIER)));
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(PERMISSION_UNKNOWN)));
            throw e;
        }
    }
    
    @Test
    public void assertThatConsumerCanAccessRestrictedResourcesWithAccessToken() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.accessToken = ACCESS_TOKEN;
        accessor.tokenSecret = ACCESS_TOKEN_SECRET;
        OAuthMessage response = client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(response.readBodyAsString(), is(equalTo("fred")));
    }

    @Test
    public void assertThatSuccessfulRequestIsAnnotatedAsOAuthRequest() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        accessor.accessToken = ACCESS_TOKEN;
        accessor.tokenSecret = ACCESS_TOKEN_SECRET;
        OAuthMessage response = client.invoke(accessor, STRICT_SECURITY_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(response.getHeader("X-OAUTH-REQUEST-ANNOTATED"), is(equalTo("true")));
    }
    
    @Test
    public void assertThatDanceCanBePerformedFromStartToFinish() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        OAuthMessage requestTokenResponse = client.getRequestTokenResponse(accessor, "POST",
            ImmutableList.of(new Parameter(OAUTH_CALLBACK, CALLBACK_URI)));
        assertNotNull("request token", accessor.requestToken);
        assertThat(requestTokenResponse.getParameter(OAUTH_CALLBACK_CONFIRMED), is(equalTo("true")));

        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + accessor.requestToken;
        HtmlPage page = webClient.getPage(authorizeUrl);
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("approve").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        String callback = response.getResponseHeaderValue("Location");
        assertThat(
            callback,
            allOf(
                startsWith("http://consumer/callback?"),
                containsString("oauth_token=" + accessor.requestToken),
                containsString("oauth_verifier=") // the verifier value is randomly generated, no way to guess it
            )
        );
        
        String verifier = callback.substring(callback.indexOf(OAUTH_VERIFIER) + OAUTH_VERIFIER.length() + 1).split("\\&")[0];
        client.getAccessToken(accessor, "POST", 
            ImmutableList.of(new Parameter(OAUTH_VERIFIER, verifier)));
        assertNotNull("access token", accessor.accessToken);
        
        OAuthMessage message = client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(message.readBodyAsString(), is(equalTo("fred")));
    }
    
    @Test
    public void assertThatNonAuthenticatedUserCanAccessOpenResourceAndNoWWWAuthenticateHeaderIsSent() throws Exception
    {
        Page page = webClient.getPage(WHOAMI_URL);
        assertThat(page.getWebResponse().getStatusCode(), is(equalTo(HttpServletResponse.SC_OK)));
        assertNull(page.getWebResponse().getResponseHeaderValue("WWW-Authenticate"));
    }
    
    @Test
    public void assertThatNonAuthenticatedUserCanNotAccessStrictSecurityResourcesAndWWWAuthenticateHeaderIsSent() throws Exception
    {
        Page page = webClient.getPage(STRICT_SECURITY_URL);
        assertThat(page.getWebResponse().getStatusCode(), is(equalTo(HttpServletResponse.SC_UNAUTHORIZED)));
        assertThat(page.getWebResponse().getResponseHeaderValue("WWW-Authenticate"), containsString("OAuth"));
    }
    
    @Test
    public void assertThatTryingToAuthorizeAnAccessTokenResultsInAnErrorWhenGettingAuthorizationForm() throws Exception
    {
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + ACCESS_TOKEN;

        Page page = webClient.getPage(authorizeUrl);
        assertThat(page.getWebResponse().getStatusCode(), is(equalTo(HttpServletResponse.SC_UNAUTHORIZED)));
        assertThat(page.getWebResponse().getResponseHeaderValue("WWW-Authenticate"), containsString("OAuth"));
        assertThat(page.getWebResponse().getContentAsString(), allOf(
                containsString("The request token you are trying to"),
                containsString("is not valid")));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAccessTokenResultsInAnErrorWhenPostingAuthorizationForm() throws Exception
    {
        WebRequestSettings request = new WebRequestSettings(new URL(AUTHORIZE_URL), HttpMethod.POST);
        request.setRequestParameters(ImmutableList.of(new NameValuePair("oauth_token", ACCESS_TOKEN)));
        request.setEncodingType(FormEncodingType.URL_ENCODED);
        Page page = webClient.getPage(request);
        assertThat(page.getWebResponse().getStatusCode(), is(equalTo(HttpServletResponse.SC_UNAUTHORIZED)));
        assertThat(page.getWebResponse().getResponseHeaderValue("WWW-Authenticate"), containsString("OAuth"));
        assertThat(page.getWebResponse().getContentAsString(), allOf(
                containsString("The request token you are trying to"),
                containsString("is not valid")));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAlreadyAuthorizedRequestTokenResultsInAnErrorWhenGettingAuthorizationForm() throws Exception
    {
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + SPARE_AUTHORIZED_REQUEST_TOKEN;

        Page page = webClient.getPage(authorizeUrl);
        assertThat(page.getWebResponse().getStatusCode(), is(equalTo(HttpServletResponse.SC_UNAUTHORIZED)));
        assertThat(page.getWebResponse().getResponseHeaderValue("WWW-Authenticate"), containsString("OAuth"));
        assertThat(page.getWebResponse().getContentAsString(), containsString("You have already approved or denied this access request"));
    }

    @Test
    public void assertThatTryingToAuthorizeAnAlreadyAuthorizedRequestTokenResultsInAnErrorWhenPostingAuthorizationForm() throws Exception
    {
        WebRequestSettings request = new WebRequestSettings(new URL(AUTHORIZE_URL), HttpMethod.POST);
        request.setRequestParameters(ImmutableList.of(new NameValuePair("oauth_token", SPARE_AUTHORIZED_REQUEST_TOKEN)));
        request.setEncodingType(FormEncodingType.URL_ENCODED);
        Page page = webClient.getPage(request);
        assertThat(page.getWebResponse().getStatusCode(), is(equalTo(HttpServletResponse.SC_UNAUTHORIZED)));
        assertThat(page.getWebResponse().getResponseHeaderValue("WWW-Authenticate"), containsString("OAuth"));
        assertThat(page.getWebResponse().getContentAsString(), containsString("You have already approved or denied this access request"));
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatTryingToSwapAccessTokenWithoutVerifierCausesParameterAbsentErrorResponse() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.requestToken = SPARE_AUTHORIZED_REQUEST_TOKEN;
        accessor.tokenSecret = SPARE_AUTHORIZED_REQUEST_TOKEN_SECRET;
        try
        {
            client.getAccessToken(accessor, "POST", null);
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(PARAMETER_ABSENT)));
            throw e;
        }
    }
    
    @Test
    public void assertThatConsumerCanRenewAccessTokensWithinValidAuthorizationSession() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.requestToken = RENEWABLE_ACCESS_TOKEN;
        OAuthMessage message = client.getAccessToken(accessor, "POST",
             ImmutableList.of(new Parameter("oauth_session_handle", RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE)));
        
        assertNotNull(message.getToken());
        assertNotNull(message.getParameter(OAUTH_TOKEN_SECRET));
        assertNotNull(message.getParameter(OAUTH_SESSION_HANDLE));
        assertNotNull(message.getParameter(OAUTH_EXPIRES_IN));
        assertNotNull(message.getParameter(OAUTH_AUTHORIZATION_EXPIRES_IN));
    }
    
    @Test(expected=OAuthProblemException.class)
    public void assertThatConsumerCannotRenewAccessTokensWhenAuthorizationSessionIsInvalid() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.requestToken = NON_RENEWABLE_ACCESS_TOKEN;
        try
        {
            client.getAccessToken(accessor, "POST",
                    ImmutableList.of(new Parameter("oauth_session_handle", NON_RENEWABLE_ACCESS_TOKEN_SESSION_HANDLE)));
        }
        catch (OAuthProblemException e)
        {
            assertThat(e.getProblem(), is(equalTo(PERMISSION_DENIED)));
            throw e;
        }
    }

    // OAuth v1.0 backward compat tests
    @Test
    public void assertThatConsumerCanGetVersion1RequestTokenWithoutCallback() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();

        OAuthMessage message = client.getRequestTokenResponse(accessor, "POST", null);
        assertNotNull("request token", accessor.requestToken);
        assertThat(message.getParameter(OAUTH_CALLBACK_CONFIRMED), is(nullValue()));
    }
    
    @Test
    public void assertThatUserCanApproveAuthorizationAndWillBeRedirectedToConsumerSpecifiedByRequestParameterIfTokenIsAVersion1Token() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" +
            NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_AUTHORIZING +
            "&" + OAUTH_CALLBACK + "=http://request/callback";
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer")));
        assertFalse(page.getElementsByName("approve").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("approve").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        assertThat(
            response.getResponseHeaderValue("Location"),
            allOf(
                startsWith("http://request/callback?"),
                containsString("oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_AUTHORIZING),
                not(containsString("oauth_verifier="))
            )
        );
    }

    @Test
    public void assertThatUserCanApproveAuthorizationForVersion1TokenWithoutCallbacksAndMessageWillBeDisplayed() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_AUTHORIZING;
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertFalse(page.getElementsByName("approve").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("approve").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_OK)));
        assertThat(response.getContentAsString(), allOf(
            containsString("You have successfully"),
            containsString("&#39;Hardcoded Consumer - Without Callback&#39;"),
            containsString("Please close this browser window and click continue in the client.")
        ));
    }
    
    @Test
    public void assertThatUserCanDenyAuthorizationOfVersion1TokenAndWillBeRedirectedToRequestedCallback() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL +
            "?oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_DENYING +
            "&" + OAUTH_CALLBACK + "=http://request/callback";
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer")));
        assertFalse(page.getElementsByName("deny").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("deny").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        assertThat(
            response.getResponseHeaderValue("Location"),
            allOf(
                startsWith("http://request/callback?"),
                containsString("oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_FOR_DENYING),
                not(containsString("oauth_verifier=")) // the verifier value is randomly generated, no way to guess it
            )
        );
    }
    
    @Test
    public void assertThatUserCanDenyAuthorizationOfVersion1TokenWithoutCallbacksAndDeniedMessageIsDisplayed() throws Exception
    {
        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL + "?oauth_token=" + NON_AUTHORIZED_V1_REQUEST_TOKEN_WITH_NO_CALLBACK_FOR_DENYING;
        
        HtmlPage page = webClient.getPage(authorizeUrl);
        assertThat(getConsumerName(page), is(equalTo("Hardcoded Consumer - Without Callback")));
        assertFalse(page.getElementsByName("deny").isEmpty());
        
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("deny").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_OK)));
        assertThat(response.getContentAsString(),
            containsString("You have denied access to &#39;Hardcoded Consumer - Without Callback&#39;")
        );
    }

    @Test
    public void assertThatConsumerCanSwapAuthorizedVersion1RequestTokenForAccessTokenWithoutVerifier() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        
        accessor.requestToken = AUTHORIZED_V1_REQUEST_TOKEN_FOR_SWAPPING;
        accessor.tokenSecret = AUTHORIZED_REQUEST_TOKEN_SECRET_FOR_SWAPPING;
        OAuthMessage message = client.getAccessToken(accessor, "POST", null);
        
        assertNotNull(message.getToken());
        assertNotNull(message.getParameter(OAUTH_TOKEN_SECRET));
    }

    @Test
    public void assertThatVersion1DanceCanBePerformedFromStartToFinish() throws Exception
    {
        OAuthAccessor accessor = createAccessor();
        OAuthClient client = createClient();
        OAuthMessage requestTokenResponse = client.getRequestTokenResponse(accessor, "POST", null);
        assertNotNull("request token", accessor.requestToken);
        assertThat(requestTokenResponse.getParameter(OAUTH_CALLBACK_CONFIRMED), is(nullValue()));

        loginAs(User.FRED);
        String authorizeUrl = AUTHORIZE_URL +
            "?oauth_token=" + accessor.requestToken +
            "&" + OAUTH_CALLBACK + "=http://request/callback";
        HtmlPage page = webClient.getPage(authorizeUrl);
        HtmlSubmitInput submit = (HtmlSubmitInput) page.getElementsByName("approve").get(0);
        Page resultPage = submit.click();
        WebResponse response = resultPage.getWebResponse();
        assertThat(response.getStatusCode(), is(equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
        String callback = response.getResponseHeaderValue("Location");
        assertThat(
            callback,
            allOf(
                startsWith("http://request/callback?"),
                containsString("oauth_token=" + accessor.requestToken),
                not(containsString("oauth_verifier="))
            )
        );
        
        client.getAccessToken(accessor, "POST", null);
        assertNotNull("access token", accessor.accessToken);
        
        OAuthMessage message = client.invoke(accessor, WHOAMI_URL, Collections.<Map.Entry<?, ?>>emptySet());
        assertThat(message.readBodyAsString(), is(equalTo("fred")));        
    }

    protected final OAuthClient createClient()
    {
        return new OAuthClient(new HttpClient4());
    }

    protected final OAuthAccessor createAccessor() throws IOException
    {
        OAuthServiceProvider serviceProvider = new OAuthServiceProvider(REQUEST_TOKEN_URL, AUTHORIZE_URL, ACCESS_TOKEN_URL);

        OAuthConsumer consumer = new OAuthConsumer(CALLBACK_URL, CONSUMER_KEY, null, serviceProvider);
        consumer.setProperty(RSA_SHA1.PRIVATE_KEY, CONSUMER_PRIVATE_KEY);
        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);

        return new OAuthAccessor(consumer);
    }

    private String getConsumerName(HtmlPage page)
    {
        HtmlElement element = page.getFirstByXPath("//div[@id='container']/p/strong");
        if (element == null)
        {
            return null;
        }
        return element.getTextContent();
    }

    protected final void loginAs(User user) throws Exception
    {
        webClient.setRedirectEnabled(true);
        try
        {
            HtmlPage page = webClient.getPage(LOGIN_URL);
            page.<HtmlInput> getHtmlElementById("os_username").setValueAttribute(user.username);
            page.<HtmlInput> getHtmlElementById("os_password").setValueAttribute(user.password);
            page = ((HtmlButton) page.getElementByAccessKey('l')).click();
            if (page.getElementById("os_username") != null)
            {
                throw new RuntimeException("Login failed");
            }
        }
        finally
        {
            webClient.setRedirectEnabled(false);
        }
    }

    protected final  String uriEncode(String uriComponent) throws UnsupportedEncodingException
    {
        return URLEncoder.encode(uriComponent, "UTF-8");
    }

    protected enum User
    {
        FRED("fred", "fred");

        public final String username;
        public final String password;

        private User(String username, String password)
        {
            this.username = username;
            this.password = password;
        }
    }
}
