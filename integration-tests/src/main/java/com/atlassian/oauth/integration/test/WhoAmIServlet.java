package com.atlassian.oauth.integration.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.user.UserManager;

public class WhoAmIServlet extends HttpServlet
{
    private final UserManager userManager;

    public WhoAmIServlet(UserManager userManager)
    {
        if (userManager == null)
        {
            throw new NullPointerException("userManager");
        }
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
       response.setContentType("text/plain");
       response.getWriter().print(userManager.getRemoteUsername(request));
    }
}
