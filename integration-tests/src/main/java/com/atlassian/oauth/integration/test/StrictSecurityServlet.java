package com.atlassian.oauth.integration.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.oauth.util.RequestAnnotations;
import com.atlassian.sal.api.user.UserManager;

public class StrictSecurityServlet extends HttpServlet
{
    private final UserManager userManager;

    public StrictSecurityServlet(UserManager userManager)
    {
        if (userManager == null)
        {
            throw new NullPointerException("userManager");
        }
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (userManager.getRemoteUsername(request) == null)
        {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Denied, Sucka!");
        }
        response.setContentType("text/plain");
        response.setHeader("X-OAUTH-REQUEST-ANNOTATED", String.valueOf(RequestAnnotations.isOAuthRequest(request)));
        response.getWriter().print(userManager.getRemoteUsername(request));
    }
}
