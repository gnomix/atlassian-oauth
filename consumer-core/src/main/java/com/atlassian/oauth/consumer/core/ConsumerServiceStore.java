package com.atlassian.oauth.consumer.core;

import java.security.PrivateKey;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.util.Check;

/**
 * Used to store the consumer information, including shared keys and private keys needed for signing.  The
 * implementation of this store should only concern itself with the immediate task that it is being asked to perform.
 * As an example, if a {@link Consumer} is being removed, the implementation should not concern itself with removing 
 * the tokens created by that {@code Consumer}.  The tokens should be removed by the caller of the
 * {@link #removeByKey(String)} method.  It is the sole task of the store to save objects to a persistent backend and
 * retrieve or remove them when requested.  
 * 
 * <p>NOTE: This is not a part of the SPI as we don't want to expose this sensitive information to the entire system.
 */
public interface ConsumerServiceStore
{
    /**
     * Return the {@code ConsumerAndSecret} for the given service.
     * 
     * @param service name of the service that recognizes the {@code ConsumerAndSecret} to return
     * @return {@code ConsumerAndSecret} to use for the named service, {@code null} if none are in the store
     */
    ConsumerAndSecret get(String service);
    
    /**
     * Return the {@code ConsumerAndSecret} for the given consumer key.
     * 
     * @param key consumer key to find the {@code ConsumerAndSecret} for
     * @return the {@code ConsumerAndSecret} for the given consumer key, {@code null} if no consumers exist for the key
     */
    ConsumerAndSecret getByKey(String key);
    
    /**
     * Store the {@code ConsumerAndSecret}, associating it with the named service.
     * 
     * @param service name of the service that can be used for later lookups of the {@code ConsumerAndSecret}
     * @param consumer consumer information, including the shared secret or public/private key pair, that should be stored
     */
    void put(String service, ConsumerAndSecret consumer);
    
    /**
     * Remove the {@code ConsumerAndSecret} from the store that has the {@code consumerKey}.  If there is no
     * {@code ConsumerAndSecret} with the {@code consumerKey}, this is a no-op.
     *  
     * @param consumerKey the key of the consumer to remove
     */
    void removeByKey(String consumerKey);

    /**
     * Returns all the service provider assigned consumer information.
     * 
     * @return all the service provider assigned consumer information
     */
    Iterable<Consumer> getAllServiceProviders();
    
    /**
     * Encapsulates the {@link Consumer} and the shared secret or private key that should be used when signing requests
     * to a service provider.
     */
    public final class ConsumerAndSecret
    {
        private final String serviceName;
        private final Consumer consumer;
        private final String sharedSecret;
        private final PrivateKey privateKey;

        /**
         * Create a {@code ConsumerAndSecret} where the consumer will use RSA-SHA1 to sign requests.
         * 
         * @param serviceName the name of the service the consumer is used with
         * @param consumer Consumer information
         * @param privateKey private RSA key to use when signing requests
         */
        public ConsumerAndSecret(String serviceName, Consumer consumer, PrivateKey privateKey)
        {
            if (consumer.getSignatureMethod() == SignatureMethod.HMAC_SHA1)
            {
                throw new IllegalArgumentException("consumer uses HMAC is being configured with an RSA private key");
            }
            this.serviceName = Check.notNull(serviceName, "serviceName");
            this.consumer = Check.notNull(consumer, "consumer");
            this.privateKey = Check.notNull(privateKey, "privateKey");
            this.sharedSecret = null;
        }
        
        /**
         * Create a {@code ConsumerAndSecret} where the consumer will use HMAC-SHA1 to sign requests.
         * 
         * @param serviceName the name of the service the consumer is used with
         * @param consumer Consumer information
         * @param sharedSecret shared secret between the consumer and service provider that is used to sign requests
         */
        public ConsumerAndSecret(String serviceName, Consumer consumer, String sharedSecret)
        {
            if (consumer.getSignatureMethod() == SignatureMethod.RSA_SHA1)
            {
                throw new IllegalArgumentException("consumer uses RSA is being configured with a shared secret");
            }
            this.serviceName = Check.notNull(serviceName, "serviceName");
            this.consumer = Check.notNull(consumer, "consumer");
            this.sharedSecret = Check.notNull(sharedSecret, "sharedSecret");
            this.privateKey = null;
        }
        
        /**
         * Returns the name of the service the consumer is used with.
         * 
         * @return name of the service the consumer is used with
         */
        public String getServiceName()
        {
            return serviceName;
        }

        /**
         * Returns the consumer information
         * 
         * @return the consumer information
         */
        public Consumer getConsumer()
        {
            return consumer;
        }
        
        /**
         * Returns the shared secret if the consumer uses HMAC-SHA1 to sign requests, {@code null} otherwise.
         * 
         * @return the shared secret if the consumer uses HMAC-SHA1 to sign requests, {@code null} otherwise
         */
        public String getSharedSecret()
        {
            return sharedSecret;
        }
        
        /**
         * Returns the private RSA key if the consumer uses RSA-SHA1 to sign requests, {@code null} otherwise.
         * 
         * @return the private RSA key if the consumer uses RSA-SHA1 to sign requests, {@code null} otherwise
         */
        public PrivateKey getPrivateKey()
        {
            return privateKey;
        }
    }
}
