package com.atlassian.oauth.consumer.core;

import static org.apache.commons.lang.StringUtils.isEmpty;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.Map;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.Request.Parameter;
import com.atlassian.oauth.bridge.Consumers;
import com.atlassian.oauth.bridge.consumer.ConsumerTokens;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.atlassian.oauth.consumer.OAuthConsumerNotFoundException;
import com.atlassian.oauth.consumer.OAuthSigningException;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.util.Check;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerServiceImpl implements ConsumerService
{
    private final ConsumerServiceStore store;
    private final ConsumerTokenStore tokenStore;
    private final HostConsumerAndSecretProvider hostCasProvider;
    private final  Logger log = LoggerFactory.getLogger(getClass());

    public ConsumerServiceImpl(ConsumerServiceStore store, ConsumerTokenStore tokenStore,
        HostConsumerAndSecretProvider hostCasProvider)
    {
        this.store = Check.notNull(store, "store");
        this.tokenStore = Check.notNull(tokenStore, "tokenStore");
        this.hostCasProvider = Check.notNull(hostCasProvider, "hostCasProvider");
    }
    
    public Consumer getConsumer()
    {
        return hostCasProvider.get().getConsumer();
    }
    
    public Consumer getConsumer(String service)
    {
        if (isEmpty(service))
        {
            throw new IllegalArgumentException("service is an empty string");
        }
        ConsumerAndSecret cas = store.get(service);
        if (cas == null)
        {
            return null;
        }
        return cas.getConsumer();
    }
    
    public Consumer getConsumerByKey(String consumerKey)
    {
        if (isEmpty(consumerKey))
        {
            throw new IllegalArgumentException("consumerKey is an empty string");
        }
        ConsumerAndSecret cas = store.getByKey(consumerKey);
        if (cas == null)
        {
            return null;
        }
        return cas.getConsumer();
    }
    
    public void removeConsumerByKey(String consumerKey)
    {
        if (isEmpty(consumerKey))
        {
            throw new IllegalArgumentException("consumerKey is an empty string");
        }
        tokenStore.removeTokensForConsumer(consumerKey);
        store.removeByKey(consumerKey);
    }
    
    public Iterable<Consumer> getAllServiceProviders()
    {
        return store.getAllServiceProviders();
    }

    public Consumer updateHostConsumerInformation(String name, String description, URI callback)
    {
        ConsumerAndSecret cas = hostCasProvider.get();
        Consumer oldConsumer = cas.getConsumer();
        Consumer newConsumer = Consumer.key(oldConsumer.getKey())
            .signatureMethod(oldConsumer.getSignatureMethod())
            .publicKey(oldConsumer.getPublicKey())
            .name(name)
            .description(description)
            .callback(callback)
            .build();
        if (newConsumer.getSignatureMethod() == SignatureMethod.HMAC_SHA1)
        {
            cas = new ConsumerAndSecret(cas.getServiceName(), newConsumer, cas.getSharedSecret());
        }
        else
        {
            cas = new ConsumerAndSecret(cas.getServiceName(), newConsumer, cas.getPrivateKey());
        }
        return hostCasProvider.put(cas).getConsumer();
    }

    public void add(String service, Consumer consumer, PrivateKey privateKey)
    {
        store.put(service, new ConsumerAndSecret(service, consumer, privateKey));
    }

    public void add(String service, Consumer consumer, String sharedSecret)
    {
        store.put(service, new ConsumerAndSecret(service, consumer, sharedSecret));
    }

    public Request sign(Request request, ServiceProvider serviceProvider)
    {
        return sign(request, hostCasProvider.get(), serviceProvider);
    }

    public Request sign(Request request, String consumerKey, ServiceProvider serviceProvider)
    {
        return sign(request, getConsumerAndSecret(consumerKey), serviceProvider);
    }
    
    public Request sign(Request request, ServiceProvider serviceProvider, ConsumerToken token)
    {
        ConsumerAndSecret cas = getConsumerAndSecret(token.getConsumer().getKey());
        OAuthAccessor accessor = asOAuthAccessor(token, cas, serviceProvider);
        return sign(request, accessor);
    }

    private ConsumerAndSecret getConsumerAndSecret(String consumerKey)
    {
        ConsumerAndSecret cas = store.getByKey(consumerKey);
        if (cas == null)
        {
            cas = hostCasProvider.get();
            if (!cas.getConsumer().getKey().equals(consumerKey))
            {
                throw new OAuthConsumerNotFoundException("Consumer with key '" + consumerKey + "' could not be found");
            }
        }
        return cas;
    }

    private Request sign(Request request, ConsumerAndSecret cas, ServiceProvider serviceProvider)
    {
        return sign(request, new OAuthAccessor(asOAuthConsumer(cas, serviceProvider)));
    }

    private OAuthConsumer asOAuthConsumer(ConsumerAndSecret cas, ServiceProvider serviceProvider)
    {
        if (cas.getConsumer().getSignatureMethod() == SignatureMethod.RSA_SHA1)
        {
            return Consumers.asOAuthConsumer(cas.getConsumer(), cas.getPrivateKey(), serviceProvider);
        }
        else
        {
            return Consumers.asOAuthConsumer(cas.getConsumer(), cas.getSharedSecret(), serviceProvider);
        }
    }
    
    private OAuthAccessor asOAuthAccessor(ConsumerToken token, ConsumerAndSecret cas, ServiceProvider serviceProvider)
    {
        if (cas.getConsumer().getSignatureMethod() == SignatureMethod.RSA_SHA1)
        {
            return ConsumerTokens.asOAuthAccessor(token, cas.getPrivateKey(), serviceProvider);
        }
        else
        {
            return ConsumerTokens.asOAuthAccessor(token, cas.getSharedSecret(), serviceProvider);
        }
    }

    private Request sign(Request request, OAuthAccessor accessor)
    {
        try
        {
            OAuthMessage oauthMessage = accessor.newRequestMessage(
                request.getMethod().name(),
                request.getUri().normalize().toString(),
                asOAuthParameters(request.getParameters())
            );
            log.debug("Signed request {}", oauthMessage);
            return new Request(request.getMethod(), request.getUri(), fromOAuthParameters(oauthMessage.getParameters()));
        }
        catch (net.oauth.OAuthException e)
        {
            throw new OAuthSigningException("Failed to sign the request", e);
        }
        catch (IOException e)
        {
            // this shouldn't happen as the message is not being read from any IO streams, but the OAuth library throws
            // these around like they're candy, but far less sweet and tasty. 
            throw new RuntimeException(e);
        }
        catch (URISyntaxException e)
        {
            // this shouldn't happen unless the caller somehow passed us an invalid URI object
            throw new RuntimeException(e);
        }
    }

    private Collection<OAuth.Parameter> asOAuthParameters(Iterable<Parameter> params)
    {
        return Lists.newArrayList(Iterables.transform(params, new Function<Request.Parameter, OAuth.Parameter>()
        {
            public OAuth.Parameter apply(Parameter p)
            {
                return new OAuth.Parameter(p.getName(), p.getValue());
            }
        }));
    }
    
    private Iterable<Parameter> fromOAuthParameters(Collection<Map.Entry<String, String>> params)
    {
        return Iterables.transform(params, new Function<Map.Entry<String, String>, Request.Parameter>()
        {
            public Parameter apply(Map.Entry<String, String> p)
            {
                return new Request.Parameter(p.getKey(), p.getValue());
            }
        });
    }
}
