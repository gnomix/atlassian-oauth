package com.atlassian.oauth.consumer.core;

import static com.atlassian.hamcrest.DeepIsEqual.deeplyEqualTo;
import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.atlassian.oauth.testing.TestData.Consumers.HMAC_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.OAuthConsumers.HMAC_OAUTH_CONSUMER_WITH_SECRET;
import static com.atlassian.oauth.testing.TestData.ServiceProviders.SERVICE_PROVIDER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.security.GeneralSecurityException;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.bridge.Consumers;
import com.atlassian.oauth.bridge.Requests;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.consumer.ConsumerTokenStore;
import com.atlassian.oauth.consumer.OAuthConsumerNotFoundException;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.google.common.collect.ImmutableList;

@RunWith(MockitoJUnitRunner.class)
public class ConsumerServiceImplTest
{
    public static final Consumer HOST_CONSUMER = Consumer.key("host-consumer")
        .name("Host Consumer")
        .description("description")
        .signatureMethod(SignatureMethod.RSA_SHA1)
        .publicKey(KEYS.getPublic())
        .callback(URI.create("http://consumer/host-callback"))
        .build();
    
    private static final ConsumerAndSecret HOST_CAS = new ConsumerAndSecret("__HOST_SERVICE_", HOST_CONSUMER, KEYS.getPrivate());
    private static final OAuthConsumer HOST_OAUTH_CONSUMER = Consumers.asOAuthConsumer(HOST_CONSUMER, HOST_CAS.getPrivateKey(), SERVICE_PROVIDER);
    
    private static final String RSA_SERVICE = "rsa-service";
    private static final String HMAC_SERVICE = "hmac-service";
    private static final String SHARED_SECRET = "secret";
    private static final ConsumerAndSecret RSA_CAS = new ConsumerAndSecret(RSA_SERVICE, RSA_CONSUMER, KEYS.getPrivate());
    private static final ConsumerAndSecret HMAC_CAS = new ConsumerAndSecret(HMAC_SERVICE, HMAC_CONSUMER, SHARED_SECRET);
    
    // we have to set the oauth_nonce and oauth_timestamp explicitly so it isn't generated cause it will throw
    // everything off
    private static final Request REQUEST = new Request(
        HttpMethod.GET, URI.create("http://service/api"), 
        ImmutableList.of(
            new Request.Parameter("oauth_nonce", "1"), 
            new Request.Parameter("oauth_timestamp", "1234567890")
        )
    );
    private static final ConsumerToken TOKEN = ConsumerToken.newAccessToken("1234")
        .tokenSecret("5678")
        .consumer(HMAC_CONSUMER)
        .build();
    
    @Mock ConsumerServiceStore consumerStore;
    @Mock HostConsumerAndSecretProvider hostCasProvider;
    @Mock ConsumerTokenStore tokenStore;
    
    ConsumerService consumerService;
    
    @Before
    public void setUp() throws GeneralSecurityException
    {
        when(hostCasProvider.get()).thenReturn(HOST_CAS);
        when(consumerStore.get(RSA_SERVICE)).thenReturn(RSA_CAS);
        when(consumerStore.get(HMAC_SERVICE)).thenReturn(HMAC_CAS);
        when(consumerStore.getByKey(RSA_CONSUMER.getKey())).thenReturn(RSA_CAS);
        when(consumerStore.getByKey(HMAC_CONSUMER.getKey())).thenReturn(HMAC_CAS);
        consumerService = new ConsumerServiceImpl(consumerStore, tokenStore, hostCasProvider);
    }
    
    @Test
    public void assertThatGetConsumerWithoutServiceNameReturnsHostConsumer()
    {
        assertThat(consumerService.getConsumer(), is(equalTo(HOST_CONSUMER)));
    }
    
    @Test
    public void assertThatGetConsumerWithServiceNameReturnsTheServicesConsumer()
    {
        assertThat(consumerService.getConsumer(HMAC_SERVICE), is(equalTo(HMAC_CONSUMER)));
    }
    
    @Test
    public void assertThatGetConsumerWithNoneExistentServiceReturnsNull()
    {
        assertThat(consumerService.getConsumer("non-existent"), is(nullValue()));
    }
    
    @Test
    public void assertThatGetConsumerByKeyReturnsConsumer()
    {
        assertThat(consumerService.getConsumerByKey(HMAC_CONSUMER.getKey()), is(equalTo(HMAC_CONSUMER)));
    }
    
    @Test
    public void assertThatGetConsumerByKeyThatDoesNotExistentReturnsNull()
    {
        assertThat(consumerService.getConsumerByKey("non-existent"), is(nullValue()));
    }
    
    @Test
    public void assertThatGetAllAssignedConsumerInformationReturnsDataFromStore()
    {
        Iterable<Consumer> consumers = ImmutableList.of();
        when(consumerStore.getAllServiceProviders()).thenReturn(consumers);
        assertThat(consumerService.getAllServiceProviders(), is(sameInstance(consumers)));
    }
    
    @Test
    public void verifyThatUpdateHostConsumerInformationOnlyUpdatesNameDescriptionAndCallback()
    {
        Consumer updatedHostConsumer = Consumer.key(HOST_CONSUMER.getKey())
            .callback(URI.create("http://host/callback-new"))
            .description("new host description")
            .name("New Host Consumer")
            .publicKey(HOST_CONSUMER.getPublicKey())
            .build();
        ConsumerAndSecret updatedHostCas = new ConsumerAndSecret(HOST_CAS.getServiceName(), updatedHostConsumer, HOST_CAS.getPrivateKey());
        when(hostCasProvider.put((ConsumerAndSecret) argThat(is(deeplyEqualTo(updatedHostCas))))).thenReturn(updatedHostCas);
        
        consumerService.updateHostConsumerInformation("New Host Consumer", "new host description", URI.create("http://host/callback-new"));
        
        verify(hostCasProvider).put((ConsumerAndSecret) argThat(is(deeplyEqualTo(updatedHostCas))));
    }
    
    @Test
    public void verifyThatAddConsumerWithPrivateKeyCreatesCorrectConsumerWithSecretAndPutsItInTheStore()
    {
        consumerService.add(RSA_SERVICE, RSA_CONSUMER, KEYS.getPrivate());
        verify(consumerStore).put(eq(RSA_SERVICE), (ConsumerAndSecret) argThat(is(deeplyEqualTo(RSA_CAS))));
    }
    
    @Test
    public void verifyThatAddConsumerWithSharedSecretCreatesCorrectConsumerWithSecretAndPutsItInTheStore()
    {
        consumerService.add(HMAC_SERVICE, HMAC_CONSUMER, SHARED_SECRET);
        verify(consumerStore).put(eq(HMAC_SERVICE), (ConsumerAndSecret) argThat(is(deeplyEqualTo(HMAC_CAS))));
    }

    @Test
    public void assertThatSigningWithNoConsumerKeyOrTokenUsesHostConsumerInformation() throws Exception
    {
        Request signedRequest = Requests.fromOAuthMessage(signedMessage(HOST_OAUTH_CONSUMER));
        assertThat(consumerService.sign(REQUEST, SERVICE_PROVIDER), is(equalTo(signedRequest)));
    }
    
    @Test
    public void assertThatSigningWithConsumerKeyUsesThatConsumersInformationWhenSigning() throws Exception
    {
        Request signedRequest = Requests.fromOAuthMessage(signedMessage(HMAC_OAUTH_CONSUMER_WITH_SECRET));
        assertThat(consumerService.sign(REQUEST, HMAC_CONSUMER.getKey(), SERVICE_PROVIDER), is(equalTo(signedRequest)));
    }
    
    @Test
    public void assertThatSigningWithHostConsumerKeyWillUseTheHostConsumerInformationForSigning() throws Exception
    {
        Request signedRequest = Requests.fromOAuthMessage(signedMessage(HOST_OAUTH_CONSUMER));
        assertThat(consumerService.sign(REQUEST, HOST_OAUTH_CONSUMER.consumerKey, SERVICE_PROVIDER), is(equalTo(signedRequest)));
    }
    
    @Test(expected=OAuthConsumerNotFoundException.class)
    public void assertThatTryingToSignWithUnknownConsumerKeyWillThrowOAuthConsumerException() throws Exception
    {
        consumerService.sign(REQUEST, "unknown-consumer-key", SERVICE_PROVIDER);
    }
    
    @Test
    public void assertThatSigningWithTokenUsesTheTokensConsumerInformationWhenSigning() throws Exception
    {
        Request signedRequest = Requests.fromOAuthMessage(signedMessageWithToken(HMAC_OAUTH_CONSUMER_WITH_SECRET));
        assertThat(consumerService.sign(REQUEST, SERVICE_PROVIDER, TOKEN), is(equalTo(signedRequest)));
    }
    
    @Test
    public void verifyThatRemovingConsumerByKeyRemovesTheConsumerAndAssociatedTokens()
    {
        consumerService.removeConsumerByKey("testconsumer");
        
        verify(consumerStore).removeByKey("testconsumer");
        verify(tokenStore).removeTokensForConsumer("testconsumer");
    }
    
    private OAuthMessage signedMessage(OAuthConsumer consumer) throws Exception
    {
        OAuthAccessor accessor = new OAuthAccessor(consumer);
        return accessor.newRequestMessage(
                REQUEST.getMethod().toString(),
                REQUEST.getUri().toString(),
                ImmutableList.copyOf(Requests.asOAuthParameters(REQUEST.getParameters()))
        );
    }

    private OAuthMessage signedMessageWithToken(OAuthConsumer consumer) throws Exception
    {
        OAuthAccessor accessor = new OAuthAccessor(consumer);
        accessor.accessToken = TOKEN.getToken();
        accessor.tokenSecret = TOKEN.getTokenSecret();
        return accessor.newRequestMessage(
                REQUEST.getMethod().toString(),
                REQUEST.getUri().toString(),
                ImmutableList.copyOf(Requests.asOAuthParameters(REQUEST.getParameters()))
        );
    }
}
