package com.atlassian.oauth.testing;

public class Tokens
{
    /*
     * Copied from the Tokens converter in the oauth-bridge because there is no way to make it visible here and
     * have the bridging tests use this utility class without create a circular dependency.
     */
    static final class AccessorProperty
    {
        static final String USER = "user";
        static final String AUTHORIZED = "authorized";
    }

}
