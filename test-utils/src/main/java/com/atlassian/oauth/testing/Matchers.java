package com.atlassian.oauth.testing;

import static com.atlassian.hamcrest.ClassMatchers.isAssignableTo;
import static com.atlassian.hamcrest.DeepIsEqual.deeplyEqualTo;
import static com.atlassian.hamcrest.MatcherFactories.isEqual;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.Matchers.argThat;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;
import net.oauth.OAuthServiceProvider;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import com.atlassian.hamcrest.MatcherFactory;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.google.common.collect.ImmutableSet;

public class Matchers
{
    private static final Map<Matcher<Class<?>>, MatcherFactory> extraTypeMatchers = new HashMap<Matcher<Class<?>>, MatcherFactory>()
    {{
        put(isAssignableTo(Key.class), isEqual());
    }};
    
    public static <T> Matcher<? super T> equalTo(T o)
    {
        return org.hamcrest.Matchers.equalTo(o);
    }

    public static Matcher<? super OAuthConsumer> equalTo(OAuthConsumer consumer)
    {
        return deeplyEqualTo(consumer, extraTypeMatchers);
    }
    
    public static Matcher<? super Consumer> equalTo(Consumer consumer)
    {
        return deeplyEqualTo(consumer, extraTypeMatchers);
    }

    public static Matcher<? super OAuthServiceProvider> equalTo(OAuthServiceProvider oauthServiceProvider)
    {
        return deeplyEqualTo(oauthServiceProvider);
    }

    public static Matcher<? super ServiceProvider> equalTo(ServiceProvider serviceProvider)
    {
        return deeplyEqualTo(serviceProvider);
    }
    
    public static Matcher<? super OAuthAccessor> equalTo(OAuthAccessor accessor)
    {
        return deeplyEqualTo(accessor);
    }
    
    public static Matcher<? super ServiceProviderToken> equalTo(ServiceProviderToken token)
    {
        return deeplyEqualTo(token);
    }
    
    public static Matcher<? super ConsumerToken> equalTo(ConsumerToken token)
    {
        return deeplyEqualTo(token);
    }

    public static Matcher<? super OAuthMessage> equalTo(OAuthMessage message)
    {
        return deeplyEqualTo(message);
    }

    public static Matcher<? super Request> equalTo(Request request)
    {
        return deeplyEqualTo(request);
    }

    public static final <K, V> Matcher<Map<? extends K, ? extends V>> hasEntries(Map<K, V> map)
    {
        ImmutableSet.Builder<Matcher<? super Map<? extends K, ? extends V>>> builder = ImmutableSet.builder();
        for (Map.Entry<K, V> entry : map.entrySet())
        {
            builder.add(hasEntry(entry.getKey(), entry.getValue()));
        }
        return allOf(builder.build());
    }

    public static Matcher<? super Map<String, Object>> mapWithKeys(final Matcher<? super String> keyMatcher)
    {
        return new TypeSafeDiagnosingMatcher<Map<String, Object>>()
        {
            @Override
            protected boolean matchesSafely(Map<String, Object> item, Description mismatchDescription)
            {
                boolean matches = true;
                for (String key : item.keySet())
                {
                    if (!keyMatcher.matches(key))
                    {
                        if (!matches)
                        {
                            mismatchDescription.appendText(", ");
                        }
                        mismatchDescription
                            .appendText("key \"")
                            .appendText(key)
                            .appendText("\" ");
                        keyMatcher.describeMismatch(key, mismatchDescription);
                        matches = false;
                    }
                }
                return matches;
            }

            public void describeTo(Description description)
            {
                description.appendText("Map with all keys with ").appendDescriptionOf(keyMatcher);
            }
        };
    }

    public static Matcher<? super String> withStringLength(final Matcher<? super Integer> lengthMatcher)
    {
        return new TypeSafeDiagnosingMatcher<String>()
        {
            @Override
            protected boolean matchesSafely(String item, Description mismatchDescription)
            {
                int length = item.length();
                if (!lengthMatcher.matches(length))
                {
                    lengthMatcher.describeMismatch(length, mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("string length of ").appendDescriptionOf(lengthMatcher);
            }
        };
    }

    public static Map<String, Object> hasFieldError(final String key, final String error)
    {
        return (Map<String, Object>) argThat(new TypeSafeDiagnosingMatcher<Map<String, Object>>()
        {
            @SuppressWarnings("unchecked")
            @Override
            protected boolean matchesSafely(Map<String, Object> item, Description mismatchDescription)
            {
                Object message = ((Map<String, Object>) item.get("fieldErrorMessages")).get(key);
                return message != null && error.equals(message.toString());
            }
    
            public void describeTo(Description description)
            {
                description.appendText("map containing[\"fieldErrorMessages\"-><{")
                    .appendValue(key)
                    .appendText("=")
                    .appendValue(error)
                    .appendText("}>]");
            }
        });
    }
}
