package com.atlassian.oauth.testing;

class Consumers
{
    /*
     * Copied from the Consumers converter in the oauth-bridge because there is no way to make it visible here and
     * have the bridging tests use this utility class without create a circular dependency.
     */
    static final class ConsumerProperty
    {
        static final String NAME = "name";
        static final String DESCRIPTION = "description";
    }
}
