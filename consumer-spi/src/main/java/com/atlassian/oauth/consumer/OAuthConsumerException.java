package com.atlassian.oauth.consumer;

/**
 * Base exception thrown if there are problems performing OAuth operations.
 */
public class OAuthConsumerException extends RuntimeException
{
    public OAuthConsumerException()
    {
        super();
    }

    public OAuthConsumerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OAuthConsumerException(String message)
    {
        super(message);
    }

    public OAuthConsumerException(Throwable cause)
    {
        super(cause);
    }
}
