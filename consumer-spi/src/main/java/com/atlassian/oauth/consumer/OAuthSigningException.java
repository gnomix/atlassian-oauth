package com.atlassian.oauth.consumer;

/**
 * Thrown by the {@link ConsumerService} sign methods if there is a problem during the OAuth signing procedure.
 */
public class OAuthSigningException extends RuntimeException
{
    public OAuthSigningException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
