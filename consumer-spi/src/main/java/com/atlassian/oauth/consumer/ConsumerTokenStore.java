package com.atlassian.oauth.consumer;

import net.jcip.annotations.Immutable;

import java.util.Map;

/**
 * A store where consumers can put their request or access tokens for persistent storage.  The store must
 * store all the token attributes. 
 */
public interface ConsumerTokenStore
{
    /**
     * Return the {@code ConsumerToken} that was previously stored with the {@code key}.  If no {@code ConsumerToken} 
     * was previously stored using the {@code key} value, {@code null} is returned.
     * 
     * @param key value used when the desired {@code ConsumerToken} was {@link #put(Key, ConsumerToken)} into the store
     * @return the {@code ConsumerToken} that was previously stored with the {@code key}, {@code null} if no 
     *         {@code ConsumerToken} has been stored with that {@code key} value
     */
    ConsumerToken get(Key key);

    /**
     * Returns all {@code ConsumerToken}s and their primary key {@link com.atlassian.oauth.consumer.ConsumerTokenStore.Key} for a given {@code consumerKey}.
     * 
     * @param consumerKey the consumer key these {@code ConsumerToken}s are stored against.
     * @return a map containing the primary key and the {@code ConsumerToken}s for the the given consumer key.
     *
     * @since 1.1.0
     */
    Map<Key, ConsumerToken> getConsumerTokens(String consumerKey);
    
    /**
     * Store the {@code token} in persistent storage, associated with (or indexed by) {@code key}.
     * 
     * @param key value to be used for later retrieval or removal from the store with the {@link #get(Key)} and
     *            {@code #remove(Key)} methods
     * @param token {@code ConsumerToken} object to put in persistent storage
     * @return the {@code ConsumerToken} that was stored
     */
    ConsumerToken put(Key key, ConsumerToken token);
    
    /**
     * Removes the {@code ConsumerToken} that was previously stored with the {@code key}.  If no {@code ConsumerToken} was previously
     * stored using the {@code key} value, nothing is done.
     * 
     * @param key value used when the {@code ConsumerToken} to remove was {@link #put(Key, ConsumerToken)} into the store
     */
    void remove(Key key);
    
    /**
     * Remove all the {@code ConsumerToken}s that are associated with the consumer, specified by the {@code consumerKey}.
     * 
     * @param consumerKey the key of the consumer that tokens should be removed for
     */
    void removeTokensForConsumer(String consumerKey);

    /**
     * {@code Key} instances are used by the store for indexing and retrieval of {@code ConsumerToken} objects. 
     */
    @Immutable
    final class Key
    {
        private final String key;

        public Key(String key)
        {
            if (key == null)
            {
                throw new NullPointerException("key");
            }
            this.key = key;
        }
        
        public String getKey()
        {
            return key;
        }
        
        @Override
        public String toString()
        {
            return key;
        }
        
        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || !o.getClass().equals(Key.class))
            {
                return false;
            }
            return key.equals(((Key) o).key);
        }
        
        @Override
        public int hashCode()
        {
            return key.hashCode();
        }
    }
}
